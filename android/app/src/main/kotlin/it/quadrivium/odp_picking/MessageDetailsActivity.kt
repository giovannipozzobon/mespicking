package it.quadrivium.MesPicking

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.PowerManager
import android.os.PowerManager.WakeLock
import android.util.Log
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.FileProvider
import org.json.JSONArray
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL


class MessageDetailsActivity : AppCompatActivity() {

    companion object {
        lateinit var expandableListView: ExpandableListView
        lateinit var expandableListAdapter: ExpandableListAdapter
        lateinit var expandableListTitle: ArrayList<String>
        lateinit var expandableListDetail: HashMap<String, List<String>>

        lateinit var expandableListViewInfo: ExpandableListView
        lateinit var expandableListAdapterInfo: ExpandableListAdapter
        lateinit var expandableListTitleInfo: ArrayList<String>
        lateinit var expandableListDetailInfo: HashMap<String, List<InfoMessaggio>>

        lateinit var noAllegatiTextView: TextView
        lateinit var allegatiExpandableListView: ExpandableListView

        lateinit var oggettoTextView: TextView
        lateinit var testoTextView: TextView

        var mProgressDialog: ProgressDialog? = null

        var openFileName: String? = null
        var authorityPackageName: String? = null

        /*lateinit var utenteTextView: TextView
        lateinit var ufficioTextView: TextView
        lateinit var prioritaTextView: TextView
        lateinit var dataRicezioneTextView: TextView
        lateinit var statoLetturaTextView: TextView
        lateinit var dataLetturaTextView: TextView*/
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message_details)
        setSupportActionBar(findViewById(R.id.toolbar_actionbar2))

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        getSupportActionBar()?.setTitle("Dettaglio messaggio")
        
        oggettoTextView = findViewById(R.id.details_oggetto)
        testoTextView = findViewById(R.id.details_testo)
        /*utenteTextView = findViewById(R.id.details_utente)
        ufficioTextView = findViewById(R.id.details_ufficio)
        prioritaTextView = findViewById(R.id.details_priorita)
        dataRicezioneTextView = findViewById(R.id.details_data_ricezione)
        statoLetturaTextView = findViewById(R.id.details_stato)
        dataLetturaTextView = findViewById(R.id.details_data_lettura)*/

        noAllegatiTextView = findViewById(R.id.details_no_allegati)
        allegatiExpandableListView = findViewById(R.id.expandableListView)

        val id_messaggio = intent.getIntExtra("id_messaggio",0)
        val utente_descr = intent.getStringExtra("utente_descr")
        val gruppo_descr = intent.getStringExtra("gruppo_descr")
        val data_ricezione = intent.getStringExtra("data_ricezione")
        val data_lettura = intent.getStringExtra("data_lettura")
        val oggetto = intent.getStringExtra("oggetto")
        val messaggio = intent.getStringExtra("messaggio")
        val priorita = intent.getStringExtra("priorita")
        val da_leggere = intent.getBooleanExtra("da_leggere", false)
        val allegati = intent.getStringExtra("allegati")
        val node = intent.getStringExtra("node")
        val token = intent.getStringExtra("token")
        val id_notifica = intent.getIntExtra("id_notifica", 0)
        val nuovi = intent.getIntExtra("nuovi",0)

        oggettoTextView.setText("Oggetto: " + oggetto)
        testoTextView.setText("Testo: " + messaggio)

        /*utenteTextView.setText("Utente: " + utente_descr)
        ufficioTextView.setText("Ufficio: " + gruppo_descr)
        prioritaTextView.setText("Priorità: " + priorita)
        dataRicezioneTextView.setText("Data ricezione: " + data_ricezione)
        if (da_leggere == true) {
            statoLetturaTextView.setText("Stato: Non letto")
        }
        else
        {
            statoLetturaTextView.setText("Stato: Già letto")
        }
        if (data_lettura != "") 
        {
            dataLetturaTextView.setText("Data lettura: " + data_lettura)
        }
        else
        {
            dataLetturaTextView.setText(data_lettura)
        }*/
        
        var listaINFO: MutableList<InfoMessaggio> = mutableListOf()
        
        if (da_leggere == true) 
        {
            if (data_lettura != "") 
            {
                listaINFO.add(InfoMessaggio(utente_descr, gruppo_descr, priorita, data_ricezione, "Stato: Non letto", "Data lettura: " + data_lettura))
            }
            else
            {
                listaINFO.add(InfoMessaggio(utente_descr, gruppo_descr, priorita, data_ricezione, "Stato: Non letto", data_lettura))
            }
        }
        else
        {
            if (data_lettura != "")
            {
                listaINFO.add(InfoMessaggio(utente_descr, gruppo_descr, priorita, data_ricezione, "Stato: Già letto", "Data lettura: " + data_lettura))
            }
            else
            {
                listaINFO.add(InfoMessaggio(utente_descr, gruppo_descr, priorita, data_ricezione, "Stato: Già letto", data_lettura))
            }
        }

        expandableListViewInfo = findViewById<View>(R.id.expandableListViewInfo) as ExpandableListView
        expandableListDetailInfo = ExpandableListInfoMess.setInfoData(listaINFO)
        expandableListTitleInfo = ArrayList(expandableListDetailInfo.keys)
        expandableListAdapterInfo = CustomExpandableListInfoAdapter(this, expandableListTitleInfo, expandableListDetailInfo)
        expandableListViewInfo.setAdapter(expandableListAdapterInfo)
        expandableListViewInfo.setOnGroupExpandListener { groupPosition ->
            Toast.makeText(applicationContext, expandableListTitleInfo[groupPosition] + " List Expanded.",
                    Toast.LENGTH_SHORT).show()
        }

        expandableListViewInfo.setOnGroupCollapseListener { groupPosition ->
            Toast.makeText(applicationContext, expandableListTitleInfo[groupPosition] + " List Collapsed.",
                    Toast.LENGTH_SHORT).show()
        }

        expandableListViewInfo.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            Toast.makeText(
                    applicationContext,
                    expandableListTitleInfo[groupPosition] + " -> "
                            + expandableListDetailInfo[expandableListTitleInfo[groupPosition]]!![childPosition], Toast.LENGTH_SHORT
            ).show()
            false
        }
        
        var listaURL: MutableList<String> = mutableListOf()

        if (allegati != "")
        {
            val jsonArray = JSONArray(allegati)

            for (i in 0 until jsonArray.length()) {
                val obj = jsonArray.getJSONObject(i)
                listaURL.add(obj.getString("url"))
            }
        }

        if (listaURL.isEmpty())
        {
            noAllegatiTextView.setVisibility(View.VISIBLE)
            allegatiExpandableListView.setVisibility(View.GONE)
        }
        else
        {
            noAllegatiTextView.setVisibility(View.GONE)
            allegatiExpandableListView.setVisibility(View.VISIBLE)

            expandableListView = findViewById<View>(R.id.expandableListView) as ExpandableListView
            expandableListDetail = ExpandableListDataURL.setURLData(listaURL)
            expandableListTitle = ArrayList(expandableListDetail.keys)
            expandableListAdapter = CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail)
            expandableListView.setAdapter(expandableListAdapter)
            expandableListView.setOnGroupExpandListener { groupPosition ->
                Toast.makeText(applicationContext, expandableListTitle[groupPosition] + " List Expanded.",
                        Toast.LENGTH_SHORT).show()
            }

            expandableListView.setOnGroupCollapseListener { groupPosition ->
                Toast.makeText(applicationContext, expandableListTitle[groupPosition] + " List Collapsed.",
                        Toast.LENGTH_SHORT).show()
            }

            expandableListView.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                Toast.makeText(
                        applicationContext,
                        expandableListTitle[groupPosition] + " -> "
                                + expandableListDetail[expandableListTitle[groupPosition]]!![childPosition], Toast.LENGTH_SHORT
                ).show()

                mProgressDialog = ProgressDialog(this@MessageDetailsActivity);
                mProgressDialog!!.setMessage("Download file...");
                mProgressDialog!!.setIndeterminate(true);
                mProgressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog!!.setCancelable(true);

                val urlString: List<String> = expandableListDetail[expandableListTitle[groupPosition]]!![childPosition].split("/")
                val fileName = urlString[urlString.size - 1]
                openFileName = fileName

                authorityPackageName = packageName

                val downloadTask = DownloadTask(this@MessageDetailsActivity)
                downloadTask.execute(expandableListDetail[expandableListTitle[groupPosition]]!![childPosition], fileName)

                mProgressDialog!!.setOnCancelListener {
                    downloadTask.cancel(true) //cancel the task
                }

                false
            }
        }

        if (da_leggere == true)
        {
            sendPost(node, id_notifica, id_messaggio, token, this, listaINFO, nuovi)
        }
    }

    fun sendPost(node: String, id_notifica: Int, id_messaggio: Int, token: String, context: Context, listaINFO: MutableList<InfoMessaggio>, nuovi: Int) {
        val thread = Thread(Runnable {
            try {
                val postCall = node + "wms/messaggio_letto"
                val url = URL(postCall)
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "POST"
                conn.setRequestProperty("Authorization", token)
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
                conn.setRequestProperty("Accept", "application/json")
                conn.doOutput = true
                conn.doInput = true
                val jsonParam = JSONObject()
                jsonParam.put("id_messaggio", id_messaggio)
                Log.d("JSON", jsonParam.toString())
                val os = DataOutputStream(conn.outputStream)
                os.writeBytes(jsonParam.toString())
                os.flush()
                os.close()
                Log.d("STATUS", conn.responseCode.toString())
                Log.d("MSG", conn.responseMessage)
                val code: Int = conn.getResponseCode()
                if (code == 200) {
                    Log.d("PostNotifica", "Post eseguita con successo!")

                    val url = URL(node + "wms/lista_messaggi_ricevuti?origine_chiamata=&id_messaggio=$id_messaggio&search=")
                    val con = url.openConnection() as HttpURLConnection
                    con.setRequestProperty("Authorization", token)
                    val sb = StringBuilder()
                    val bufferedReader = BufferedReader(InputStreamReader(con.inputStream))
                    var line: String? = null
                    while ({ line = bufferedReader.readLine(); line }() != null) {
                        sb.append("""$line""".trimIndent())
                    }
                    
                    val jsonArray = JSONArray(sb.toString())
                    
                    var msgLetto: Boolean = false
                    var msgDataLettura: String = ""

                    for (i in 0 until jsonArray.length()) {
                        val obj = jsonArray.getJSONObject(i)
                        msgDataLettura = obj.getString("data_lettura")
                        msgLetto = obj.getBoolean("da_leggere")
                    }

                    if (msgLetto == true)
                    {
                        listaINFO[0].setStatoLettura("Stato: Non letto")
                    }
                    else
                    {
                        listaINFO[0].setStatoLettura("Stato: Appena letto")
                    }

                    if (msgDataLettura != "")
                    {
                        listaINFO[0].setDataLettura("Data lettura: " + msgDataLettura)
                    }
                    else
                    {
                        listaINFO[0].setDataLettura(msgDataLettura)
                    }

                    /*val repliedNotification = NotificationCompat.Builder(context, MainActivity.CANALE_NOTIFICHE)
                            .setSmallIcon(R.drawable.baseline_notifications_active_black_18dp)
                            .setContentText("Notifica visualizzata!")
                            .setTimeoutAfter(3000)
                            .build()

                    val serviceContext = Context.NOTIFICATION_SERVICE
                    val notificationManager: NotificationManager = MainActivity.systemConfig?.getSystemService(serviceContext) as NotificationManager
                    NotificationManagerCompat.from(context).apply {
                        if (id_notifica != null) {
                            notificationManager.notify(id_notifica, repliedNotification)
                        }
                    }*/
                }
                conn.disconnect()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        })
        thread.start()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        val menuItem1 = menu?.findItem(R.id.action_search)
        val menuItem2 = menu?.findItem(R.id.action_notifications)

        menuItem1?.setVisible(false)
        menuItem2?.setVisible(false)

        return true
    }

    /*override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_search -> {
            // Implementare gestione ricerca appbar
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }*/

    private class DownloadTask(private val context: Context) : AsyncTask<String?, Int?, String?>() {
        private var mWakeLock: WakeLock? = null
        override fun doInBackground(vararg params: String?): String? {
            var input: InputStream? = null
            var output: OutputStream? = null
            var connection: HttpURLConnection? = null
            try {
                val url = URL(params[0])
                connection = url.openConnection() as HttpURLConnection
                connection.connect()

                if (connection!!.responseCode != HttpURLConnection.HTTP_OK) {
                    return ("Server returned HTTP " + connection.responseCode
                            + " " + connection.responseMessage)
                }

                val fileLength = connection.contentLength
                if (Environment.getExternalStorageState() != null) {
                    val internalPath = Environment.getExternalStorageDirectory().absolutePath + "/Android/data/it.quadrivium.MesPicking/files/"
                    val directory = File(internalPath, "Allegati")

                    if (!directory.exists()) {
                        directory.mkdirs()
                    }

                    input = connection.inputStream
                    output = FileOutputStream("${directory.absolutePath}/${params[1]}")
                    val data = ByteArray(4096)
                    var total: Long = 0
                    var count: Int? = null
                    while (input.read(data).also({ count = it }) != -1) {
                        if (isCancelled) {
                            input.close()
                            return null
                        }
                        total += count!!.toLong()
                        if (fileLength > 0)
                            publishProgress((total * 100 / fileLength).toInt())
                        output.write(data, 0, count!!)
                    }
                }
                else
                {
                    Log.d("DownloadAllegatoTask", "SD card non presente")
                }
            } catch (e: Exception) {
                return e.toString()
            } finally {
                try {
                    if (output != null) output.close()
                    if (input != null) input.close()
                } catch (ignored: IOException) {
                }
                connection?.disconnect()
            }
            return null
        }

        override fun onPreExecute() {
            super.onPreExecute()
            val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    javaClass.name)
            mWakeLock!!.acquire()
            mProgressDialog!!.show()
        }

        override fun onProgressUpdate(vararg progress: Int?) {
            super.onProgressUpdate(*progress)
            mProgressDialog!!.isIndeterminate = false
            mProgressDialog!!.max = 100
            mProgressDialog!!.progress = progress[0]!!
        }

        override fun onPostExecute(result: String?) {
            mWakeLock!!.release()
            mProgressDialog!!.dismiss()
            if (result != null)
            {
                Toast.makeText(context, "Download error: $result", Toast.LENGTH_LONG).show()
            }
            else
            {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show()
                try {
                    val myIntent = Intent(Intent.ACTION_VIEW)
                    val file = File(Environment.getExternalStorageDirectory().absolutePath + "/Android/data/it.quadrivium.MesPicking/files/Allegati/$openFileName")

                    val contentUri = FileProvider.getUriForFile(context, "$authorityPackageName.provider", file)
                    myIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    
                    val extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())
                    val mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
                    myIntent.setDataAndType(contentUri, mimetype)
                    context.startActivity(myIntent)
                } catch (e: java.lang.Exception) {
                    val data = e.message
                    Log.d("ApriAllegatoScaricato", data.toString())
                }
            }
        }
    }

    class ExpandableListDataURL {
        companion object {
            fun setURLData(listaURL: MutableList<String>): HashMap<String, List<String>> {
                val expandableListDetail = HashMap<String, List<String>>()
                expandableListDetail["URL"] = listaURL
                return expandableListDetail
            }
        }
    }

    class ExpandableListInfoMess {
        companion object {
            fun setInfoData(listaInfo: MutableList<InfoMessaggio>): HashMap<String, List<InfoMessaggio>> {
                val expandableListDetail = HashMap<String, List<InfoMessaggio>>()
                expandableListDetail["INFO"] = listaInfo
                return expandableListDetail
            }
        }
    }

    class InfoMessaggio {
        var utente_descr: String = ""
        var gruppo_descr: String = ""
        var priorita: String = ""
        var data_ricezione: String = ""
        var stato_lettura: String = ""
        var data_lettura: String = ""

        constructor(utente_descr: String, gruppo_descr: String, priorita: String, data_ricezione: String, stato_lettura: String, data_lettura: String)
        {
            this.utente_descr = utente_descr
            this.gruppo_descr = gruppo_descr
            this.priorita = priorita
            this.data_ricezione = data_ricezione
            this.stato_lettura = stato_lettura
            this.data_lettura = data_lettura
        }

        fun getUtenteDescr(): String? {
            return this.utente_descr
        }

        fun setUtenteDescr(utente_descr: String) {
            this.utente_descr = utente_descr
        }

        fun getGruppoDescr(): String? {
            return this.gruppo_descr
        }

        fun setGruppoDescr(mRelease: String) {
            this.gruppo_descr = gruppo_descr
        }

        fun getDataRicezione(): String? {
            return this.data_ricezione
        }

        fun setDataRicezione(data_ricezione: String) {
            this.data_ricezione = data_ricezione
        }

        fun getPrioritaMessaggio(): String? {
            return this.priorita
        }

        fun setPrioritaMessaggio(priorita: String) {
            this.priorita = priorita
        }

        fun getDataLettura(): String? {
            return this.data_lettura
        }

        fun setDataLettura(data_lettura: String) {
            this.data_lettura = data_lettura
        }
        
        fun getStatoLettura(): String? {
            return this.stato_lettura;
        }
        
        fun setStatoLettura(stato_lettura: String) {
            this.stato_lettura = stato_lettura
        }
    }

    class CustomExpandableListAdapter(context: Context, expandableListTitle: List<String>,
                                      expandableListDetail: HashMap<String, List<String>>) : BaseExpandableListAdapter() {
        private val context: Context
        private val expandableListTitle: List<String>
        private val expandableListDetail: HashMap<String, List<String>>
        override fun getChild(listPosition: Int, expandedListPosition: Int): String? {
            return expandableListDetail[expandableListTitle[listPosition]]
                    ?.get(expandedListPosition)
        }

        override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
            return expandedListPosition.toLong()
        }

        override fun getChildView(listPosition: Int, expandedListPosition: Int,
                                  isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {
            var convertView: View? = convertView
            val expandedListText = getChild(listPosition, expandedListPosition) as String
            if (convertView == null) {
                val layoutInflater = context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = layoutInflater.inflate(R.layout.list_expandable_item, null)
            }
            val expandedListTextView = convertView
                    ?.findViewById(R.id.expandedListItem) as TextView
            expandedListTextView.text = expandedListText
            return convertView
        }

        override fun getChildrenCount(listPosition: Int): Int {
            return expandableListDetail[expandableListTitle[listPosition]]?.size!!
        }

        override fun getGroup(listPosition: Int): Any {
            return expandableListTitle[listPosition]
        }

        override fun getGroupCount(): Int {
            return expandableListTitle.size
        }

        override fun getGroupId(listPosition: Int): Long {
            return listPosition.toLong()
        }

        override fun getGroupView(listPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup?): View? {
            var convertView: View? = convertView
            val listTitle = getGroup(listPosition) as String
            if (convertView == null) {
                val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = layoutInflater.inflate(R.layout.list_group, null)
            }
            val listTitleTextView = convertView
                    ?.findViewById(R.id.listTitle) as TextView
            listTitleTextView.setTypeface(null, Typeface.BOLD)
            listTitleTextView.text = listTitle
            return convertView
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
            return true
        }

        init {
            this.context = context
            this.expandableListTitle = expandableListTitle
            this.expandableListDetail = expandableListDetail
        }
    }

    class CustomExpandableListInfoAdapter(context: Context, expandableListTitle: List<String>,
                                      expandableListDetail: HashMap<String, List<InfoMessaggio>>) : BaseExpandableListAdapter() {

        private val context: Context
        private val expandableListTitle: List<String>
        private val expandableListDetail: HashMap<String, List<InfoMessaggio>>

        override fun notifyDataSetChanged() {
            notifyDataSetChanged()
        }

        override fun getChild(listPosition: Int, expandedListPosition: Int): InfoMessaggio? {
            return expandableListDetail[expandableListTitle[listPosition]]
                    ?.get(expandedListPosition)
        }

        override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
            return expandedListPosition.toLong()
        }

        override fun getChildView(listPosition: Int, expandedListPosition: Int,
                                  isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {
            var convertView: View? = convertView
            val expandedListTextInfoMessaggio = getChild(listPosition, expandedListPosition) as InfoMessaggio
            if (convertView == null) {
                val layoutInflater = context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = layoutInflater.inflate(R.layout.list_expandable_item_info, null)
            }
            val expandedListTextViewUtente = convertView?.findViewById(R.id.expandedListItemInfo1) as TextView
            val expandedListTextViewUfficio = convertView.findViewById(R.id.expandedListItemInfo2) as TextView
            val expandedListTextViewPriorita = convertView.findViewById(R.id.expandedListItemInfo3) as TextView
            val expandedListTextViewDataRicezione = convertView.findViewById(R.id.expandedListItemInfo4) as TextView
            val expandedListTextViewStatoLettura = convertView.findViewById(R.id.expandedListItemInfo5) as TextView
            val expandedListTextViewDataLettura = convertView.findViewById(R.id.expandedListItemInfo6) as TextView
            
            expandedListTextViewUtente.text = "Utente: " + expandedListTextInfoMessaggio.getUtenteDescr()
            expandedListTextViewUfficio.text = "Ufficio: " + expandedListTextInfoMessaggio.getGruppoDescr()
            expandedListTextViewPriorita.text = "Priorità: " + expandedListTextInfoMessaggio.getPrioritaMessaggio()
            expandedListTextViewDataRicezione.text = "Data ricezione: " + expandedListTextInfoMessaggio.getDataRicezione()
            expandedListTextViewStatoLettura.text = expandedListTextInfoMessaggio.getStatoLettura()
            expandedListTextViewDataLettura.text = expandedListTextInfoMessaggio.getDataLettura()
            
            return convertView
        }

        override fun getChildrenCount(listPosition: Int): Int {
            return expandableListDetail[expandableListTitle[listPosition]]?.size!!
        }

        override fun getGroup(listPosition: Int): Any {
            return expandableListTitle[listPosition]
        }

        override fun getGroupCount(): Int {
            return expandableListTitle.size
        }

        override fun getGroupId(listPosition: Int): Long {
            return listPosition.toLong()
        }

        override fun getGroupView(listPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup?): View? {
            var convertView: View? = convertView
            val listTitle = getGroup(listPosition) as String
            if (convertView == null) {
                val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = layoutInflater.inflate(R.layout.list_group, null)
            }
            val listTitleTextView = convertView
                    ?.findViewById(R.id.listTitle) as TextView
            listTitleTextView.setTypeface(null, Typeface.BOLD)
            listTitleTextView.text = listTitle
            return convertView
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
            return true
        }

        init {
            this.context = context
            this.expandableListTitle = expandableListTitle
            this.expandableListDetail = expandableListDetail
        }
    }
}

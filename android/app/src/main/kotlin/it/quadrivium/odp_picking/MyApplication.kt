package it.quadrivium.MesPicking

import android.content.Context
import io.flutter.app.FlutterApplication

class MyApplication : FlutterApplication() {
    override fun onCreate() {
        super.onCreate()
        globalInstance = this
    }

    override fun getApplicationContext(): Context? {
        return super.getApplicationContext()
    }

    companion object {
        var globalInstance: MyApplication? = null
        fun getInstance(): MyApplication? { return globalInstance }
    }
}
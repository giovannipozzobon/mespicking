package it.quadrivium.MesPicking

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.util.Log

class MyBroadcastReceiver : BroadcastReceiver() {
    private val TAG = BroadcastReceiver::class.java.simpleName
    override fun onReceive(context: Context?, intent: Intent) {
        val action = intent.action
        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            var state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            when (state)
            {
                BluetoothAdapter.STATE_OFF -> Log.d(TAG, "Bluetooth off")
                BluetoothAdapter.STATE_TURNING_OFF -> Log.d(TAG, "Bluetooth turning off")
                BluetoothAdapter.STATE_ON -> Log.d(TAG, "Bluetooth on")
                BluetoothAdapter.STATE_TURNING_ON -> Log.d(TAG, "Bluetooth turning on")
            }
        } else if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
            var state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
            when (state)
            {
                WifiManager.WIFI_STATE_ENABLED -> Log.d(TAG, "WI-FI on")
                WifiManager.WIFI_STATE_DISABLED -> Log.d(TAG, "WI-FI off")
            }
        }
    }
}
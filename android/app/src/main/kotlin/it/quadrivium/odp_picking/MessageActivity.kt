package it.quadrivium.MesPicking

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.*
import android.service.notification.StatusBarNotification
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import it.quadrivium.MesPicking.Utils.setBadgeCount
import org.json.JSONArray
import org.json.JSONException
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


class MessageActivity : AppCompatActivity() {

    companion object {
        private lateinit var listView: ListView

        private var adapter: CustomAdapter? = null

        private var listaMessaggi: MutableList<Messaggio> = mutableListOf()
        
        private lateinit var node: String
        private lateinit var token: String
        private var id_notifica: Int = 0
        private var nuovi: Int = 0

        lateinit var mainHandler: Handler

        private var ricerca: String = ""

        private var mNotificationsCount = 0

        private lateinit var searchView: SearchView

        private lateinit var searchItem: MenuItem
        private lateinit var notificationsItem: MenuItem

        private lateinit var menuToolbar: Menu

        private lateinit var notificationsIcon: LayerDrawable
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
        setSupportActionBar(findViewById(R.id.toolbar_actionbar))

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        getSupportActionBar()?.setTitle("Lista messaggi")
        
        adapter = null

        ricerca = ""

        mainHandler = Handler(Looper.getMainLooper())

        node = intent.getStringExtra("node")
        token = intent.getStringExtra("token")
        id_notifica = intent.getIntExtra("id_notifica",0)
        nuovi = intent.getIntExtra("nuovi",0)

        try {
            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            var notifications: Array<StatusBarNotification>? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                notifications = notificationManager.getActiveNotifications()
            } else {
                notificationManager.cancel(id_notifica)
            }
            if (notifications != null) {
                for (notification in notifications) {
                    if (notification.id == id_notifica) {
                        notificationManager.cancel(id_notifica)
                    }
                }
            }

            val message = Message()
            message.obj = "stop"
            message.what = 1
            ChannelParams.mHandler?.sendMessage(message)
        }
        catch (e: Exception) {
            Log.d("Notifica", e.toString())
        }
        
        listView = findViewById(R.id.message_listView)

        mainHandler.post(object : Runnable {
            override fun run() {
                try {
                    val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.cancel(id_notifica)
                }
                catch (e: Exception) {
                    Log.d("Notifica", e.toString())
                }
                getJSON(node + "wms/lista_messaggi_ricevuti?origine_chiamata=&id_messaggio=0&search=$ricerca&nuovi=$nuovi", token, id_notifica)
                mainHandler.postDelayed(this, 3000)
            }
        })
    }

    override fun onBackPressed() {
        if (mainHandler != null) {
            mainHandler.removeCallbacksAndMessages(null)
        }

        val message = Message()
        message.obj = "start"
        message.what = 1
        ChannelParams.mHandler?.sendMessage(message)
        
        super.onBackPressed()
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuToolbar = menu!!
        menuInflater.inflate(R.menu.main_menu, menu)

        searchItem = menu.findItem(R.id.action_search)!!
        searchView = searchItem.actionView as SearchView

        notificationsItem = menu.findItem(R.id.action_notifications)
        notificationsIcon = notificationsItem.icon as LayerDrawable

        setBadgeCount(this, notificationsIcon, mNotificationsCount)

        val expandListener = object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                Log.d("SearchViewToolbar", "SearchView collapsed")
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                Log.d("SearchViewToolbar", "SearchView expanded")
                return true
            }
        }

        searchItem.setOnActionExpandListener(expandListener)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d("SeatchViewToolbar", "SearchView text submitted")
                callSearch(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("SearchViewToolbar", "SearchView text changed")
                callSearch(newText)
                return true
            }

            fun callSearch(query: String?) {
                Log.d("SearchViewToolbar", "Valore SearchView: $query")
                if (query != null) {
                    ricerca = query
                }
            }
        })

        for (i in 0 until menu!!.size()) {
            val item = menu.getItem(i).setVisible(true)
            val drawable: Drawable? = menu.getItem(i).icon
            if (drawable != null) {
                drawable.mutate()
                drawable.setColorFilter(resources.getColor(R.color.colorText), PorterDuff.Mode.SRC_ATOP)
            }
        }

        return true
    }

    private fun updateNotificationsBadge(count: Int) {
        mNotificationsCount = count

        notificationsItem = menuToolbar.findItem(R.id.action_notifications)
        notificationsIcon = notificationsItem.icon as LayerDrawable

        setBadgeCount(this, notificationsIcon, mNotificationsCount)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_search -> {
            // Implementare gestione ricerca appbar
            true
        }

        R.id.action_notifications -> {
            // Implementare action click counter
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }


    private fun getJSON(urlGET: String, token: String, id_notifica: Int) {
        class GetJSON : AsyncTask<Void?, Void?, String?>() {
            override fun onPreExecute() {
                super.onPreExecute()
            }

            override fun onPostExecute(dataResult: String?) {
                super.onPostExecute(dataResult)
                //Toast.makeText(applicationContext, dataResult, Toast.LENGTH_SHORT).show()
                try {
                    if (dataResult != null) {
                        loadIntoListView(dataResult)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun doInBackground(vararg params: Void?): String? {
                return try {
                    val url = URL(urlGET)
                    val con = url.openConnection() as HttpURLConnection
                    con.setRequestProperty("Authorization", token)
                    val sb = StringBuilder()
                    val bufferedReader = BufferedReader(InputStreamReader(con.inputStream))
                    var line: String? = null
                    while ({ line = bufferedReader.readLine(); line }() != null) {
                        sb.append("""$line""".trimIndent())
                    }
                    sb.toString()
                } catch (e: Exception) {
                    Log.d("AsyncTaskGET", e.toString())
                    null
                }
            }
        }

        val getJSON = GetJSON()
        getJSON.execute()
    }

    @Throws(JSONException::class)
    private fun loadIntoListView(json: String) {
        val jsonArray = JSONArray(json)
        
        updateNotificationsBadge(jsonArray.length())
        
        val id_messaggio = arrayOfNulls<Int>(jsonArray.length())
        val utente_descr = arrayOfNulls<String>(jsonArray.length())
        val gruppo_descr = arrayOfNulls<String>(jsonArray.length())
        val data_ricezione = arrayOfNulls<String>(jsonArray.length())
        val data_lettura = arrayOfNulls<String>(jsonArray.length())
        val oggetto = arrayOfNulls<String>(jsonArray.length())
        val messaggio = arrayOfNulls<String>(jsonArray.length())
        val priorita = arrayOfNulls<String>(jsonArray.length())
        val da_leggere = arrayOfNulls<Boolean>(jsonArray.length())
        val allegati = arrayOfNulls<String>(jsonArray.length())

        listaMessaggi.removeAll(listaMessaggi)

        for (i in 0 until jsonArray.length()) {
            val obj = jsonArray.getJSONObject(i)

            id_messaggio[i] = obj.getInt("id")
            utente_descr[i] = obj.getString("utente_descr")
            gruppo_descr[i] = obj.getString("gruppo_descr")
            data_ricezione[i] = obj.getString("data_ricezione")
            data_lettura[i] = obj.getString("data_lettura")
            oggetto[i] = obj.getString("oggetto")
            messaggio[i] = obj.getString("messaggio")
            priorita[i] = obj.getString("priorita")
            da_leggere[i] = obj.getBoolean("da_leggere")
            allegati[i] = obj.getString("allegati")

            val elemento = Messaggio(
                    obj.getInt("id"),
            obj.getString("utente_descr"),
            obj.getString("gruppo_descr"),
            obj.getString("data_ricezione"),
            obj.getString("data_lettura"),
            obj.getString("oggetto"),
            obj.getString("messaggio"),
            obj.getString("priorita"),
            obj.getBoolean("da_leggere"),
            obj.getString("allegati")
            )

            listaMessaggi.add(elemento)
        }

        val listaSenzaDoppioni = listaMessaggi.distinctBy { it.id; }

        listaMessaggi.removeAll(listaMessaggi)

        for (item in listaSenzaDoppioni)
        {
            listaMessaggi.add(item)
        }

        if (adapter == null) {
            adapter = CustomAdapter(this, R.layout.activity_message, listaMessaggi)
            listView.adapter = adapter
        }
        else
        {
            adapter!!.notifyDataSetChanged()
        }

        listView.setOnItemClickListener { parent, view, position, id ->
            var item: Messaggio = parent.getItemAtPosition(position) as Messaggio
            val messageDetailsIntent = Intent(this, MessageDetailsActivity::class.java)
            messageDetailsIntent.putExtra("id_messaggio", item.id)
            messageDetailsIntent.putExtra("utente_descr", item.utente_descr)
            messageDetailsIntent.putExtra("gruppo_descr", item.gruppo_descr)
            messageDetailsIntent.putExtra("data_ricezione", item.data_ricezione)
            messageDetailsIntent.putExtra("data_lettura", item.data_lettura)
            messageDetailsIntent.putExtra("oggetto", item.oggetto)
            messageDetailsIntent.putExtra("messaggio", item.messaggio)
            messageDetailsIntent.putExtra("priorita", item.priorita)
            messageDetailsIntent.putExtra("da_leggere", item.da_leggere)
            messageDetailsIntent.putExtra("allegati", item.allegati)
            messageDetailsIntent.putExtra("node", node)
            messageDetailsIntent.putExtra("token", token)
            messageDetailsIntent.putExtra("id_notifica", id_notifica)
            messageDetailsIntent.putExtra("nuovi", nuovi)
            startActivity(messageDetailsIntent)
        }
    }
}

class CustomAdapter(context: Context, textViewResourceId: Int,
                    objects: List<Messaggio>) : ArrayAdapter<Messaggio?>(context, textViewResourceId, objects) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var listView = convertView
        if (listView == null)
        {
            listView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        val messaggio = getItem(position) as Messaggio
        val oggetto = listView?.findViewById(R.id.oggetto) as TextView
        val testo = listView.findViewById(R.id.testo) as TextView
        val utente = listView.findViewById(R.id.mittente) as TextView
        val ufficio = listView.findViewById(R.id.ufficio_mittente) as TextView
        val priorita = listView.findViewById(R.id.priorita) as TextView
        val data = listView.findViewById(R.id.data) as TextView
        val imgUnread = listView.findViewById(R.id.list_image) as ImageView
        val imgAlreadyRead = listView.findViewById(R.id.list_image2) as ImageView
        if (messaggio.getMessaggioDaLeggere() == true)
        {
            imgUnread.setVisibility(View.VISIBLE)
            imgAlreadyRead.setVisibility(View.GONE)
            oggetto.setTypeface(null, Typeface.BOLD)
            /*testo.setTypeface(null, Typeface.BOLD)
            utente.setTypeface(null, Typeface.BOLD)
            ufficio.setTypeface(null, Typeface.BOLD)
            priorita.setTypeface(null, Typeface.BOLD)
            data.setTypeface(null, Typeface.BOLD)*/
        }
        else
        {
            imgUnread.setVisibility(View.GONE)
            imgAlreadyRead.setVisibility(View.VISIBLE)
            oggetto.setTypeface(null, Typeface.NORMAL)
            /*testo.setTypeface(null, Typeface.NORMAL)
            utente.setTypeface(null, Typeface.NORMAL)
            ufficio.setTypeface(null, Typeface.NORMAL)
            priorita.setTypeface(null, Typeface.NORMAL)
            data.setTypeface(null, Typeface.NORMAL)*/
        }
        oggetto.text = "Oggetto: " + messaggio.getOggettoMessaggio()
        testo.text = "Testo: " + messaggio.getMessaggioUtente()
        utente.text = "Utente: " + messaggio.getUtenteDescr()
        ufficio.text = "Ufficio: " + messaggio.getGruppoDescr()
        priorita.text = "Priorità: " + messaggio.getPrioritaMessaggio()
        data.text = "Data ricezione: " + messaggio.getDataRicezione()
        return listView
    }
}

class Messaggio {
    var id: Int = 0
    var utente_descr: String = ""
    var gruppo_descr: String = ""
    var data_ricezione: String = ""
    var data_lettura: String = ""
    var oggetto: String = ""
    var messaggio: String = ""
    var priorita: String = ""
    var da_leggere: Boolean = false
    var allegati: String = ""

    constructor(id: Int, utente_descr: String, gruppo_descr: String, data_ricezione: String, data_lettura: String, oggetto: String, messaggio: String, priorita: String, da_leggere: Boolean, allegati: String) {
        this.id = id
        this.utente_descr = utente_descr
        this.gruppo_descr = gruppo_descr
        this.data_ricezione = data_ricezione
        this.data_lettura = data_lettura
        this.oggetto = oggetto
        this.messaggio = messaggio
        this.priorita = priorita
        this.da_leggere = da_leggere
        this.allegati = allegati
    }

    fun getIdMessaggio(): Int {
        return this.id
    }

    fun setIdMessaggio(id: Int) {
        this.id = id
    }

    fun getUtenteDescr(): String? {
        return this.utente_descr
    }

    fun setUtenteDescr(utente_descr: String) {
        this.utente_descr = utente_descr
    }

    fun getGruppoDescr(): String? {
        return this.gruppo_descr
    }

    fun setGruppoDescr(mRelease: String) {
        this.gruppo_descr = gruppo_descr
    }

    fun getDataRicezione(): String? {
        return this.data_ricezione
    }

    fun setDataRicezione(data_ricezione: String) {
        this.data_ricezione = data_ricezione
    }

    fun getDataLettura(): String? {
        return this.data_lettura
    }

    fun setDataLettura(data_lettura: String) {
        this.data_lettura = data_lettura
    }

    fun getOggettoMessaggio(): String? {
        return this.oggetto
    }

    fun setOggettoMessaggio(oggetto: String) {
        this.oggetto = oggetto
    }

    fun getMessaggioUtente(): String? {
        return this.messaggio
    }

    fun setMessaggioUtente(messaggio: String) {
        this.messaggio = messaggio
    }

    fun getPrioritaMessaggio(): String? {
        return this.priorita
    }

    fun setPrioritaMessaggio(priorita: String) {
        this.priorita = priorita
    }

    fun getMessaggioDaLeggere(): Boolean {
        return this.da_leggere
    }

    fun setMessaggioDaLeggere(da_leggere: Boolean) {
        this.da_leggere = da_leggere
    }

    fun getAllegatiMessaggio(): String? {
        return this.allegati
    }

    fun setAllegatiMessaggio(allegati: String) {
        this.allegati = allegati
    }
}
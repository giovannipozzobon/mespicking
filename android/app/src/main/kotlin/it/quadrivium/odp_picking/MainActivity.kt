package it.quadrivium.MesPicking

import android.app.*
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.*
import android.service.notification.StatusBarNotification
import android.util.Log
import android.view.Gravity
import android.webkit.MimeTypeMap
import android.widget.Toast
//import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.FileProvider
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.io.File


object ChannelParams {
  var mHandler: Handler? = null
  var flutterChannel: MethodChannel? = null
}

class MainActivity: FlutterActivity() {

  private val myBroadcastReceiver: BroadcastReceiver = MyBroadcastReceiver()

  private lateinit var mService: MyService

  private var mBound: Boolean = false
  private var controlloBackground: Boolean = false

  private var updateApkPath: String = ""
  private var updateResult: MethodChannel.Result? = null

  companion object {
    const val CHANNEL = "CANALE"
    const val CANALE_NOTIFICHE = "CANALE NOTIFICHE"
    const val NOME_CANALE_NOTIFICHE = "Notifiche"
    const val DESCR_CANALE_NOTIFICHE = "Canale notifiche capo reparto"
    var systemConfig: Context? = null
  }

  //@RequiresApi(Build.VERSION_CODES.M)
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    Thread.setDefaultUncaughtExceptionHandler(MyExceptionHandler(this))
    GeneratedPluginRegistrant.registerWith(this)

    systemConfig = this

    createNotificationChannel()

    val intentFilter = IntentFilter()
    intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
    intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
    registerReceiver(myBroadcastReceiver, intentFilter)

    if (getIntent().getBooleanExtra("crash", false)) {
      val toast = Toast.makeText(applicationContext, "App restarted after crash", Toast.LENGTH_SHORT)
      toast.setGravity(Gravity.CENTER, 0, 0)
      toast.show()
    }

    ChannelParams.flutterChannel = MethodChannel(flutterView, CHANNEL)

    ChannelParams.flutterChannel!!.setMethodCallHandler { call, result ->
      if (call.method == "testChannel") {
        result.success("Connessione canale flutter/java stabilita!")
      }
      else if (call.method == "OpenAnotherApplication") {
        val parametri: String? = call.argument("parametri")
        val launchIntent = Intent()
        launchIntent.component = ComponentName("it.quadrivium.roncatodropshipping", "it.quadrivium.roncatodropshipping.MainActivity")
        if (launchIntent != null) {
          launchIntent.action = Intent.ACTION_SEND
          launchIntent.putExtra(Intent.EXTRA_TEXT, parametri)
          launchIntent.type = "text/plain"
          try {
            startActivity(launchIntent)
            result.success("Applicazione aperta e token inviato!")
          }
          catch (e: Exception)
          {
            val toast = Toast.makeText(applicationContext, "Launch Intent not available!\nException: " + e.toString(), Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            result.success("Launch Intent not available!\nException: " + e.toString())
          }
        } else {
          val toast = Toast.makeText(applicationContext, "Launch Intent not available!\nException: Launch Intent equals to null", Toast.LENGTH_SHORT)
          toast.setGravity(Gravity.CENTER, 0, 0);
          toast.show();
          result.success("Launch Intent not available!\nException: Launch Intent equals to null")
        }
      }
      else if (call.method == "InstallAppWMSUpdate") {
        updateApkPath = call.argument("apkPath")!!
        updateResult = result

        ///// INIZIO TEST

        /*val uninstallIntent = Intent(Intent.ACTION_DELETE)
        uninstallIntent.data = Uri.parse("package:$packageName")
        startActivityForResult(uninstallIntent, 1234)*/

        ///// FINE TEST

        // INIZIO CODICE PRECEDENTE

        /*val contentUri = FileProvider.getUriForFile(this@MainActivity, "$packageName.provider", file)
        val installIntent = Intent(Intent.ACTION_VIEW)
        installIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        installIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        installIntent.data = contentUri*/

        val file = File(updateApkPath)
        val installIntent: Intent

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
          val contentUri = FileProvider.getUriForFile(this@MainActivity, "$packageName.provider", file)
          installIntent = Intent(Intent.ACTION_INSTALL_PACKAGE)
          installIntent.data = contentUri
          installIntent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
          installIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        } else {
          val apkUri = Uri.fromFile(file)
          installIntent = Intent(Intent.ACTION_VIEW)
          val extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())
          val mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
          installIntent.setDataAndType(apkUri, mimetype)
          installIntent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
          installIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }

        try {
          ListenActivities(this).start()
          startActivity(installIntent)
        } catch (e: Exception) {
          val toast = Toast.makeText(applicationContext, "Update not available!\nException: " + e.toString(), Toast.LENGTH_SHORT)
          toast.setGravity(Gravity.CENTER, 0, 0);
          toast.show();
          updateResult?.success("Update not available!\nException: " + e.toString())
        }

        // FINE CODICE PRECEDENTE
      } 
      else if (call.method == "CloseApplication") {
        finish()
        System.exit(0)
      }
      else if (call.method == "RestartApplication") {
        triggerRestart()
      }
      else if (call.method == "BackgroundApplication") {
        controlloBackground = true
        moveTaskToBack(true)
      }
      else if (call.method == "OpenFileDownloaded") {
        val filePath: String? = call.argument("filePath")
        val myIntent = Intent(Intent.ACTION_VIEW)
        val file = File(filePath)

        val contentUri = FileProvider.getUriForFile(this@MainActivity, "$packageName.provider", file)
        myIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        val extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())
        val mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        myIntent.setDataAndType(contentUri, mimetype)
        startActivity(myIntent)
        result.success("File Aperto")
      }
      else if (call.method == "CheckExistApp") {
        val parametri: String? = call.argument("parametri")
        val arrayValori = parametri?.split(";")?.toTypedArray()
        val nomePacchetto = arrayValori?.get(0)
        val nomeApp = arrayValori?.get(1)
        val pm: PackageManager = this@MainActivity.getPackageManager()
        val isInstalled = isPackageInstalled(nomePacchetto.toString(), pm)
        result.success(isInstalled)
      }
      else if (call.method == "OpenAppGeneral") {
        val parametri: String? = call.argument("parametri")
        val arrayValori = parametri?.split(";")?.toTypedArray()
        val nomePacchetto = arrayValori?.get(0)
        val nomeApp = arrayValori?.get(1)
        val launchIntent = packageManager.getLaunchIntentForPackage(nomePacchetto)
        if (launchIntent != null) {
          try {
            startActivity(launchIntent)
            result.success("App " + nomeApp + " aperta")
          }
          catch (e: Exception)
          {
            Toast.makeText(this@MainActivity, "Exception: " + e.toString(), Toast.LENGTH_LONG).show()
          }
        } else {
          Toast.makeText(this@MainActivity, nomeApp + " is not installed on this device", Toast.LENGTH_LONG).show()
        }
      }
      else if (call.method == "ShowNotification") {
        val dettNotification: String? = call.argument("dettNotification")
        val arrayValori = dettNotification?.split(";")?.toTypedArray()
        val titolo = arrayValori?.get(0)
        val contenuto = arrayValori?.get(1)
        val id_notifica = arrayValori?.get(2)?.toInt()
        val token = arrayValori?.get(3)
        val node = arrayValori?.get(4)
        val nuovi = arrayValori?.get(5)?.toInt()
        if (titolo != null) {
          if (contenuto != null) {
            if (id_notifica != null) {
              if (token != null) {
                if (node != null) {
                  if (nuovi != null) {
                    sendNotification(titolo, contenuto, id_notifica, token, node, nuovi)
                  }
                  result.success("Notifica inviata all'utente!")
                }
              }
            }
          }
        }
      }
      else if (call.method == "ShowAllNotifications") {
        val dettNotification: String? = call.argument("dettAllNotifications")
        val arrayValori = dettNotification?.split(";")?.toTypedArray()
        val id_notifica = arrayValori?.get(0)?.toInt()
        val token = arrayValori?.get(1)
        val node = arrayValori?.get(2)
        val nuovi = arrayValori?.get(3)?.toInt()
        val messageIntent = Intent(this, MessageActivity::class.java)
        messageIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        messageIntent.putExtra("node", node)
        messageIntent.putExtra("token", token)
        messageIntent.putExtra("id_notifica", id_notifica)
        messageIntent.putExtra("nuovi", nuovi)
        startActivity(messageIntent)
        result.success("Maschera notifiche aperta!")
      }
      else if (call.method == "ShowNewNotifications") {
        val dettNotification: String? = call.argument("dettNewNotifications")
        val arrayValori = dettNotification?.split(";")?.toTypedArray()
        val id_notifica = arrayValori?.get(0)?.toInt()
        val token = arrayValori?.get(1)
        val node = arrayValori?.get(2)
        val nuovi = arrayValori?.get(3)?.toInt()
        val messageIntent = Intent(this, MessageActivity::class.java)
        messageIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        messageIntent.putExtra("node", node)
        messageIntent.putExtra("token", token)
        messageIntent.putExtra("id_notifica", id_notifica)
        messageIntent.putExtra("nuovi", nuovi)
        startActivity(messageIntent)
        result.success("Maschera notifiche aperta!")
      }
      else if (call.method == "DeleteNotification") {
        try {
          val NotificationID: String? = call.argument("notificationID")
          val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
          var notifications: Array<StatusBarNotification>? = null
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            notifications = notificationManager.getActiveNotifications()
          } else {
            if (NotificationID != null) {
              notificationManager.cancel(NotificationID.toInt())
            }
          }
          if (notifications != null) {
            for (notification in notifications) {
              if (NotificationID != null) {
                if (notification.id == NotificationID.toInt()) {
                  notificationManager.cancel(NotificationID.toInt())
                }
              }
            }
          }
          result.success("Delete notification successfull!")
        }
        catch (e: Exception) {
          val toast = Toast.makeText(applicationContext, "Error deleting notification!\nException: " + e.toString(), Toast.LENGTH_SHORT)
          toast.setGravity(Gravity.CENTER, 0, 0);
          toast.show();
          result.success("Error deleting notification!\nException: " + e.toString())
        }
      }
    }

    ChannelParams.mHandler = object : Handler() {
      override fun handleMessage(msg: Message) {
        if (msg.what === 1) {
          Log.d("Evento handler ricevuto", "Stop notifications timer")
          ChannelParams.flutterChannel!!.invokeMethod("stopNotificationsTimer", msg.obj.toString())
        } else if (msg.what === 2) {
          Log.d("Evento handler ricevuto", "Test data transfer")
          ChannelParams.flutterChannel!!.invokeMethod("testDataTransfer", msg.obj.toString())
        } else if (msg.what === 3) {
          Log.d("Evento handler ricevuto", "Background application")
          ChannelParams.flutterChannel!!.invokeMethod("BackgroundApplication", msg.obj.toString())
        }
      }
    }
  }

  private fun isPackageInstalled(packageName: String, packageManager: PackageManager): Boolean {
    return try {
      packageManager.getPackageInfo(packageName, 0)
      true
    } catch (e: PackageManager.NameNotFoundException) {
      false
    }
  }

  private fun triggerRestart() {
    val mStartActivity = Intent(this@MainActivity, MainActivity::class.java)
    val mPendingIntentId = 123456
    val mPendingIntent = PendingIntent.getActivity(this@MainActivity, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT)
    val mgr = this@MainActivity.getSystemService(ALARM_SERVICE) as AlarmManager
    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
    System.exit(0)
  }

  private fun createNotificationChannel() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      val importance = NotificationManager.IMPORTANCE_DEFAULT
      val channel = NotificationChannel(CANALE_NOTIFICHE, NOME_CANALE_NOTIFICHE, importance).apply {
        description = DESCR_CANALE_NOTIFICHE
      }
      val notificationManager: NotificationManager =
              getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
      notificationManager.createNotificationChannel(channel)
    }
  }

  private fun sendNotification(titolo: String, contenuto: String, id_notifica: Int, token: String, node: String, nuovi: Int) {
    /*var contatore: Int = 0
    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    var notifications: Array<StatusBarNotification>? = null
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
       notifications = notificationManager.getActiveNotifications()
    } else {*/
      createNotificationWithData(titolo, contenuto, id_notifica, token, node, nuovi)
    /*}
    if (notifications != null) {
      for (notification in notifications) {
        if (notification.id == id_notifica) {
          contatore = contatore + 1
        }
      }
    }
    if (contatore == 0)
    {
      createNotificationWithData(titolo, contenuto, id_notifica, token, node, nuovi)
    }*/
  }

  private fun createNotificationWithData(titolo: String, contenuto: String, id_notifica: Int, token: String, node: String, nuovi: Int) {
    val replyIntent = Intent(this, MainActivity::class.java)
    val stackBuilder: TaskStackBuilder = TaskStackBuilder.create(this)
    stackBuilder.addNextIntent(replyIntent)

    var intentForPending = Intent(this@MainActivity, MessageBroadcastReceiver::class.java)
    intentForPending.putExtra("id_notifica", id_notifica)
    intentForPending.putExtra("token", token)
    intentForPending.putExtra("node", node)
    intentForPending.putExtra("nuovi", nuovi)

    var PendingIntent: PendingIntent =
            PendingIntent.getBroadcast(
                    this@MainActivity,
                    id_notifica,
                    intentForPending,
                    PendingIntent.FLAG_ONE_SHOT)

    val newMessageNotification = NotificationCompat.Builder(this, CANALE_NOTIFICHE)
            .setSmallIcon(R.drawable.baseline_notifications_active_black_18dp)
            .setContentTitle(titolo)
            .setContentText(contenuto)
            .setContentIntent(PendingIntent)
            .setOnlyAlertOnce(true)
            .setAutoCancel(true)
            .build()

    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    with(NotificationManagerCompat.from(this)) {
      notificationManager.notify(id_notifica, newMessageNotification)
    }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
    /*if (resultCode == 1234) {
      Log.d(MainActivity::class.simpleName, "Uninstall activity result called")

      //// INIZIO TEST

      val file = File(updateApkPath)
      val installIntent: Intent

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        val contentUri = FileProvider.getUriForFile(this@MainActivity, "$packageName.provider", file)
        installIntent = Intent(Intent.ACTION_INSTALL_PACKAGE)
        installIntent.data = contentUri
        installIntent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
        installIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
      } else {
        val apkUri = Uri.fromFile(file)
        installIntent = Intent(Intent.ACTION_VIEW)
        val extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())
        val mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        installIntent.setDataAndType(apkUri, mimetype)
        installIntent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
        installIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
      }

      try {
        ListenActivities(this).start()
        startActivityForResult(installIntent, 5678)
      } catch (e: Exception) {
        val toast = Toast.makeText(applicationContext, "Update not available!\nException: " + e.toString(), Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        updateResult?.success("Update not available!\nException: " + e.toString())
      }

      //// FINE TEST
    }
    else if (resultCode == 5678)
    {
      Log.d(MainActivity::class.simpleName, "Install activity result called")

      //// INIZIO TEST

      val mStartActivity = Intent(this@MainActivity, MainActivity::class.java)
      val mPendingIntentId = 789101112
      val mPendingIntent = PendingIntent.getActivity(this@MainActivity, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT)
      val mgr = this@MainActivity.getSystemService(ALARM_SERVICE) as AlarmManager
      mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
      System.exit(0)
      updateResult?.success("Update completed, restart of application in progress...")

      ///// FINE TEST
    }*/
    super.onActivityResult(requestCode, resultCode, resultData)
  }

  override fun onResume() {
    super.onResume()
    if (controlloBackground) {
      controlloBackground = false
      val message = Message()
      message.obj = ""
      message.what = 3
      ChannelParams.mHandler?.sendMessage(message)
    }
  }

  override fun onPause() {
    super.onPause()
    Log.d(MainActivity::class.java.simpleName, "WMS App goes to background")
  }

  override fun onDestroy() {
    super.onDestroy()
    Log.d(MainActivity::class.java.simpleName, "WMS App destroyed")
    unregisterReceiver(myBroadcastReceiver)
  }

  override fun isFinishing(): Boolean {
    Log.d(MainActivity::class.java.simpleName, "WMS App is finishing")
    return super.isFinishing()
  }

  private val connection = object : ServiceConnection {

    override fun onServiceConnected(className: ComponentName, service: IBinder) {
      val binder = service as MyService.LocalBinder
      mService = binder.getService()
      mBound = true
    }

    override fun onServiceDisconnected(arg0: ComponentName) {
      mBound = false
    }
  }

  override fun onStart() {
    super.onStart()
    Intent(this, MyService::class.java).also { intent ->
      bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }
  }

  override fun onStop() {
    super.onStop()
    unbindService(connection)
    mBound = false
  }
}

class MessageBroadcastReceiver : BroadcastReceiver() {
  override fun onReceive(context: Context, intent: Intent?) {
    val id_notifica = intent?.extras?.getInt("id_notifica")
    val token = intent?.extras?.getString("token")
    val node = intent?.extras?.getString("node")
    val nuovi = intent?.extras?.getInt("nuovi", 0)

    /*val repliedNotification = NotificationCompat.Builder(context, MainActivity.CANALE_NOTIFICHE)
            .setSmallIcon(R.drawable.baseline_notifications_active_black_18dp)
            .setContentText("Notifica visualizzata!")
            .setTimeoutAfter(3000)
            .build()

    val serviceContext = Context.NOTIFICATION_SERVICE
    val notificationManager: NotificationManager = MainActivity.systemConfig?.getSystemService(serviceContext) as NotificationManager
    NotificationManagerCompat.from(context).apply {
      if (id_notifica != null) {
        notificationManager.notify(id_notifica, repliedNotification)
      }
    }*/

    val message = Message()
    message.obj = "stop"
    message.what = 1
    ChannelParams.mHandler?.sendMessage(message)

    val messageIntent = Intent(context, MessageActivity::class.java)
    messageIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    messageIntent.putExtra("node", node)
    messageIntent.putExtra("token", token)
    messageIntent.putExtra("id_notifica", id_notifica)
    messageIntent.putExtra("nuovi", nuovi)
    context.startActivity(messageIntent)
  }
}

internal class ListenActivities(con: Context?) : Thread() {
  var exit = false
  var am: ActivityManager? = null
  var context: Context? = con
  var msg: String? = ""
  override fun run() {
    Looper.prepare()
    while (!exit) {
      val taskInfo = am!!.getRunningTasks(MAX_PRIORITY)
      val activityName = taskInfo[0].topActivity.className
      var activityMsg = "CURRENT Activity ::" + activityName
      if (msg != activityMsg)
      {
        msg = activityMsg
        Log.d("topActivity", msg)
      }
    }
    Looper.loop()
  }

  init {
    context = con
    am = context!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
  }
}

class MyService : Service() {

  private val binder = LocalBinder()

  inner class LocalBinder : Binder() {
    fun getService(): MyService = this@MyService
  }

  override fun onBind(intent: Intent): IBinder {
    return binder
  }

  override fun onDestroy() {
    super.onDestroy()
    Log.d(MainActivity::class.java.simpleName, "Service destroyed")
  }

  override fun onTaskRemoved(rootIntent: Intent?) {
    super.onTaskRemoved(rootIntent)
    Log.d(MainActivity::class.java.simpleName, "Task removed from service")
  }
}
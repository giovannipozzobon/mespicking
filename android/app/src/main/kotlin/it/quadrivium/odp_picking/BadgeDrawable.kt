package it.quadrivium.MesPicking

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable

class BadgeDrawable(context: Context): Drawable() {

    companion object {
        private var mTextSize = 0f
        private var mBadgePaint: Paint? = null
        private var mTextPaint: Paint? = null
        private val mTxtRect: Rect = Rect()

        private var mCount = ""
        private var mWillDraw = false
    }

    init {
        mTextSize = context.getResources().getDimension(R.dimen.badge_text_size)
        mBadgePaint = Paint()
        mBadgePaint!!.color = Color.rgb(255, 165, 0)
        mBadgePaint!!.isAntiAlias = true
        mBadgePaint!!.style = Paint.Style.FILL
        mTextPaint = Paint()
        mTextPaint!!.color = Color.BLACK
        mTextPaint!!.typeface = Typeface.DEFAULT_BOLD
        mTextPaint!!.textSize = mTextSize
        mTextPaint!!.isAntiAlias = true
        mTextPaint!!.textAlign = Paint.Align.CENTER
    }

    override fun draw(canvas: Canvas) {
        if (!mWillDraw) {
            return
        }
        val bounds = bounds
        val width = bounds.right - bounds.left.toFloat()
        val height = bounds.bottom - bounds.top.toFloat()

        // Position the badge in the top-right quadrant of the icon.
        val radius = (Math.min(width, height) / 2 - 1) / 2
        val centerX = width - radius - 1
        val centerY = radius + 1

        // Draw badge circle.
        canvas.drawCircle(centerX, centerY, radius, mBadgePaint)

        // Draw badge count text inside the circle.
        mTextPaint!!.getTextBounds(mCount, 0, mCount.length, mTxtRect)
        val textHeight = (mTxtRect.bottom - mTxtRect.top).toFloat()
        val textY = centerY + textHeight / 2f
        canvas.drawText(mCount, centerX, textY, mTextPaint)
    }

    /*
    Sets the count (i.e notifications) to display.
     */
    fun setCount(count: Int) {
        mCount = Integer.toString(count)

        // Only draw a badge if there are notifications.
        mWillDraw = count > 0;
        invalidateSelf();
    }

    override fun setAlpha(alpha: Int) {
        // do nothing
    }

    override fun setColorFilter(cf: ColorFilter?) {
        // do nothing
    }

    override fun getOpacity(): Int {
        return PixelFormat.UNKNOWN
    }
}

object Utils {
    fun setBadgeCount(context: Context?, icon: LayerDrawable, count: Int) {
        val badge: BadgeDrawable

        // Reuse drawable if possible
        val reuse = icon.findDrawableByLayerId(R.id.ic_badge)
        badge = if (reuse != null && reuse is BadgeDrawable) {
            reuse
        } else {
            BadgeDrawable(context!!)
        }
        badge.setCount(count)
        icon.mutate()
        icon.setDrawableByLayerId(R.id.ic_badge, badge)
    }
}
package it.quadrivium.MesPicking

import io.flutter.app.FlutterActivity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent

class MyExceptionHandler(private val activity: FlutterActivity) : Thread.UncaughtExceptionHandler {
    override fun uncaughtException(thread: Thread, ex: Throwable) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.putExtra("crash", true)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(MyApplication.getInstance()!!.baseContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val mgr = MyApplication.getInstance()!!.baseContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr[AlarmManager.RTC, System.currentTimeMillis() + 100] = pendingIntent
        activity.finish()
        System.exit(2)
    }

}
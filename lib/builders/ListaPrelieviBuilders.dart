import 'dart:async';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:MesPicking/model/json/ListaPrelievi.dart';
import 'package:MesPicking/service/Observer.dart';

class ListaPrelieviBuilder extends StatelessWidget {
  @required
  final Function builder;
  final Stream stream;

  ListaPrelieviBuilder({this.builder, this.stream});

  @override
  Widget build(BuildContext context) {
    return Observer<List<ListaPrelievi>>(
      stream: stream,
      onSuccess: (BuildContext context, List<ListaPrelievi> data) =>
          builder(context, data),
      onWaiting: (context) => LinearProgressIndicator(
        backgroundColor: Color(4286588201),
      ),
      onError: (context, error_msg) => Scaffold.of(context).showSnackBar(SnackBar(content: Text(error_msg), backgroundColor: Colors.red,)),
    );
  }
}

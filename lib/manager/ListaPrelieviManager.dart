import 'dart:async';
import 'package:MesPicking/model/json/ListaPrelievi.dart';
import 'package:MesPicking/service/ListaPrelieviService.dart';
import 'package:rxdart/rxdart.dart';

class ListaPrelieviManager {
  final PublishSubject<String> _filterSubject = PublishSubject<String>();
  final PublishSubject<int> _countSubject = PublishSubject<int>();
  final PublishSubject<List<ListaPrelievi>> _collectionSubject = PublishSubject();

  Sink<String> get inFilter => _filterSubject.sink;

  Observable<int> get count$ => _countSubject.stream;
  Observable<List<ListaPrelievi>> get browse$ => _collectionSubject.stream;
  Stream<List<ListaPrelievi>> get streambrowse$ => _collectionSubject.stream;

  ListaPrelieviManager()
  {
    _filterSubject
        .debounceTime(Duration(milliseconds: 500))
        .switchMap((filter) async*
    {
      yield await ListaPrelieviService.browse(filter: filter);
    }).listen((contents) async
    {
      _collectionSubject.add(contents);
    });

    _collectionSubject.listen((list) => _countSubject.add(list.length));
  }

  void dispose()
  {
    _countSubject.close();
    _filterSubject.close();
  }
}

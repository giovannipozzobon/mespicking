import 'package:MesPicking/ui/ElencoLogsScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:MesPicking/manager/Provider.dart';
import 'package:MesPicking/service/Overseer.dart';
import 'package:MesPicking/ui/AggiornamentiScreen.dart';
import 'package:MesPicking/ui/ElencoDownloadsScreen.dart';
import 'package:MesPicking/ui/InformazioniScreen.dart';
import 'package:MesPicking/ui/ListaPrelieviScreen.dart';
import 'package:MesPicking/ui/loginScreen.dart';
import 'package:MesPicking/ui/splashScreen.dart';

//void main() => runApp(MyApp());

main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();

    //setupLocator();

    await GlobalConfiguration().loadFromAsset("app_settings");
  }
  catch (e)
  {
    print(e.toString());
  }

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

class StatefulApp extends StatefulWidget {
  final Function onInit;
  final Widget child;
  const StatefulApp({@required this.onInit, @required this.child});
  @override
  _StatefulAppState createState() => _StatefulAppState();
}

class _StatefulAppState extends State<StatefulApp> {
  @override
  initState() {
    if (widget.onInit != null) {
      widget.onInit();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
      },
      onTapDown: (tapDown) {
      },
      onPanDown: (panDown) {
      },
      onPanUpdate: (panUpdate) {
      },
      child: Provider(
        data: Overseer(),
        child: MaterialApp(
          title: "MES Picking",
          theme: ThemeData(
              primaryColor: Color(4286588201)),
          routes: <String, WidgetBuilder>{
            "/LoginScreen": (BuildContext context) => LoginScreen(),
            "/ListaPrelieviScreen": (BuildContext context) => ListaPrelieviScreen(),
            "/InformazioniScreen": (BuildContext context) => InformazioniScreen(),
            "/AggiornamentoScreen": (BuildContext context) => AggiornamentoScreen(),
            "/ElencoDownloadsScreen": (BuildContext context) => ElencoDownloadsScreen(),
            "/ElencoLogsScreen": (BuildContext context) => ElencoLogsScreen()
          },
          home: SplashScreen(),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:MesPicking/manager/ListaPrelieviManager.dart';
import 'package:MesPicking/manager/Provider.dart';
import 'package:MesPicking/service/Observer.dart';

class ListaPrelieviCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ListaPrelieviManager manager = Provider.of(context).fetch(ListaPrelieviManager);

    return Observer<int>(
      stream: manager.count$,
      onSuccess: (context, data) {
        return Chip(
          label: Text(
            (data ?? 0).toString(),
            style: TextStyle(fontWeight: FontWeight.bold, color: Color(4286588201)),
          ),
          backgroundColor:Colors.white,
        );
      },
    );
  }
}

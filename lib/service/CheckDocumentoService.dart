import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:MesPicking/common/functions/getToken.dart';
import 'package:MesPicking/model/json/CheckDocumento.dart';

class CheckDocumentoService {
  static String _url = GlobalConfiguration().getString("asar_server") + GlobalConfiguration().getString("url_check_documento");

  static Future<List<CheckDocumento>> browse({String filter}) async {
    var token;

    await getToken().then((result) {
      token = result;
    });

    http.Response response = await http.get(
      "$_url?$filter",
      headers: {HttpHeaders.authorizationHeader: "$token"},
    );
    String content = response.body;

    List collection = json.decode(content);

    Iterable<CheckDocumento> _resources = collection.map((_) => CheckDocumento.fromJson(_));

    return _resources.toList();
  }
}
import 'package:MesPicking/manager/ListaPrelieviManager.dart';

class Overseer {
  Map<dynamic, dynamic> repository = {};

  Overseer() {
    register(ListaPrelieviManager, ListaPrelieviManager());
  }

  register(name, object) {
    repository[name] = object;
  }

  fetch(name) => repository[name];
}

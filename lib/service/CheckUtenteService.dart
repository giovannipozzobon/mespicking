import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:MesPicking/common/functions/getToken.dart';
import 'package:MesPicking/model/json/CheckUtente.dart';

class CheckUtenteService {
  static String _url = GlobalConfiguration().getString("asar_server") + GlobalConfiguration().getString("url_check_utente");

  static Future<List<CheckUtente>> browse({String filter}) async {
    var token;

    await getToken().then((result) {
      token = result;
    });

    http.Response response = await http.get(
      "$_url?$filter",
      headers: {HttpHeaders.authorizationHeader: "$token"},
    );
    String content = response.body;

    List collection = json.decode(content);

    Iterable<CheckUtente> _resources = collection.map((_) => CheckUtente.fromJson(_));

    return _resources.toList();
  }
}


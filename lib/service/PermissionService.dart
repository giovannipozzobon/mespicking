import 'package:permission_handler/permission_handler.dart';

class PermissionsService {
  static PermissionHandler permissionHandler = PermissionHandler();

  Future<bool> requestPermission(PermissionGroup permission) async {
    var result = await permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      return true;
    }
    return false;
  }

  Future<bool> requestStoragePermission({Function onPermissionDenied}) async {
    var granted = await requestPermission(PermissionGroup.storage);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  Future<bool> requestCameraPermission({Function onPermissionDenied}) async {
    var granted = await requestPermission(PermissionGroup.camera);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  Future<bool> requestMicrophonePermission({Function onPermissionDenied}) async {
    var granted = await requestPermission(PermissionGroup.microphone);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  Future<bool> hasStoragePermission() async {
    return hasPermission(PermissionGroup.storage);
  }

  Future<bool> hasCameraPermission() async {
    return hasPermission(PermissionGroup.camera);
  }

  Future<bool> hasMicrophonePermission() async {
    return hasPermission(PermissionGroup.microphone);
  }

  Future<bool> hasPermission(PermissionGroup permission) async {
    var permissionStatus =
    await permissionHandler.checkPermissionStatus(permission);
    return permissionStatus == PermissionStatus.granted;
  }
}
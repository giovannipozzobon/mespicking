import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:MesPicking/common/functions/LogManagment.dart';
import 'package:MesPicking/common/functions/LogScheduler.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:MesPicking/common/functions/ShowMessage.dart';
import 'package:MesPicking/common/functions/FileManagment.dart';
import 'package:MesPicking/model/json/CheckParametri.dart';
import 'package:MesPicking/service/CheckParametriService.dart';
import 'package:MesPicking/ui/AsarDrawer.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
String _percentuale = "0%";
String _valore_Percentuale = "0";
String _versione_attuale = "";
String _versione_disponibile = "";
bool _exist_aggiornamento = false;
bool _search_aggiornamento = false;
String _directory_applicazione = "/storage/emulated/0/Android/data/it.quadrivium.MesPicking/files";
StreamSubscription _streamSubscription = null;

List<CheckParametri> _parametri;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(4288362813);
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  PARAMETRI

class AggiornamentoScreen extends StatefulWidget {
  @override
  _AggiornamentoScreen createState() => new _AggiornamentoScreen();
}

class _AggiornamentoScreen extends State<AggiornamentoScreen> {
  final GlobalKey<ScaffoldState> scaffoldKEY = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKEY = new GlobalKey<FormState>();

  bool Initialized = false;

  static var httpClient = new HttpClient();

  AzzeraParametri() {
    setState(() {
      _parametri = new List<CheckParametri>();

      setState(() {
        _parametri.add(new CheckParametri(0, "NOMEAPKAPP", "", "MesPicking.apk", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEAPKANYDESK", "", "anydesk.apk", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEAPKPLUGIN", "", "anydesk-plugin.apk", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEFILEVERSIONE", "", "version.txt", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEMANUALE", "", "manuale.pdf", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "PERCORSONODEAPP", "", "http://13.93.33.246:8080/gmenode/ODP Picking/", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "MOSTRAELDOWNLOADS", "", "S", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "MOSTRAMANUALE", "", "S", "", 0, "", 0, "", ""));
      });
    });
  }

  Future getParametri() async {
    List<CheckParametri> check = await CheckParametriService.browse(filter: "");
    if (check.length > 0) {
      for (int i = 0; i < check.length; i++) {
        for (int j = 0; j < _parametri.length; j++) {
          if (check[i].codice_parametro == _parametri[j].codice_parametro) {
            setState(() {
              _parametri[j].id_parametro = check[i].id_parametro;
              _parametri[j].text_message = check[i].text_message;
              _parametri[j].num_message = check[i].num_message;
            });
          }
        }
      }
    }
  }

  Future getCurrentVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      _versione_attuale = packageInfo.version;
    });
  }

  Future displayInstallUpdate(BuildContext context, String _percorso) async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colore1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Text(
                "Aggiornamento scaricato, vuoi installare l'aggiornamento?",
                style: TextStyle(color: Colors.white)),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  "NO",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.pop(context, false);
                },
              ),
              FlatButton(
                child: Text(
                  "SI",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.pop(context, true);
                },
              )
            ],
          );
        }).then((val) async {
      if (val != null) {
        if (val) {
          await onInstallAggiornamento(_percorso);
        } else {
          setState(() {
            _exist_aggiornamento = false;
          });
          await getCurrentVersion();
        }
      } else {
        setState(() {
          _exist_aggiornamento = false;
        });
        await getCurrentVersion();
      }
    });
  }

  Future displayAggiornamentoDisponibile(BuildContext context) async {
    setState(() {
      _search_aggiornamento = false;
    });
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colore1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Text("Nuova versione disponibile", style: TextStyle(color: Colors.white)),
            content: Text(
                    "La nuova versione disponibile e' $_versione_disponibile. La versione attuale e' $_versione_attuale, vuoi effettuare l'aggiornamento?",
                style: TextStyle(color: Colors.white)),
            actions: <Widget>[
              FlatButton(
                child: Text("ESCI",
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                onPressed: () {
                  setState(() {
                    _exist_aggiornamento = false;
                  });
                  Navigator.pop(context, "ESCI");
                },
              ),
              FlatButton(
                child: Text("ANNULLA",
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                onPressed: () async {
                  setState(() {
                    _exist_aggiornamento = false;
                  });
                  Navigator.pop(context, false);
                },
              ),
              FlatButton(
                child: Text("AGGIORNA",
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                onPressed: () async {
                  setState(() {
                    _exist_aggiornamento = true;
                  });
                  Navigator.pop(context, true);
                },
              )
            ],
          );
        }).then((val) async {
      if (val != null) {
        if (val == "ESCI") {
          Navigator.pop(scaffoldKEY.currentContext, null);
          Navigator.pop(context, null);
        } else {
          if (val) {
            await DownloadInstallFile().then((val2) {});
          } else {
            try {
              await getCurrentVersion();
            } catch (e) {
              print(e.toString());
            }
          }
        }
      } else {
        try {
          await getCurrentVersion();
        } catch (e) {
          print(e.toString());
        }
      }
    });
  }

  Future displayNessunAggiornamento(BuildContext context) async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colore1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Nessun aggiornamento \ndell'applicazione disponibile",
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Divider(
                  color: Colors.white,
                  height: 4.0,
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  "CHIUDI",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }).then((val) async {
      try {
        await getCurrentVersion();
      } catch (e) {
        print(e.toString());
      }
    });
  }

  Future onInstallAggiornamento(String _percorso) async {
    WriteLogs();
    stopTimerLogTerminale();
    try {
      print(_percorso);
      final String result = await CanaleFlutterAndroid.platformMethodChannel
          .invokeMethod('InstallAppWMSUpdate', {'apkPath': '$_percorso'});
      print(result);
    } on PlatformException catch (e) {
      WriteLogs();
      startTimerLogTerminale();

      setState(() {
        _exist_aggiornamento = false;
      });
      print(e.message);
    }
  }

  Future DownloadInstallFile() async {
    String _percorso = "$_directory_applicazione/${_parametri[0].text_message}";

    try {
      final request = Request('GET', Uri.parse("${_parametri[5].text_message}${_parametri[0].text_message}"));
      final StreamedResponse response = await Client().send(request);

      List<List<int>> chunks = new List();
      int downloaded = 0;

      _streamSubscription = null;

      _streamSubscription = response.stream.listen((List<int> chunk) {
        setState(() {
          _valore_Percentuale = (downloaded / response.contentLength * 100).toStringAsFixed(0);
          _percentuale = (downloaded / response.contentLength * 100).toStringAsFixed(0) + "%";
        });
        chunks.add(chunk);
        downloaded += chunk.length;
      }, onDone: () async {
        setState(() {
          _valore_Percentuale =
              (downloaded / response.contentLength * 100).toStringAsFixed(0);
          _percentuale =
              (downloaded / response.contentLength * 100).toStringAsFixed(0) +
                  "%";
        });
        if (_exist_aggiornamento) {
          final Uint8List bytes = Uint8List(response.contentLength);
          int offset = 0;
          for (List<int> chunk in chunks) {
            bytes.setRange(offset, offset + chunk.length, chunk);
            offset += chunk.length;
          }
          File file = await new File(_percorso);
          await file.writeAsBytes(bytes).then((val) async {
            setState(() {
              _streamSubscription = null;
            });
            await displayInstallUpdate(context, _percorso);
          });
        }
      }, onError: (err) {
        setState(() {
          _exist_aggiornamento = false;
        });
        print(err);
        showMessage(scaffoldKEY, err.toString());
        setState(() {
          _streamSubscription = null;
        });
      }, cancelOnError: true);
    } catch (e) {
      setState(() {
        _exist_aggiornamento = false;
      });
      print(e);
      showMessage(scaffoldKEY, e.toString());
      setState(() {
        _streamSubscription = null;
      });
    }
  }

  Future CheckManuale() async {
    await downloadFile("${_parametri[5].text_message}${_parametri[4].text_message}", "$_directory_applicazione/${_parametri[4].text_message}");
  }

  Future CheckVersioneApp() async {
    getNode().then((v) async {
      String _percorso = "$_directory_applicazione/${_parametri[3].text_message}";
      await downloadFile(_parametri[5].text_message + _parametri[3].text_message, _percorso);

      if (await File(_percorso).exists()) {
      try {
      final _txt_file = await File(_percorso);
      String _content = await _txt_file.readAsString();
      _versione_disponibile = _content;
      } catch (e) {
      print("Eccezione: " + e.toString());
      }
      }

      await getCurrentVersion();

      if (_versione_disponibile != _versione_attuale) {
      await displayAggiornamentoDisponibile(context);
      } else {
      setState(() {
      _exist_aggiornamento = false;
      _search_aggiornamento = false;
      });
      await displayNessunAggiornamento(context);
      }
    });
  }

  onPressedManuale() async {
    if (await File("$_directory_applicazione/${_parametri[4].text_message}").exists()) {
      openFile("$_directory_applicazione/${_parametri[4].text_message}");
    } else {
      showMessage(scaffoldKEY, "Manuale non scaricato correttamente");
    }
  }

  getNode() async {
    _directory_applicazione = (await getExternalStorageDirectory()).path;
    await Future.delayed(const Duration(milliseconds: 1200));
  }

  onExitAggiornamento() async {
    if (_streamSubscription != null) {
      await _streamSubscription.pause();
      await onWillPop().then((val) async {
        if (val) {
          if (_streamSubscription != null) {
            await _streamSubscription.cancel().then((val) {
              Navigator.pop(context, null);
            });
          } else {
            return true;
          }
        } else {
          if (_streamSubscription != null) {
            await _streamSubscription.resume();
            return false;
          } else {
            return false;
          }
        }
      });
    } else {
      Navigator.pop(context, false);
    }
  }

  Future<bool> onWillPop() async {
    return await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colore1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Center(
                      child: Text("Attenzione",
                          style:
                              TextStyle(color: Colors.white, fontSize: 15.0)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Divider(
                  color: Colors.white,
                  height: 4.0,
                ),
              ],
            ),
            content: Text(
                "Sei sicuro di voler cancellare il download dell'aggiornamento?",
                style: TextStyle(color: Colors.white, fontSize: 22.0)),
            actions: <Widget>[
               FlatButton(
                child:  Text("NO",
                    style: TextStyle(color: Colors.white, fontSize: 16.0)),
                onPressed: () {
                  Navigator.pop(context, false);
                },
              ),
               FlatButton(
                child:  Text("SI",
                    style: TextStyle(color: Colors.white, fontSize: 16.0)),
                onPressed: () {
                  Navigator.pop(context, true);
                },
              )
            ],
          );
        }).then((val) {
      if (val != null) {
        if (val) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    });
  }

  onPressedRicercaAggiornamento() async {
    if (!_search_aggiornamento) {
      setState(() {
        _search_aggiornamento = true;
        _exist_aggiornamento = false;
      });
      try {
        await CheckVersioneApp().then((v) async {
          await CheckManuale();
        });
      } catch (e) {
        print(e.toString());
        setState(() {
          _search_aggiornamento =
          false;
          _exist_aggiornamento =
          false;
        });
      }
    }
  }

  onPressedElencoDownloads() {
    Navigator.of(context).pushNamed('/ElencoDownloadsScreen');
  }

  @override
  Widget build(BuildContext context) {
    if (!Initialized) {
      Initialized = true;

      AzzeraParametri();
      getParametri();

      setState(() {
        _exist_aggiornamento = false;
        _search_aggiornamento = false;
        _streamSubscription = null;
      });

      if (!_search_aggiornamento) {
        setState(() {
          _search_aggiornamento = true;
          _exist_aggiornamento = false;
        });
        CheckVersioneApp().then((v) async {
          await CheckManuale();
        });
      }
    }

    return WillPopScope(
      onWillPop: () {
        onExitAggiornamento();
      },
      child: Scaffold(
          key: scaffoldKEY,
          appBar: AppBar(
            backgroundColor: colore1,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                onExitAggiornamento();
              },
            ),
            title: Text("Aggiornamento"),
            actions: <Widget>[],
          ),
          body: SafeArea(
            bottom: false,
            top: false,
            child: Form(
              key: formKEY,
              child: Center(
                child: _exist_aggiornamento
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Scaricamento...",
                            style: TextStyle(
                              fontSize: 30.0,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          new CircularPercentIndicator(
                              radius: 125.0,
                              lineWidth: 10.0,
                              percent:
                                  (double.parse(_valore_Percentuale) / 100),
                              center: new Text(_percentuale),
                              backgroundColor: Colors.grey,
                              progressColor: colore1)
                        ],
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Center(
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Card(
                                          color: colore2,
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: Text(
                                              "Versione app: $_versione_attuale",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                  color: Colors.white),
                                            )
                                          )
                                        )
                                      )
                                    )
                                  )
                                ]
                              )
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: Row(
                                children: <Widget>[
                                  Visibility(
                                    visible: (!_exist_aggiornamento),
                                    child: Expanded(
                                      child: Center(
                                        child: FlatButton.icon(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(16.0),
                                                side:
                                                    BorderSide(color: colore1)),
                                            color: colore1,
                                            onPressed: () {
                                            onPressedRicercaAggiornamento();
                                            },
                                            label: Text(
                                                "Ricerca Aggiornamenti...",
                                                style: TextStyle(
                                                    color: Colors.white)),
                                            icon: Icon(Icons.refresh,
                                                color: Colors.white)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Visibility(
                                    visible: (_parametri[6].text_message == "S"),
                                    child: Expanded(
                                        child: Center(
                                            child: FlatButton.icon(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(16.0),
                                                    side: BorderSide(color: colore1)),
                                                color: colore1,
                                                onPressed: () {
                                                  onPressedElencoDownloads();
                                                },
                                                label: Text("Downloads Utili",
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                                icon: Icon(Icons.format_line_spacing,
                                                    color: Colors.white))
                                        )
                                    )
                                )
                              ]
                          ),
                            Row(
                              children: <Widget>[
                                Visibility(
                                    visible: (_parametri[7].text_message == "S"),
                                    child: Expanded(
                                        child: Center(
                                            child: FlatButton.icon(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(16.0),
                                                    side: BorderSide(color: colore1)),
                                                color: colore1,
                                                onPressed: () {
                                                  onPressedManuale();
                                                },
                                                label: Text("Manuale Applicazione",
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                                icon: Icon(Icons.library_books,
                                                    color: Colors.white))
                                        )
                                    )
                                )
                              ]
                          ),
                          ])
              )
            )
          ),
          floatingActionButton: Visibility(
              visible: (_streamSubscription != null),
              child: FloatingActionButton(
                heroTag: "Scarica aggiornamento app",
                tooltip: (_streamSubscription != null &&_streamSubscription.isPaused)
                    ? "Riprendi download"
                    : "Metti in pausa il download",
                child: Icon(
                  (_streamSubscription != null &&_streamSubscription.isPaused) ? Icons.play_arrow : Icons.pause,
                        color: Colors.white
                      ),
                backgroundColor: colore1,
                onPressed: () {
                  if (_streamSubscription != null &&_streamSubscription.isPaused) {
                    _streamSubscription.resume();
                  } else {
                    _streamSubscription.pause();
                  }
                }
              )))
    );
  }
}
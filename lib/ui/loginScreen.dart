import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:MesPicking/common/apifunctions/requestLoginAPI.dart';
import 'package:MesPicking/common/functions/TextFormFieldReplacing.dart';
import 'package:MesPicking/common/platform/platformScaffold.dart';
import 'package:MesPicking/common/widgets/basicDrawer.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(4288362813);
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
const URL = "http://www.quadrivium.it";
bool FileExist = false;
String IndirizzoIPServer = "";
String data = "";
String _directory = "";
String filePath = "";
File fileJson;
Map<String, dynamic> fileJsonContent;

bool _exec = false;

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> with TickerProviderStateMixin {
  final GlobalKey<FormState> formKEY = new GlobalKey<FormState>();
  
  AnimationController _animationController;
  Animation _appbarAnimation;
  Animation _titleAnimation;
  Animation _subtitleAnimation;
  Animation _usernameAnimation;
  Animation _txtusernameAnimation;
  Animation _passwordAnimation;
  Animation _txtpasswordAnimation;
  Animation _loginAnimation;

  TextEditingController _tc_username = TextEditingController();
  TextEditingController _tc_password = TextEditingController();
  
  FocusNode _fn_username = FocusNode();
  FocusNode _fn_password = FocusNode();
  FocusNode _fn_conferma = FocusNode();

  bool Initialized = false;

  Future getDirectoryApplicazione() async {
    _directory = (await getExternalStorageDirectory()).path;
    await Future.delayed(const Duration(milliseconds: 1000));
  }

  createFile(Map<String, dynamic> content, String dir, String fileName) {
    File file = new File(dir + "/" + fileName);
    file.createSync();
    file.writeAsStringSync(json.encode(content));
  }

  writeToFile(String key, String value) {
    Map<String, dynamic> content = {key: value};
    if (FileExist) {
      Map<String, dynamic> jsonFileContent =
      json.decode(fileJson.readAsStringSync());
      jsonFileContent.addAll(content);
      fileJson.writeAsStringSync(json.encode(jsonFileContent));
    } else {
      createFile(content, _directory, "config.json");
    }
  }

  CreaFileJSON() async {
    writeToFile("indirizzo_ip", "http://13.93.33.246:8080/wms/");
    //writeToFile("indirizzo_ip", "http://10.99.0.4:3010/");

    await Future.delayed(const Duration(milliseconds: 1000));

    this.setState(
    () => fileJsonContent = json.decode(fileJson.readAsStringSync()));
    print(fileJsonContent);
    print("Creazione file con il node");
  }

  Future CheckExistParams() async {
    await getDirectoryApplicazione().then((val) async {
      filePath = _directory.toString() + "/config.json";
      fileJson = new File(filePath);

      FileExist = await fileJson.exists();
      if (FileExist) {
        this.setState(() => fileJsonContent = json.decode(fileJson.readAsStringSync()));

        fileJsonContent.forEach((final String key, final value) async {
          if (key == "indirizzo_ip") {
            IndirizzoIPServer = value;
          }
        });
        GlobalConfiguration().updateValue("asar_server", "$IndirizzoIPServer");
      } else {
        CreaFileJSON();
      }
    });
  }

  setAnimation() {
    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2000));

    _appbarAnimation = Tween(begin: 0.0, end: 30.0).animate(CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.0, 0.125, curve: Curves.easeOut)));

    _titleAnimation = Tween(begin: 0.0, end: 30.0).animate(CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.125, 0.250, curve: Curves.easeOut)));

    _subtitleAnimation = Tween(begin: 0.0, end: 16.0).animate(CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.250, 0.375, curve: Curves.easeOut)));

    _usernameAnimation = Tween(begin: 0.0, end: 18.0).animate(CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.375, 0.500, curve: Curves.easeOut)));

    _txtusernameAnimation = Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: _animationController,
            curve: Interval(0.500, 0.625, curve: Curves.easeOut)));

    _passwordAnimation = Tween(begin: 0.0, end: 18.0).animate(CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.625, 0.750, curve: Curves.easeOut)));

    _txtpasswordAnimation = Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: _animationController,
            curve: Interval(0.750, 0.875, curve: Curves.easeOut)));

    _loginAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.875, 1.0, curve: Curves.easeOut)));

    _animationController.forward();
    _animationController.addListener(() {
      setState(() {});
    });
  }

  @override
  initState() {
    try {
      CheckExistParams();
      super.initState();
      _saveCurrentRoute("/LoginScreen");
      setAnimation();
    } catch (e) {
      print(e.toString());
    }
  }

  _saveCurrentRoute(String lastRoute) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('LastScreenRoute', lastRoute);
  }

  @override
  Widget build(BuildContext context) {
    if (!Initialized) {
      Initialized = true;

      setState(() {
        FocusScope.of(context).requestFocus(_fn_username);
      });
    }

    Future onSubmittedLogin() async {
      await CheckExistParams().then((val) async {
        data = await DefaultAssetBundle.of(context)
            .loadString("assets/cfg/app_settings.json");

        await GlobalConfiguration().updateValue("asar_server", "$IndirizzoIPServer");
        print("NODE: " + GlobalConfiguration().getString("asar_server"));
        SystemChannels.textInput.invokeMethod('TextInput.hide');

        await requestLoginAPI(context, _tc_username.text, _tc_password.text);
      });
    }

    onExitLogin() {
      if (Navigator.canPop(context)) {
        Navigator.of(context).pushNamedAndRemoveUntil('/LoginScreen', (Route<dynamic> route) => false);
      } else {
        Navigator.of(context).pushReplacementNamed('/LoginScreen');
      }
    }

    onTextFormField(String value, String textfield) {
      print(textfield + ": " + value);
      if (value.contains('\n')) {
        print('Enter premuto: ' + textfield);
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (textfield == "username") {
            _tc_username.text = ReplaceTextController(value);
            FocusScope.of(context).requestFocus(_fn_password);
          }
        });
      }
    }

    return WillPopScope(
      onWillPop: () {
        onExitLogin();
      },
      child: PlatformScaffold(
        drawer: BasicDrawer(
          callback: (isOpen) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
            });
          }
        ),
        appBar: AppBar(
          title: Text("LOGIN",
            style: TextStyle(
              fontSize: _appbarAnimation.value,
              color: Colors.black
            )
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
          ]
        ),
        backgroundColor: Colors.white,
        body: SafeArea(
          bottom: false,
          top: false,
          child: Form(
            key: formKEY,
            child: RawKeyboardListener(
              focusNode: FocusNode(),
              onKey: (RawKeyEvent event) async {
                if ((event.runtimeType == RawKeyDownEvent &&
                    event.logicalKey == LogicalKeyboardKey.enter)) {
                  if (_fn_username != null && _fn_username.hasFocus) {
                    setState(() {
                      FocusScope.of(context).requestFocus(_fn_password);
                    });
                  } else if (_fn_password != null && _fn_password.hasFocus) {
                    setState(() {
                      FocusScope.of(context).requestFocus(_fn_conferma);
                    });
                  } else if (_fn_conferma != null && _fn_conferma.hasFocus) {
                    await onSubmittedLogin();
                  }
                }
              },
              child: Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
                  child: ListView(
                    children: <Widget>[
                      Container(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 25.0),
                            child: Text(
                              "MES Picking",
                              style: TextStyle(
                                  fontSize: _titleAnimation.value,
                                  color: Colors.black)
                            )
                          )),
                      Text("Nome utente",
                        style: TextStyle(
                          fontSize: _usernameAnimation.value,
                          color: Colors.black,
                          fontWeight: FontWeight.bold
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                        child: Opacity(
                          opacity: _txtusernameAnimation.value,
                          child: /*TextField(
                            focusNode: _fn_username,
                            controller: _tc_username,
                            autofocus: true,
                            decoration: InputDecoration(
                              hintText: "Inserisci nome utente",
                            ),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),*/
                          TextFormField(
                            readOnly: false,
                            autofocus: false,
                            focusNode: _fn_username,
                            controller: _tc_username,
                            decoration: InputDecoration(
                              hintText: "Inserisci nome utente",
                              errorText: null,
                            ),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold
                            ),
                            maxLines: null,
                            autovalidate: true,
                            onChanged: (v) {
                              print("NODE: " + GlobalConfiguration().getString("asar_server"));
                            },
                            validator: (value) {
                              onTextFormField(value, "username");
                            }
                          )
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 0.0),
                        child: Text("Password",
                          style: TextStyle(
                            fontSize: _passwordAnimation.value,
                            color: Colors.black,
                            fontWeight: FontWeight.bold
                          )
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                        child: Opacity(
                          opacity: _txtpasswordAnimation.value,
                          child: TextField(
                            controller: _tc_password,
                            focusNode: _fn_password,
                            decoration: InputDecoration(
                              hintText: "Digita la password",
                            ),
                            obscureText: true,
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold
                            )
                          )
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 10.0),
                        child: Container(
                          height: 65.0,
                          child: Transform.scale(
                            scale: _loginAnimation.value,
                            child: RaisedButton(
                              focusNode: _fn_conferma,
                              focusColor: colore3,
                              onPressed: () async {
                                await onSubmittedLogin();
                              },
                              child: Text("LOGIN",style: TextStyle(color: Colors.white, fontSize: 22.0)),
                              color: colore1
                            )
                          )
                        )
                      )
                    ]
                  )
                )
              )
            )
          )
        )
      )
    );
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:MesPicking/common/functions/LogManagment.dart';
import 'package:MesPicking/service/ListaPrelieviService.dart';
import 'package:flutter/material.dart';
import 'package:MesPicking/common/functions/LogScheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:MesPicking/builders/ListaPrelieviBuilders.dart';
import 'package:MesPicking/common/functions/Numeric.dart';
import 'package:MesPicking/common/functions/ShowMessage.dart';
import 'package:MesPicking/common/functions/TextFormFieldReplacing.dart';
import 'package:MesPicking/common/functions/genericPost.dart';
import 'package:MesPicking/common/functions/genericPostCheckArticolo.dart';
import 'package:MesPicking/common/widgets/RoundedBorderPainter.dart';
import 'package:MesPicking/manager/ListaPrelieviManager.dart';
import 'package:MesPicking/manager/Provider.dart';
import 'package:MesPicking/model/json/CheckArticolo.dart';
import 'package:MesPicking/model/json/CheckDocumento.dart';
import 'package:MesPicking/model/json/CheckParametri.dart';
import 'package:MesPicking/model/json/ListaPrelievi.dart';
import 'package:MesPicking/model/json/PickingArticolo.dart';
import 'package:MesPicking/model/json/PostCheckArticolo.dart';
import 'package:MesPicking/service/CheckDocumentoService.dart';
import 'package:MesPicking/service/CheckParametriService.dart';
import 'package:MesPicking/service/Observer.dart';
import 'package:path_provider/path_provider.dart';
import 'package:popup_menu/popup_menu.dart';
import 'AsarDrawer.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
CheckDocumento _documento;
CheckArticolo _articolo;
List<CheckParametri> _parametri;
double _quantita_prelevata = 0;
List<DropdownMenuItem> _stati_dettaglio = [];
String _directory_applicazione = "/storage/emulated/0/Android/data/it.quadrivium.MesPicking/files";

String _f_ordinamento = "ORDINA_CELLA";
String _f_stato = "APERTI";
String _temp = "";
Timer _timer = null;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(0xffb83d3d); //4288362813
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);
Color colore7 = Colors.pinkAccent[100];
Color colore8 = Colors.lightGreen[200];
Color _colore_aperto_1 = Colors.green;
Color _colore_aperto_2 = Colors.lightGreen;
Color _colore_chiuso_1 = Colors.red;
Color _colore_chiuso_2 = Colors.redAccent;

class ListaPrelieviScreen extends StatefulWidget {
  @override
  _ListaPrelieviScreen createState() => new _ListaPrelieviScreen();
}

class _ListaPrelieviScreen extends State<ListaPrelieviScreen>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  BouncingScrollPhysics bouncingScroll = new BouncingScrollPhysics();
  Animation<Offset> _slideAnimation;
  GlobalKey<ScaffoldState> scaffoldKEY = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKEY = new GlobalKey<FormState>();
  GlobalKey<FormState> formKEY2 = new GlobalKey<FormState>();
  GlobalKey menuKEY = GlobalKey();

  TextEditingController _tc_articolo = new TextEditingController();
  TextEditingController _tc_quantita_prelevata = new TextEditingController();

  FocusNode _fn_articolo = new FocusNode();
  FocusNode _fn_quantita_prelevata = new FocusNode();
  FocusNode _fn_conferma_quantita_prelevata = new FocusNode();

  bool Initialized = false;

  Initialization() {
    AzzeraParametri();
    AzzeraDocumento();
    AzzeraArticolo();
    AzzeraStatiDettagli();

    getDirectoryApplicazione();
  }

  TextSelections() {
    _fn_articolo.addListener(() {
      if (_fn_articolo.hasFocus) {
        _tc_articolo.selection = TextSelection(
            baseOffset: 0, extentOffset: _tc_articolo.text.length);
      }
    });

    _fn_quantita_prelevata.addListener(() {
      if (_fn_quantita_prelevata.hasFocus) {
        _tc_quantita_prelevata.selection = TextSelection(
            baseOffset: 0, extentOffset: _tc_quantita_prelevata.text.length);
      }
    });
  }

  getDirectoryApplicazione() async {
    _directory_applicazione = (await getExternalStorageDirectory()).path;
    await Future.delayed(const Duration(milliseconds: 1200));
  }

  Future getParametri() async {
    try {
      List<CheckParametri> check = await CheckParametriService.browse(
          filter: "");
      if (check.length > 0) {
        for (int i = 0; i < check.length; i++) {
          for (int j = 0; j < _parametri.length; j++) {
            if (check[i].codice_parametro == _parametri[j].codice_parametro) {
              setState(() {
                _parametri[j].id_parametro = check[i].id_parametro;
                _parametri[j].text_message = check[i].text_message;
                _parametri[j].num_message = check[i].num_message;
              });
            }
          }
        }
      }

      setGlobalConfiguration();
    } catch(e) {
      addLog("Errore caricamento parametri: ${e.toString()}", "X", "ListaPrelieviScreen");
    }
  }

  setGlobalConfiguration() {
    setState(() {
      GlobalConfiguration().updateValue("tipo_log", _parametri[4].text_message);
      GlobalConfiguration().updateValue("nome_file_log", _parametri[5].text_message);
      GlobalConfiguration().updateValue("cartella_log_server", _parametri[6].text_message);
      GlobalConfiguration().updateValue("cartella_terminale", _directory_applicazione);
      GlobalConfiguration().updateValue("giorni_retenzione_file_log", _parametri[7].num_message);
      GlobalConfiguration().updateValue("time_upload_log_server", _parametri[6].num_message);
    });
  }

  getAnimations() {
    try {
      _animationController = AnimationController(
          vsync: this, duration: Duration(milliseconds: 1000));
      _slideAnimation =
          Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset.zero)
              .animate(CurvedAnimation(
              parent: _animationController,
              curve: Interval(0.0, 1.0, curve: Curves.elasticIn)));
    } catch(e) {
      addLog("Errore caricamento animazioni: ${e.toString()}", "X", "ListaPrelieviScreen");
    }
  }

  AzzeraArticolo() {
    if(_parametri[0].text_message == "TUTTI") {
      addLog("Azzeramento articolo", "E", "ListaPrelieviScreen");
    }

    setState(() {
      _articolo = new CheckArticolo("", "", 0, 0, "", 0, 0, 0, "", false);
      _tc_articolo.text = "";
    });
    AzzeraQuantitaPrelevata();
  }

  AzzeraDocumento() {
    if(_parametri[0].text_message == "TUTTI") {
      addLog("Azzeramento documento", "E", "ListaPrelieviScreen");
    }

    setState(() {
      _documento = new CheckDocumento(0, "", "", "", "", "", 0, 0, 0);
    });
  }

  AzzeraParametri() {
    addLog("Azzeramento parametri", "E", "ListaPrelieviScreen");

    setState(() {
      _parametri = new List<CheckParametri>();

      _parametri.add(new CheckParametri(
          0, "MOSTRAINFOID", "", "N", "", 0, "", 0, "", ""));
      _parametri.add(new CheckParametri(
          0, "MOSTRAPROPRIETA", "", "N", "", 0, "", 0, "", ""));
      _parametri.add(
          new CheckParametri(0, "TIMERODPREFRESH", "", "", "", 7000, "", 0, "", ""));
      _parametri.add(
          new CheckParametri(0, "MOSTRAMESSDEBUG", "", "N", "", 0, "", 0, "", ""));
      _parametri.add(new CheckParametri(
          0, "CATEGORIALOG", "", "TUTTI", "", 0, "", 0, "", ""));
      _parametri.add(new CheckParametri(
          0, "NOMEFILELOG", "", "log_mes_picking.txt", "", 0, "", 0, "", ""));
      _parametri.add(new CheckParametri(
          0, "NOMELOGSERVER", "", "http://13.93.33.246:8080/gmenode/ODP Picking/logs", "", 60, "", 0, "", ""));
      _parametri.add(new CheckParametri(
          0, "LOGSRETENTIONDAY", "", "", "", 15, "", 0, "", ""));
    });
    setGlobalConfiguration();
  }

  AzzeraStatiDettagli() {
    if(_parametri[0].text_message == "TUTTI") {
      addLog("Azzeramento stati riga/dettaglio", "E", "ListaPrelieviScreen");
    }

    setState(() {
      _stati_dettaglio = [];

      _stati_dettaglio.add(DropdownMenuItem(
          child: Text("Aperto"), key: new Key("N"), value: "Aperto"));
      _stati_dettaglio.add(DropdownMenuItem(
          child: Text("Chiuso"), key: new Key("S"), value: "Chiuso"));
    });
  }

  AzzeraQuantitaPrelevata() {
    if(_parametri[0].text_message == "TUTTI") {
      addLog("Azzeramento quantita prelevata (in prelievo articolo)", "E", "ListaPrelieviScreen");
    }

    setState(() {
      _tc_quantita_prelevata.text = "";
      _quantita_prelevata = 0;
    });
  }

  @override
  initState() {
    super.initState();

    getAnimations();
  }

  @override
  Widget build(BuildContext context) {
    ListaPrelieviManager managerListaPrelievi = Provider.of(context).fetch(ListaPrelieviManager);

    stopTimerDocumento() async {
      try {
        if (_timer != null) {
          addLog("Timer stoppato", "E", "ListaPrelieviScreen");
          _timer.cancel();
          _timer = null;
        }
        await Future.delayed(const Duration(milliseconds: 500));
      }  catch(e) {
        addLog(
            "Errore stop timer: ${e.toString()}", "X",
            "ListaPrelieviScreen");
      }
    }

    setManager() {
      addLog("Aggiornamento manager lista prelievi (id_testata: ${_documento.id_testata.toString()}, ordinamento: $_f_ordinamento stato: $_f_stato)", "E", "ListaPrelieviScreen");

      try {
        managerListaPrelievi.inFilter.add(
            "id_documento=${_documento.id_testata
                .toString()}&ordinamento=$_f_ordinamento&stato=$_f_stato&mode=LISTA_PRELIEVI");
      } catch(e) {
        addLog("Errore aggiornamento manager lista prelievi: ${e.toString()}", "X", "ListaPrelieviScreen");
      }

      stopTimerDocumento();
    }

    displayMoltiDocumentiUtente(BuildContext context, int num_documenti) async {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog molti documenti per l'utente (trovati ${num_documenti.toString()} documenti associati)", "E", "ListaPrelieviScreen");
      }

      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Text(
                  "Ci sono ${num_documenti.toString()} ODP associati all'utente. Modificare l'assegnazione dal MES",
                  style: TextStyle(fontSize: 16.0, color: Colors.white)),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {},
                )
              ],
            );
          });
    }

    displayNessunaDettaglioAperto(BuildContext context) async {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog nessun dettaglio/riga aperto", "E", "ListaPrelieviScreen");
      }

      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Text("Nessun prelievo ancora aperto",
                  style: TextStyle(fontSize: 16.0, color: Colors.white)),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }

    Future validateDocumento(int id_documento) async {
      addLog("Validazione documento (id ${id_documento.toString()})", "E", "ListaPrelieviScreen");

      try {
        List<CheckDocumento> check = await CheckDocumentoService.browse(filter: "id_documento=${id_documento.toString()}");
        if (check.length == 0) {
          addLog("Nessun documento trovato (id ${id_documento.toString()})", "E", "ListaPrelieviScreen");
          AzzeraDocumento();
          if (_timer == null) {
            addLog("Partenza timer (perche nessun documento trovato)", "E", "ListaPrelieviScreen");
            _timer = Timer.periodic(
                Duration(milliseconds: _parametri[2].num_message), (
                timer) async {
              try {
                addLog("Esecuzione timer ricerca documenti", "E", "ListaPrelieviScreen");

                validateDocumento(_documento.id_testata);
              } catch (e) {
                print(e.toString());
                addLog("Errore esecuzione timer ricerca documenti: ${e.toString()}", "X", "ListaPrelieviScreen");
              }
            });
          }
        } else if (check.length > 1) {
          addLog("Piu' documenti trovati (num documenti ${check.length.toString()})", "E", "ListaPrelieviScreen");
          await stopTimerDocumento().then((v2) async {
            await displayMoltiDocumentiUtente(context, check.length)
                .then((v) async {
              if (_timer == null) {
                await Future.delayed(const Duration(milliseconds: 5000));
                Navigator.pop(context, true);
                addLog("Esecuzione timer ricerca documenti (2)", "E", "ListaPrelieviScreen");
                _timer = Timer.periodic(
                    Duration(milliseconds: _parametri[2].num_message), (
                    timer) async {
                  try {
                    validateDocumento(_documento.id_testata);
                  } catch (e) {
                    print(e.toString());
                    addLog("Errore esecuzione timer ricerca documenti: ${e.toString()}", "X", "ListaPrelieviScreen");
                  }
                });
              }
            });
          });
        } else {
          addLog("Documento trovato (id ${id_documento.toString()})", "E", "ListaPrelieviScreen");
          if (_timer != null) {
            await stopTimerDocumento().then((v) async {
              validateDocumento(0);
            });
          } else {
            if(_parametri[0].text_message == "TUTTI") {
              addLog("Assegnazione del documento alle variabili locali", "E", "ListaPrelieviScreen");
            }

            try {
              _documento.codice_testata = check[0].codice_testata;
              _documento.data_testata = check[0].data_testata;
              _documento.stato_testata = check[0].stato_testata;
              _documento.tipo_documento = check[0].tipo_documento;
              _documento.id_testata = check[0].id_testata;
              _documento.num_dettagli_aperti = check[0].num_dettagli_aperti;
              _documento.num_dettagli_chiusi = check[0].num_dettagli_chiusi;
              _documento.nome_utente = check[0].nome_utente;
              _documento.id_utente = check[0].id_utente;

              await Future.delayed(const Duration(milliseconds: 500));
              setState(() {});
            } catch (e) {
              print(e.toString());
              addLog(
                  "Errore assegnazione del documento alle variabili locali: ${e.toString()}",
                  "X", "ListaPrelieviScreen");
            }

            if (_documento.num_dettagli_aperti == 0) {
              addLog("Il documento non ha nessun dettaglio/riga aperto (id ${id_documento.toString()})", "E", "ListaPrelieviScreen");

              displayNessunaDettaglioAperto(context);
            }
            setManager();
          }
        }
      } catch(e) {
        addLog("Errore validazione documento (id ${id_documento.toString()}): ${e.toString()}", "X", "ListaPrelieviScreen");
      }
    }

    Future startTimerDocumento() async {
      if (_timer == null) {
        addLog("Partenza timer", "E", "ListaPrelieviScreen");
        _timer = Timer.periodic(Duration(milliseconds: _parametri[2].num_message), (timer) async {
          addLog("Richiamo timer (tipo 0)", "E", "ListaPrelieviScreen");
          try {
            validateDocumento(_documento.id_testata);
          } catch (e) {
            print(e.toString());
            addLog("Errore richiamo timer (tipo 0): ${e.toString()}", "X", "ListaPrelieviScreen");
          }
        });
      }
    }

    PostModificaRiga(ListaPrelievi l, String stato_dettaglio, double quantita_prelevata, String mode) async {
      addLog(
          "Post Modifica Riga (id dettaglio: ${l.id_dettaglio.toString()}, stato nuovo: ${stato_dettaglio}, quantita nuova: ${quantita_prelevata.toString()}, mode: $mode)", "E",
          "ListaPrelieviScreen");

      bool _done = false;

      try {
      String body;
      PickingArticolo d = new PickingArticolo(l.id_articolo, l.id_testata,
          quantita_prelevata, stato_dettaglio, mode);
      List<PickingArticolo> _lnc = new List<PickingArticolo>();
      _lnc.add(d);
      List jsonList = PickingArticolo.encondeToJson(_lnc);
      body = json.encode(jsonList);

      final Postresult = await genericPost(
          GlobalConfiguration().getString("url_picking_articolo"),
          body,
          context,
          scaffoldKEY,
          true);

      if (Postresult != null &&
          Postresult is PostReturn &&
          Postresult.result == true) {
        _done = true;
        addLog("Post eseguita con successo", "E", "ListaPrelieviScreen");

        WriteLogs();
      } else {
        addLog(
            "Errore ritorno post modifica dettaglio (id dettaglio: ${l.id_dettaglio.toString()}, stato nuovo: ${stato_dettaglio}, quantita nuova: ${quantita_prelevata.toString()}, mode: $mode): ${Postresult.error_msg}", "X",
            "ListaPrelieviScreen");
        showMessage(
            scaffoldKEY, "ERRORE PICKING ARTICOLO: " + Postresult.error_msg);
      }

      if (_done) {
        if (mode == "CAMBIA_STATO") {
          showMessageDuration(
              scaffoldKEY, "Stato riga cambiato", 1000, colore1);
        } else if (mode == "CAMBIA_QUANTITA_PRELEVATA") {
          showMessageDuration(
              scaffoldKEY,
              "Quantita' prelevata impostata a ${quantita_prelevata.toString()}",
              1000,
              colore1);
        }
        if(_parametri[0].text_message == "TUTTI") {
          addLog("Richiamo validazione documento in seguito a post corretta (id documento: ${_documento.id_testata.toString()})", "E", "ListaPrelieviScreen");
        }

        validateDocumento(_documento.id_testata);
      }
      } catch (e) {
        print(e.toString());
        addLog(
            "Errore codice post modifica riga (mode: $mode): ${e.toString()}", "X",
            "ListaPrelieviScreen");
      }
    }

    displayRigaTrasferita(BuildContext context) {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog dettaglio/riga gia' trasferito", "E", "ListaPrelieviScreen");
      }

     showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Text(
                "La riga di prelievo selezionata e' gia' stata trasferita al gestionale Embyon, pertanto non e' possibile modificarla",
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK",
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            );
          }).then((val) async {
        if (val != null) {
          if (val) {
          }
        }
      });
    }

    ConfermaModificaRiga(ListaPrelievi l, String stato_dettaglio, double quantita_prelevata, String mode) async {
      List<ListaPrelievi> check = await ListaPrelieviService.browse(filter: "id_documento=${l.id_dettaglio.toString()}&ordinamento=&stato=&mode=DETTAGLIO_SPECIFICO");
      if (check.length == 0) {
        showMessage(scaffoldKEY, "Riga di prelievo non valida");
      } else {
        if(check[0].fl_trasferito == "N") {
          if (mode == "CAMBIA_QUANTITA_PRELEVATA") {
            PostModificaRiga(l, "", quantita_prelevata, "CAMBIA_QUANTITA_PRELEVATA");
          } else if (mode == "CAMBIA_STATO") {
            PostModificaRiga(l, stato_dettaglio, 0, "CAMBIA_STATO");
          } else if (mode == "ANNULLA_PRELIEVO") {
            PostModificaRiga(l, "", 0, "ANNULLA_PRELIEVO");
          }
        } else {
          displayRigaTrasferita(context);
          //showMessageDuration(scaffoldKEY, "La riga di prelievo selezionata e' gia' stata trasferita al gestionale Embyon, pertanto non e' possibile modificarla",5000);
        }
      }
    }

    onPressedInfoRiga(ListaPrelievi l) {
      String message =
          "ID Articolo: ${l.id_articolo.toString()} \nID Ubicazione: ${l.id_cella.toString()} \nID Dettaglio: ${l.id_dettaglio.toString()}";
      showMessage(scaffoldKEY, message, colore1);
    }

    onPressedInfo() {
      String message =
          "ID Documento: ${_documento.id_testata.toString()} \nID Utente: ${_documento.id_utente.toString()}";
      showMessage(scaffoldKEY, message, colore1);
    }

    displayCambiaQuantitaPrelevataRiga(BuildContext context, ListaPrelievi l) {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog cambio quantita' prelevata", "E", "ListaPrelieviScreen");
      }

      TextEditingController _tc_quantita_prelevata2 =
          new TextEditingController();
      FocusNode _fn_quantita_prelevata2 = new FocusNode();

      onSubmittedQuantita(String quantita_string, String mode) {
        addLog("Esecuzione submitt cambio quantita' prelevata (id dettaglio: ${l.id_dettaglio.toString()}, quantita: $quantita_string)", "E", "ListaPrelieviScreen");

        quantita_string = ReplaceTextController(quantita_string);
        _tc_quantita_prelevata2.text =
            ReplaceTextController(_tc_quantita_prelevata2.text);

        double quantita = 0;

        if (NumericField(quantita_string) == true) {
          if (double.parse(quantita_string) > -1) {
            setState(() {
              quantita = double.parse(quantita_string);
            });
//
            if (mode == "POST") {
              ConfermaModificaRiga(l, "", quantita, "CAMBIA_QUANTITA_PRELEVATA");
            }
           /* if (quantita <= l.quantita) {
              if (mode == "POST") {
                ConfermaModificaRiga(l, "", quantita, "CAMBIA_QUANTITA_PRELEVATA");
              }
            } else {
              showMessage(scaffoldKEY,
                  "La quantita' prelevata non puo' essere superiore alla quantita' dell'ordine");
              setState(() {
                _tc_quantita_prelevata2.text = quantita_string = "";
                quantita = 0;

                FocusScope.of(context).requestFocus(_fn_quantita_prelevata2);
              });
            }*/
          } else {
            showMessage(scaffoldKEY, "Quantita' non valida");
            setState(() {
              _tc_quantita_prelevata2.text = quantita_string = "";
              quantita = 0;

              FocusScope.of(context).requestFocus(_fn_quantita_prelevata2);
            });
          }
        } else {
          showMessage(scaffoldKEY, "Quantita' non valida");
          setState(() {
            _tc_quantita_prelevata2.text = quantita_string = "";
            quantita = 0;

            FocusScope.of(context).requestFocus(_fn_quantita_prelevata2);
          });
        }
      }

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Articolo ${l.codice_articolo} \nQtà Prelevata: ${l.quantita_prelevata.toString()} \nQtà Ordine: ${l.quantita.toString()}",
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                  )
                ],
              ),
              content: SafeArea(
                bottom: false,
                top: false,
                child: Form(
                  key: formKEY2,
                  child: RawKeyboardListener(
                      focusNode: FocusNode(),
                      onKey: (RawKeyEvent event) async {
                        if (event.runtimeType == RawKeyDownEvent &&
                            event.logicalKey == LogicalKeyboardKey.enter) {
                          if (_fn_quantita_prelevata2 != null &&
                              _fn_quantita_prelevata2.hasFocus) {
                            onSubmittedQuantita(
                                _tc_quantita_prelevata2.text, "");
                          }
                        }
                      },
                      child: TextField(
                        readOnly: false,
                        autofocus: true,
                        focusNode: _fn_quantita_prelevata2,
                        controller: _tc_quantita_prelevata2,
                        decoration: InputDecoration(
                            labelText: "Quantita' Prel.",
                            errorText: null,
                            labelStyle: TextStyle(color: Colors.black)),
                        maxLines: null,
                        onSubmitted: (value) {
                          onSubmittedQuantita(_tc_quantita_prelevata2.text, "");
                        },
                      )),
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text("ANNULLA",
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                ),
                FlatButton(
                  child: Text("CONFERMA",
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            );
          }).then((val) async {
        if (val != null) {
          if (val) {

            onSubmittedQuantita(_tc_quantita_prelevata2.text, "POST");
          }
        }
      });
    }

    onPressedModificaQuantitaPrelevataRiga(ListaPrelievi l) {
      if(_parametri[0].text_message == "TUTTI") {
        addLog(
            "Premuta funzione di modifica quantita prelevata (id dettaglio: ${l.id_dettaglio.toString()})", "A", "ListaPrelieviScreen");
      }

      if (l.stato_dettaglio != "N" || (l.stato_dettaglio == "N" && l.quantita_prelevata > 0)) {
        displayCambiaQuantitaPrelevataRiga(context, l);
      } else {
        addLog("Funzione di cambio quantita prelevata negata: L'articolo ${l.codice_articolo} deve ancora essere prelevato (id dettaglio: ${l.id_dettaglio.toString()})", "E", "ListaPrelieviScreen");
        showMessage(
            scaffoldKEY,
            "L'articolo ${l.codice_articolo} deve ancora essere prelevato",
            colore1);
      }
    }

    displayCambiaStatoRiga(BuildContext context, ListaPrelievi l) {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog cambio stato dettaglio/riga (id dettaglio: ${l.id_dettaglio.toString()}, stato: ${l.stato_dettaglio})", "E", "ListaPrelieviScreen");
      }

      String stato_i = "";
      String stato_f = "";
      if (l.stato_dettaglio == "N") {
        stato_f = stato_i = "N"; //"Aperto";
      } else if (l.stato_dettaglio == "S") {
        stato_f = stato_i = "S";//"Chiuso";
      }

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Articolo ${l.codice_articolo} \nStato: ${stato_i}",
                      style: TextStyle(fontSize: 16.0, color: Colors.black),
                    ),
                  ),
                ],
              ),
              content:
                  /* Visibility(
                    visible: (_stati_dettaglio.length != null),
                    child: Container(
                        width: MediaQuery.of(context).size.width / 10 * 5,
                        child: new DropdownButton(
                            key: new Key("statidett"),
                            items: _stati_dettaglio,
                            value: stato_f,
                            onChanged: (value) {
                              setState(() {
                                stato_f = value;
                              });
                            })),
                  )*/
                  ListTile(
                title: Row(
                  children: <Widget>[
                    Switch(
                      activeColor: colore1,
                      value: (stato_f == "S"),
                      onChanged: (bool value) {
                        if(_parametri[0].text_message == "TUTTI") {
                          addLog("Set provvisorio stato dettaglio a $stato_f (id dettaglio: ${l.id_dettaglio.toString()}, stato iniziale: ${l.stato_dettaglio})", "E", "ListaPrelieviScreen");
                        }

                        if (value) {
                          setState(() {
                            stato_f = "S";
                          });
                        } else {
                          setState(() {
                            stato_f = "N";
                          });
                        }
                      },
                    ),
                  ],
                ),
                subtitle: Text("Aperto/Chiuso"),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text("ANNULLA",
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                ),
                FlatButton(
                  child: Text("CONFERMA",
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            );
          }).then((val) async {
        if (val != null) {
          if (val) {
            if(_parametri[0].text_message == "TUTTI") {
              addLog(
                  "Apertura dialog cambio stato dettaglio/riga (id dettaglio: ${l
                      .id_dettaglio.toString()}, stato iniziale: ${l
                      .stato_dettaglio}, stato finale: $stato_f)", "E",
                  "ListaPrelieviScreen");
            }
            ConfermaModificaRiga(l, stato_f, 0, "CAMBIA_STATO");
          }
        }
      });
    }

    onPressedCambiaStatoRiga(ListaPrelievi l) {
      displayCambiaStatoRiga(context, l);
    }

    displayAnnullaPrelievoRiga(BuildContext context, ListaPrelievi l) {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog di annullamento prelievo", "E", "ListaPrelieviScreen");
      }

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Text(
                "Vuoi annullare il prelievo di questo articolo?",
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text("NO",
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                ),
                FlatButton(
                  child: Text("SI",
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            );
          }).then((val) async {
        if (val != null) {
          if (val) {
            addLog("Conferma annullamento prelievo (id dettaglio: ${l.id_dettaglio.toString()})", "A", "ListaPrelieviScreen");
            ConfermaModificaRiga(l, "", 0, "ANNULLA_PRELIEVO");
          }
        }
      });
    }

    onPressedAnnullaPrelievoRiga(ListaPrelievi l) {
      if(_parametri[0].text_message == "TUTTI") {
        addLog(
            "Premuta funzione di annullamento prelievo (id dettaglio: ${l.id_dettaglio.toString()})", "A", "ListaPrelieviScreen");
      }

      if (l.stato_dettaglio != "N" || l.quantita_prelevata > 0) {
        displayAnnullaPrelievoRiga(context, l);
      } else {
        addLog("Funzione di annullamento prelievo negata: L'articolo ${l.codice_articolo} deve ancora essere prelevato (id dettaglio: ${l.id_dettaglio.toString()})", "E", "ListaPrelieviScreen");

        showMessage(
            scaffoldKEY,
            "L'articolo ${l.codice_articolo} deve ancora essere prelevato",
            colore1);
      }
    }

    displayProprietaRiga(BuildContext context, ListaPrelievi l) async {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog proprieta riga", "E", "ListaPrelieviScreen");
      }

      String stato_dettaglio = "";
      if (l.stato_dettaglio == "S")  {
        stato_dettaglio = "CHIUSA";
      } else if (l.stato_dettaglio == "N")  {
        stato_dettaglio = "APERTA";
      }

      String message =
          "${l.descr_articolo} \n\nStato Riga: $stato_dettaglio \nQuantita': ${l.quantita.toString()} \nQuantita' Prelevata: ${l.quantita_prelevata.toString()} \nUbicazione: ${l.codice_cella}";

      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Center(
                child: Text(
                  "Articolo: ${l.codice_articolo}",
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
              ),
              content: Text(message,
                  style: TextStyle(fontSize: 16.0, color: Colors.white)),
              actions: <Widget>[
                SizedBox(
                    height: MediaQuery.of(context).size.height / 20,
                    child: FloatingActionButton.extended(
                        tooltip: "Annulla Prelievo",
                        heroTag: "cancprel",
                        backgroundColor: Colors.white,
                        icon: Icon(Icons.cancel, color: colore1),
                        label: Text(
                          "",
                          style: TextStyle(color: colore1),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          onPressedAnnullaPrelievoRiga(l);
                        })),
                SizedBox(
                    height: MediaQuery.of(context).size.height / 20,
                    child: FloatingActionButton.extended(
                        tooltip: "Modifica Stato",
                        heroTag: "editstato",
                        backgroundColor: Colors.white,
                        icon: Icon(Icons.local_activity, color: colore1),
                        label: Text(
                          "",
                          style: TextStyle(color: colore1),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          onPressedCambiaStatoRiga(l);
                        })),
                SizedBox(
                    height: MediaQuery.of(context).size.height / 20,
                    child: FloatingActionButton.extended(
                        tooltip: "Modifica Quantita' Prelevata",
                        heroTag: "editqtaprel",
                        backgroundColor: Colors.white,
                        icon: Icon(Icons.edit_location, color: colore1),
                        label: Text(
                          "",
                          style: TextStyle(color: colore1),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          onPressedModificaQuantitaPrelevataRiga(l);
                        }))
              ],
            );
          });
    }

    displayProprieta(BuildContext context) async {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Apertura dialog proprieta", "E", "ListaPrelieviScreen");
      }

      String stato = "";
      if (_documento.stato_testata == "S")  {
        stato = "CHIUSO";
      } else if (_documento.stato_testata == "N")  {
        stato = "APERTO";
      }

      String message =
          "Stato ${_documento.tipo_documento}: $stato \nRighe Aperte: ${_documento.num_dettagli_aperti.toString()} \nRighe Chiuse: ${_documento.num_dettagli_chiusi.toString()} \nUtente: ${_documento.nome_utente}";

      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) {
            return AlertDialog(
              backgroundColor: colore1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Center(
                child: Text(
                  "${_documento.tipo_documento} ${_documento.codice_testata} del ${_documento.data_testata}",
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
              ),
              content: Text(message,
                  style: TextStyle(fontSize: 16.0, color: Colors.white)),
              actions: <Widget>[],
            );
          });
    }

    onPressedProprieta() {
      displayProprieta(context);
      if (_parametri[0].text_message == "S") {
        onPressedInfo();
      }
    }

    onPressedProprietaRiga(ListaPrelievi l) {
      displayProprietaRiga(context, l);
      if (_parametri[0].text_message == "S") {
        onPressedInfoRiga(l);
      }
    }

    onPressedAggiorna() {
      addLog("Premuta funzione di aggiornamento lista (id documento: ${_documento.id_testata.toString()})", "A", "ListaPrelieviScreen");

      showMessage(scaffoldKEY, "Aggiornamento lista prelievi...", colore1);
      AzzeraArticolo();
      setState(() {
        FocusScope.of(context).requestFocus(_fn_articolo);
      });
      validateDocumento(_documento.id_testata);
    }

    onClickMenu(MenuItemProvider item) {
      if (item.menuTitle == "Aperte") {
        setState(() {
          _f_stato = "APERTI";
        });
        setManager();
        showMessageDuration(scaffoldKEY, "Aggiornamento...", 700, colore1);
      } else if (item.menuTitle == "Chiuse") {
        setState(() {
          _f_stato = "CHIUSI";
        });
        setManager();
        showMessageDuration(scaffoldKEY, "Aggiornamento...", 700, colore1);
      } else if (item.menuTitle == "Tutte") {
        setState(() {
          _f_stato = "TUTTI";
        });
        setManager();
        showMessageDuration(scaffoldKEY, "Aggiornamento...", 700, colore1);
      } else if (item.menuTitle == "Ord.Ubicaz.") {
        setState(() {
          _f_ordinamento = "ORDINA_CELLA";
        });
        setManager();
        showMessageDuration(scaffoldKEY, "Aggiornamento...", 700, colore1);
      } else if (item.menuTitle == "Ord.Articolo") {
        setState(() {
          _f_ordinamento = "ORDINA_ARTICOLO";
        });
        setManager();
        showMessageDuration(scaffoldKEY, "Aggiornamento...", 700, colore1);
      } else if (item.menuTitle == "Info ODP") {
        onPressedProprieta();
      } else if (item.menuTitle == "Aggiorna") {
        onPressedAggiorna();
      }
    }

    stateChanged(bool isShow) {}

    onCloseMenu() {}

    displayMenu() {
      PopupMenu menu = PopupMenu(
          backgroundColor: colore1,
          lineColor: Colors.white,
          maxColumn: 3,
          items: [
            MenuItem(
                title: "Aperte",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon(
                  Icons.lock_open,
                  color: Colors.white,
                )),
            MenuItem(
                title: "Chiuse",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon(
                  Icons.lock,
                  color: Colors.white,
                )),
            MenuItem(
                title: "Tutte",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon(
                  Icons.all_inclusive,
                  color: Colors.white,
                )),
            MenuItem(
                title: "Ord.Articolo",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon(
                  Icons.looks_one,
                  color: Colors.white,
                )),
            MenuItem(
                title: "Ord.Ubicaz.",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon(
                  Icons.looks_two,
                  color: Colors.white,
                )),
            MenuItem(
                title: "Info ODP",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon(
                  Icons.info_outline,
                  color: Colors.white,
                )),
            MenuItem(
                title: "Aggiorna",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon(
                  Icons.refresh,
                  color: Colors.white,
                )),
          ],
          onClickMenu: onClickMenu,
          stateChanged: stateChanged,
          onDismiss: onCloseMenu);
      menu.show(widgetKey: menuKEY);
    }

    restoreMenuContext() {
      setState(() {
        PopupMenu.context = context;
      });
    }

    onPressedMenu() async {
      restoreMenuContext();
      displayMenu();
    }

    PostPickingArticolo(String mode) async {
      addLog(
          "Post Prelievo Articolo (id documento: ${_documento.id_testata.toString()}, articolo: ${_articolo.codice_articolo}, quantita prelevata (ordine): ${_articolo.quantita.toString()}, mode: $mode)", "E",
          "ListaPrelieviScreen");

      bool _done = false;

      try {
        String body;
        List<PickingArticolo> _lnc = new List<PickingArticolo>();

        if (mode == "PICKING_ARTICOLO") {
          _lnc.add(new PickingArticolo(_articolo.id_articolo,
              _documento.id_testata, _articolo.quantita, "", mode));
        } else if (mode == "CHIUDI_ODP") {
          _lnc.add(new PickingArticolo(0, _documento.id_testata, 0, "", mode));
        } else {
          _lnc.add(new PickingArticolo(_articolo.id_articolo,
              _documento.id_testata, _quantita_prelevata, "", mode));
        }

        List jsonList = PickingArticolo.encondeToJson(_lnc);
        body = json.encode(jsonList);

        final Postresult = await genericPost(
            GlobalConfiguration().getString("url_picking_articolo"),
            body,
            context,
            scaffoldKEY,
            true);

        if (Postresult != null &&
            Postresult is PostReturn &&
            Postresult.result == true) {
          _done = true;
          addLog(
              "Post eseguita con successo", "E", "ListaPrelieviScreen");

          WriteLogs();
        } else {
          addLog(
              "Errore ritorno post picking articolo (id documento: ${_documento.id_testata.toString()}, articolo: ${_articolo.codice_articolo}, quantita prelevata (ordine): ${_articolo.quantita.toString()}, mode: $mode): ${Postresult.error_msg}", "X",
              "ListaPrelieviScreen");
          showMessage(
              scaffoldKEY, "ERRORE PICKING ARTICOLO: " + Postresult.error_msg);
        }

        if (_done) {
          if (mode == "PICKING_ARTICOLO") {
            showMessageDuration(scaffoldKEY,
                "Articolo ${_articolo.codice_articolo} prelevato", 500,
                colore1);
          } else if (mode == "CHIUDI_ODP") {
            showMessageDuration(
                scaffoldKEY, "ODP Chiuso correttamente", 1000, colore1);
            AzzeraDocumento();
          } else {
            showMessageDuration(
                scaffoldKEY,
                "Prelevato ${_quantita_prelevata
                    .toString()} per l'articolo ${_articolo.codice_articolo}",
                500,
                colore1);
            AzzeraArticolo();
          }
          if(_parametri[0].text_message == "TUTTI") {
            addLog("Richiamo validazione documento in seguito a post corretta (id documento: ${_documento.id_testata.toString()})", "E", "ListaPrelieviScreen");
          }

          setState(() {
            FocusScope.of(context).requestFocus(_fn_articolo);
            _tc_articolo.selection = TextSelection(
                baseOffset: 0, extentOffset: _tc_articolo.text.length);
          });
          validateDocumento(_documento.id_testata);
        }
      } catch (e) {
        print(e.toString());
        addLog(
            "Errore codice post modifica riga (mode: $mode): ${e.toString()}",
            "X",
            "ListaPrelieviScreen");
      }
    }

    Future validateArticolo(String articolo) async {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Validazione articolo (articolo provvisorio: $articolo)", "E", "ListaPrelieviScreen");
      }

      AzzeraArticolo();

      try {
      List<PostCheckArticolo> _lnc = new List<PostCheckArticolo>();
      _lnc.add(new PostCheckArticolo(articolo, _documento.id_testata));

      List jsonList = PostCheckArticolo.encondeToJson(_lnc);
      String body = json.encode(jsonList);

      final Postresult = await genericPostCheckArticolo(
          GlobalConfiguration().getString("url_check_articolo"),
          body,
          context,
          scaffoldKEY,
          true);

      if (Postresult != null &&
          Postresult is CheckArticolo &&
          Postresult.result == true) {

        WriteLogs();

        if (Postresult.id_articolo > 0) {
          try {
            setState(() {
              _articolo.id_articolo = Postresult.id_articolo;
              _tc_articolo.text =
                  _articolo.codice_articolo = Postresult.codice_articolo;
              _articolo.descr_articolo = Postresult.descr_articolo;
              _articolo.quantita = Postresult.quantita;
              _quantita_prelevata = Postresult.quantita;
              _tc_quantita_prelevata.text = Postresult.quantita.toString();
            });
          }
          catch (e) {
            print(e.toString());
            addLog(
                "Errore assegnazione del articolo alle variabili locali: ${e.toString()}",
                "X", "ListaPrelieviScreen");
          }

          addLog("Richiamo di picking di articolo (id documento: ${_documento.id_testata.toString()}, articolo provv: $articolo)", "E", "ListaPrelieviScreen");
          PostPickingArticolo("PICKING_ARTICOLO");
        } else {
          addLog("Nessun articolo trovato (id documento: ${_documento.id_testata.toString()}, articolo provv: $articolo)", "E", "ListaPrelieviScreen");

          AzzeraArticolo();
          setState(() {
            FocusScope.of(context).requestFocus(_fn_articolo);
          });
        }
      } else {
        AzzeraArticolo();
        setState(() {
          FocusScope.of(context).requestFocus(_fn_articolo);
        });
      }
      } catch (e) {
        print(e.toString());
        addLog(
            "Errore codice post check articolo (articolo provvisorio: $articolo): ${e.toString()}",
            "X",
            "ListaPrelieviScreen");
      }
    }

    onSubmittedArticolo(String value) {
      addLog("Richiamo Submit Articolo (valore: $value)", "A", "ListaPrelieviScreen");

      value = ReplaceTextController(value);
      _tc_articolo.text = ReplaceTextController(_tc_articolo.text);

      _temp = _articolo.codice_articolo;
      if (value == "" && _temp != "" && _articolo.id_articolo != 0) {
        value = _tc_articolo.text = _temp;
      }

      validateArticolo(value);
    }

    Future validateQuantita(String quantita) async {
      if(_parametri[0].text_message == "TUTTI") {
        addLog("Validazione quantita (quantita provvisoria: $quantita, articolo: ${_articolo.codice_articolo})", "E", "ListaPrelieviScreen");
      }

      if (NumericField(quantita) == true) {
        if (double.parse(quantita) > -1) {
          if(_parametri[0].text_message == "TUTTI") {
            addLog(
                "Quantita valida (quantita provvisoria: $quantita, articolo: ${_articolo.codice_articolo})", "E", "ListaPrelieviScreen");
          }

          try {
          setState(() {
            _quantita_prelevata = double.parse(quantita);
            _tc_quantita_prelevata.text = quantita;
          });

          await Future.delayed(const Duration(milliseconds: 600));
          setState(() {
            FocusScope.of(context)
                .requestFocus(_fn_conferma_quantita_prelevata);
          });
          /*if (_quantita_prelevata > _articolo.quantita) {
            addLog(
                "Quantita non valida perche' superiore alla quantita' da prelevare (quantita provvisoria: $quantita, articolo: ${_articolo.codice_articolo})", "E", "ListaPrelieviScreen");
            showMessageDuration(scaffoldKEY, "La quantita' prelevata e' superiore alla quantita' da prelevare",1000,colore1);
          }*/
        }
      catch (e) {
        print(e.toString());
        addLog(
            "Errore assegnazione della quantita alle variabili locali: ${e
                .toString()}",
            "X", "ListaPrelieviScreen");
      }
        } else {
          addLog(
              "Quantita non valida (quantita provvisoria: $quantita, articolo: ${_articolo.codice_articolo})", "X", "ListaPrelieviScreen");
          showMessage(scaffoldKEY, "Quantita' non valida");
          AzzeraQuantitaPrelevata();
          setState(() {
            FocusScope.of(context).requestFocus(_fn_quantita_prelevata);
          });
        }
      } else {
        addLog(
            "Quantita non valida (quantita provvisoria: $quantita, articolo: ${_articolo.codice_articolo})", "X", "ListaPrelieviScreen");
        showMessage(scaffoldKEY, "Quantita' non valida");
        AzzeraQuantitaPrelevata();
        setState(() {
          FocusScope.of(context).requestFocus(_fn_quantita_prelevata);
        });
      }
    }

    onSubmittedQuantita(String value) {
      addLog("Richiamo Submit Quantita (valore: $value)", "A", "ListaPrelieviScreen");

      value = ReplaceTextController(value);
      _tc_quantita_prelevata.text =
          ReplaceTextController(_tc_quantita_prelevata.text);

      _temp = _quantita_prelevata.toString();
      if (value == "" && _temp != "" && _quantita_prelevata != 0) {
        value = _tc_quantita_prelevata.text = _temp;
      }

      validateQuantita(value);
    }

    onPressedDocumentoSuccessivo() {
      addLog("Pressione tasto di documento successivo", "A", "ListaPrelieviScreen");

      AzzeraDocumento();
      validateDocumento(0);
    }

    onTextFormField(String value, String textfield) {
      print(textfield + ": " + value);
      if (value.contains('\n')) {
        addLog("Richiamo Text Form field (textfield: $textfield, valore: $value)", "A", "ListaPrelieviScreen");

        print('Enter premuto: ' + textfield);
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (textfield == "articolo") {
            _tc_articolo.text = ReplaceTextController(value);
            onSubmittedArticolo(_tc_articolo.text);
          } else if (textfield == "quantita_prelevata") {
            _tc_quantita_prelevata.text = ReplaceTextController(value);
            onSubmittedQuantita(_tc_quantita_prelevata.text);
          }
        });
      }
    }

    logManagment() {
      WriteLogs().then((v) {
        startTimerLogTerminale();
      });
    }

    if (!Initialized) {
      Initialized = true;

      restoreMenuContext();
      Initialization();
      TextSelections();

      addLog("Inizializzazione maschera", "E", "ListaPrelieviScreen");

      getParametri().then((v) {
        logManagment();
      });

      validateDocumento(0);
      setState(() {
        FocusScope.of(context).requestFocus(_fn_articolo);
      });
    }

    return Scaffold(
        key: scaffoldKEY,
        drawer: AppDrawer(
          callback: (isOpen) {
            if(_parametri[0].text_message == "TUTTI") {
              addLog("Apertura drawer laterale",
                  "A", "ListaPrelieviScreen");
            }

            print("isAppDrawerOpen ${isOpen}");
            WidgetsBinding.instance.addPostFrameCallback((_) async {
              if (isOpen) {
                if(_parametri[0].text_message == "TUTTI") {
                  addLog("Eventuale sospensione timer in quanto apro il drawer",
                      "E", "ListaPrelieviScreen");
                }
                stopTimerDocumento();
              } else {
                if (_documento.id_testata == 0) {
                  if(_parametri[0].text_message == "TUTTI") {
                    addLog("Riattivo timer alla chiusura del drawer e siccome id documento 0",
                        "E", "ListaPrelieviScreen");
                  }
                  startTimerDocumento();
                }
              }
            });
          },
        ),
        appBar: AppBar(
            backgroundColor: colore1,
            /*leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, null),
          ),*/
            actions: <Widget>[
              /* Visibility(
              visible: true,
              child: IconButton(
                icon: Icon(Icons.refresh, color: Colors.white),
                tooltip: "Aggiorna",
                onPressed: () {
                 onPressedAggiorna();
                },
              ),
            ),*/
              Visibility(
                  visible: (_documento.id_testata > 0),
                  child: IconButton(
                      key: menuKEY,
                      tooltip: "Menu'",
                      icon: Icon(Icons.filter_list),
                      onPressed: () {
                        if(_parametri[0].text_message == "TUTTI") {
                          addLog("Apertura menu", "A", "ListaPrelieviScreen");
                        }
                        onPressedMenu();
                      })),
              Padding(
                padding: EdgeInsets.only(right: 5),
              ),
              Observer<int>(
                stream: managerListaPrelievi.count$,
                onSuccess: (context, data) {
                  return Chip(
                    label: Text(
                      " ${(data ?? 0).toString()} ",
                      style: TextStyle(fontWeight: FontWeight.bold, color: (_f_stato == "TUTTI") ? Color(4286588201) : Colors.white),
                    ),
                    backgroundColor: (_f_stato == "APERTI") ? _colore_aperto_1 : (_f_stato == "CHIUSI") ? _colore_chiuso_1 : colore5,
                  );
                },
              ),
              Padding(
                padding: EdgeInsets.only(right: 16),
              )
            ],
            title: Center(
                child: Text(
              "Lista Prelievi",
              style: TextStyle(fontSize: 18),
            ))),
        body: SafeArea(
          top: false,
          bottom: false,
          child: Form(
            key: formKEY,
            child: RawKeyboardListener(
                focusNode: FocusNode(),
                onKey: (RawKeyEvent event) {
                  if (event.runtimeType == RawKeyDownEvent &&
                      event.logicalKey == LogicalKeyboardKey.enter) {
                    if (_fn_articolo != null && _fn_articolo.hasFocus) {
                      //onSubmittedArticolo(_tc_articolo.text);
                    } else if (_fn_quantita_prelevata != null &&
                        _fn_quantita_prelevata.hasFocus) {
                      //onSubmittedQuantita(_tc_quantita_prelevata.text);
                    }
                  }
                },
                child: ListView(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(
                            top: 8.0, left: 8.0, right: 8.0, bottom: 0.0),
                        child: (_documento.id_testata > 0)
                            ? Row(children: <Widget>[
                                Card(
                                    color: colore5,
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                10 *
                                                9.25,
                                        child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    "${_documento.tipo_documento} ${_documento.codice_testata} del ${_documento.data_testata}",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.black,
                                                        fontSize: 19.0),
                                                  ),
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Container(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            10 *
                                                            7,
                                                        child: Align(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          child: Text(
                                                            "Righe aperte: ${_documento.num_dettagli_aperti.toString()} / ${(_documento.num_dettagli_chiusi + _documento.num_dettagli_aperti).toString()} \nRighe prelevate: ${_documento.num_dettagli_chiusi.toString()} / ${(_documento.num_dettagli_chiusi + _documento.num_dettagli_aperti).toString()}",
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 15),
                                                          ),
                                                        )),
                                                  /*  Container(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            10 *
                                                            2,
                                                        child: Align(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: Chip(
                                                              label: Text(
                                                                "$_f_stato",
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color: Colors
                                                                        .black,
                                                                    fontSize:
                                                                        10),
                                                              ),
                                                              backgroundColor: (_parametri[2]
                                                                              .text_message !=
                                                                          "S" ||
                                                                      _f_stato ==
                                                                          "TUTTI")
                                                                  ? colore2
                                                                  : ((_f_stato ==
                                                                          "APERTI")
                                                                      ? _colore_aperto_1
                                                                      : _colore_chiuso_1),
                                                            )))*/
                                                  ],
                                                ),
                                              ],
                                            )))),
                                /*Container(
                                  width: MediaQuery.of(context).size.width / 10 * 2,
                                  child: Center(
                                      child: Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Chip(
                                            label: Text(
                                              "$_f_stato",
                                              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 13),
                                            ),
                                            backgroundColor: colore2,
                                          )
                                        ],
                                      )),
                                )*/
                              ])
                            : Card(
                                color: colore5,
                                child: Container(
                                    width: MediaQuery.of(context).size.width /
                                        10 *
                                        9.25,
                                    child: Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                "Nessun ODP da mostrare per l'utente",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black,
                                                    fontSize: 19.0),
                                              ),
                                            )
                                          ],
                                        ))))),
                    Visibility(
                        visible: (_documento.id_testata > 0),
                        child: Padding(
                            padding: const EdgeInsets.only(
                                top: 0.0, left: 0.0, right: 0.0),
                            child: Container(
                                height: MediaQuery.of(context).size.height / 10 * ((_documento.num_dettagli_aperti > 0) ? 5: 4),
                                child: ListaPrelieviBuilder(
                                    stream: managerListaPrelievi.browse$,
                                    builder: (context, posizioni) {
                                      if (posizioni?.length == 0) {
                                        return Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Center(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Center(
                                                    child: Icon(
                                                        Icons.announcement,
                                                        size: 100.0),
                                                  ),
                                                  Center(
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 10.0,
                                                          right: 10.0,
                                                          top: 5.0),
                                                      child: Text(
                                                        "Nessun Articolo ancora da prelevare",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      } else {
                                        return ListView.separated(
                                            itemCount: posizioni?.length ?? 0,
                                            physics: bouncingScroll,
                                            separatorBuilder:
                                                (context, index) => Divider(),
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              _animationController.forward();
                                              _animationController
                                                  .addListener(() {
                                                setState(() {});
                                              });
                                              return Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    Slidable(
                                                        actionPane:
                                                            SlidableDrawerActionPane(),
                                                        actionExtentRatio: 0.25,
                                                        actions: <Widget>[],
                                                        secondaryActions: <
                                                            Widget>[
                                                          IconSlideAction(
                                                            caption:
                                                                "Mod. Stato",
                                                            color: (posizioni[
                                                                            index]
                                                                        .stato_dettaglio ==
                                                                    "S")
                                                                ? _colore_chiuso_1
                                                                : _colore_aperto_1, //Colors.yellow,
                                                            icon: Icons
                                                                .local_activity,
                                                            onTap: () {
                                                              onPressedCambiaStatoRiga(
                                                                  posizioni[
                                                                      index]);
                                                            },
                                                          ),
                                                          IconSlideAction(
                                                            caption:
                                                                "Mod. Qta'Prel.",
                                                            color: (posizioni[
                                                                            index]
                                                                        .stato_dettaglio ==
                                                                    "S")
                                                                ? _colore_chiuso_2
                                                                : _colore_aperto_2, //Colors.yellow,
                                                            icon: Icons
                                                                .edit_location,
                                                            onTap: () {
                                                              onPressedModificaQuantitaPrelevataRiga(
                                                                  posizioni[
                                                                      index]);
                                                            },
                                                          ),
                                                          IconSlideAction(
                                                            caption:
                                                            "Annulla Prel.",
                                                            color: (posizioni[
                                                            index]
                                                                .stato_dettaglio ==
                                                                "S")
                                                                ? _colore_chiuso_1
                                                                : _colore_aperto_1, //Colors.yellow,
                                                            icon: Icons
                                                                .cancel,
                                                            onTap: () {
                                                              onPressedAnnullaPrelievoRiga(
                                                                  posizioni[
                                                                  index]);
                                                            },
                                                          ),
                                                        ],
                                                        child: SlideTransition(
                                                            position:
                                                                _slideAnimation,
                                                            child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            0.0,
                                                                        bottom:
                                                                            0.0,
                                                                        left: 0,
                                                                        right:
                                                                            0),
                                                                child:
                                                                    RoundedChiseledBorder(
                                                                  borderRadius:
                                                                      0,
                                                                  borderWidth:
                                                                      (_parametri[2].text_message ==
                                                                              "S")
                                                                          ? 0
                                                                          : 0,
                                                                  bottomBorderColor:
                                                                      Colors.grey[
                                                                          50],
                                                                  leftBorderColor:
                                                                      Colors.grey[
                                                                          50], //(posizioni[index].stato_dettaglio == "S") ? _colore_chiuso_1 : _colore_aperto_1,
                                                                  rightBorderColor:
                                                                      Colors.grey[
                                                                          50], //(posizioni[index].stato_dettaglio == "S") ? _colore_chiuso_1 : _colore_aperto_1,
                                                                  topBorderColor:
                                                                      Colors.grey[
                                                                          50],
                                                                  child:
                                                                      ListTile(
                                                                    title: Container(
                                                                        color: (posizioni[index].stato_dettaglio == "S") ? ((posizioni[index].quantita_prelevata != posizioni[index].quantita)? colore7 : colore4) : ((posizioni[index].quantita_prelevata != posizioni[index].quantita && posizioni[index].quantita_prelevata > 0) ? colore8 : colore4),
                                                                        child: Column(
                                                                          children: <
                                                                              Widget>[
                                                                            Row(
                                                                              children: <Widget>[
                                                                                Container(
                                                                                  width: MediaQuery.of(context).size.width / 10 * 5.625,
                                                                                  child: Align(
                                                                                    alignment: Alignment.centerLeft,
                                                                                    child: Card(
                                                                                      color: (posizioni[index].stato_dettaglio == "S") ? _colore_chiuso_1 : _colore_aperto_1,
                                                                                      child: Padding(
                                                                                        padding: const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 4.0, right: 4.0),
                                                                                        child: Text(
                                                                                          "Articolo: " + posizioni[index].codice_articolo,
                                                                                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 14),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                Container(
                                                                                  width: MediaQuery.of(context).size.width / 10 * 3,
                                                                                  child: Align(
                                                                                    alignment: Alignment.centerLeft,
                                                                                    child: Card(
                                                                                      color: (posizioni[index].stato_dettaglio == "S") ? _colore_chiuso_2 : _colore_aperto_2,
                                                                                      child: Padding(
                                                                                        padding: const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 4.0, right: 4.0),
                                                                                        child: Text(
                                                                                          "Ubi: " + posizioni[index].codice_cella,
                                                                                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 14),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(left: 5.0),
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Container(
                                                                                    width: MediaQuery.of(context).size.width / 10 * 5.5,
                                                                                    child: Align(
                                                                                      alignment: Alignment.centerLeft,
                                                                                      child: Text(
                                                                                        posizioni[index].descr_articolo,
                                                                                        style: TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 13),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                  Container(
                                                                                      width: MediaQuery.of(context).size.width / 10 * 3,
                                                                                      child: Align(
                                                                                        alignment: Alignment.centerLeft,
                                                                                        child: Card(
                                                                                          color: colore5,
                                                                                          child: Padding(
                                                                                            padding: const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 4.0, right: 4.0),
                                                                                            child: Text(
                                                                                              "Qta': ${posizioni[index].quantita.toString()}" +
                                                                                                  ((posizioni[index].stato_dettaglio == "S") || (posizioni[index].stato_dettaglio == "N" && posizioni[index].quantita_prelevata > 0)
                                                                                                      ? "\n"
                                                                                                          "Prel.: ${posizioni[index].quantita_prelevata.toString()}"
                                                                                                      : ""),
                                                                                              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 14),
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                      )),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        )),
                                                                    onLongPress:
                                                                        () {
                                                                          if(_parametri[0].text_message == "TUTTI") {
                                                                            addLog("on long press", "A", "ListaPrelieviScreen");
                                                                          }
                                                                      if (_parametri[1]
                                                                              .text_message ==
                                                                          "S") {
                                                                        onPressedProprietaRiga(
                                                                            posizioni[index]);
                                                                      }
                                                                    },
                                                                    onTap: () {
                                                                      if(_parametri[0].text_message == "TUTTI") {
                                                                        addLog("on press riga/dettaglio", "A", "ListaPrelieviScreen");
                                                                      }
                                                                      onPressedProprietaRiga(
                                                                          posizioni[
                                                                              index]);
                                                                    },
                                                                  ),
                                                                ))))
                                                  ]);
                                            });
                                      }
                                    })))),
                    Visibility(
                        visible: (_documento.id_testata > 0 && _documento.num_dettagli_aperti == 0),
                        child:  Center( child:Padding(
                          padding: const EdgeInsets.only(
                              top: 0.0, left: 8, right: 8, bottom: 4),
                          child: Center(
                            child: Row(
                              children: <Widget>[
                               Center( child: Padding(
                                    padding:
                                    const EdgeInsets.only(
                                        top: 16.0,
                                        left: 0.0,
                                        right: 0.0,
                                        bottom: 0),
                                    child:  Center( child:  FloatingActionButton.extended(
                                        tooltip: "Prendi in carico ODP successivo",
                                        backgroundColor: colore1,
                                        onPressed: () {
                                          onPressedDocumentoSuccessivo();
                                        },
                                        icon: Icon(Icons.skip_next, color: Colors.white),
                                        label: Text("ODP Successivo")))))
                              ],
                            ),
                          ),
                        ))),
                    Visibility(
                        visible: _documento.id_testata == 0,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Center(
                                  child: SizedBox(
                                    child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                colore1)),
                                    width: 45,
                                    height: 45,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: EdgeInsets.only(top: 16),
                                    child: Center(
                                      child: Text('Ricerca documenti...'),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )),
                    Visibility(
                        visible: (_documento.id_testata > 0),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 0.0, left: 8, right: 8, bottom: 4),
                          child: Center(
                            child: Card(
                                color: colore5,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 0.0, left: 8.0, right: 8.0),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                10 *
                                                5,
                                        child: Align(
                                            alignment: Alignment.center,
                                            child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: TextFormField(
                                                  readOnly: (_documento
                                                          .num_dettagli_aperti ==
                                                      0),
                                                  autofocus: false,
                                                  focusNode: _fn_articolo,
                                                  controller: _tc_articolo,
                                                  decoration: InputDecoration(
                                                    labelText: "Articolo",
                                                    errorText: null,
                                                  ),
                                                  maxLines: null,
                                                  autovalidate: true,
                                                  validator: (value) {
                                                    onTextFormField(
                                                        value, "articolo");
                                                  },
                                                ))),
                                      ),
                                      Visibility(
                                          visible: (_articolo.id_articolo > 0),
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                10 *
                                                2.5,
                                            child: Align(
                                                alignment: Alignment.center,
                                                child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: TextFormField(
                                                      readOnly: false,
                                                      autofocus: false,
                                                      focusNode:
                                                          _fn_quantita_prelevata,
                                                      controller:
                                                          _tc_quantita_prelevata,
                                                      decoration:
                                                          InputDecoration(
                                                        labelText: "Qta' Prel.",
                                                        errorText: null,
                                                      ),
                                                      maxLines: null,
                                                      autovalidate: true,
                                                      validator: (value) {
                                                        onTextFormField(value,
                                                            "quantita_prelevata");
                                                      },
                                                    ))),
                                          )),
                                      Visibility(
                                          visible: (_quantita_prelevata !=
                                                  _articolo.quantita &&
                                              _articolo.id_articolo > 0),
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  10 *
                                                  1,
                                              child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 16.0,
                                                          left: 0.0,
                                                          right: 0.0,
                                                          bottom: 0),
                                                  child: SizedBox(
                                                      height:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width /
                                                              10,
                                                      child:
                                                          FloatingActionButton(
                                                              tooltip:
                                                                  "Conferma Quantita' Prelevata",
                                                              heroTag:
                                                                  "qta_prel",
                                                              focusNode:
                                                                  _fn_conferma_quantita_prelevata,
                                                              focusColor:
                                                                  colore3,
                                                              backgroundColor:
                                                                  colore1,
                                                              child: Icon(
                                                                  FontAwesomeIcons
                                                                      .check,
                                                                  color: Colors
                                                                      .white),
                                                              onPressed: () {
                                                                if(_parametri[0].text_message == "TUTTI") {
                                                                  addLog("Premuto bottone conferma cambio quantita prelevata (articolo: ${_articolo.codice_articolo}, quantita prelevata: ${_articolo.quantita_prelevata.toString()}, quantita ordine: ${_articolo.quantita.toString()}, quantita nuova: ${_quantita_prelevata.toString()}", "A", "ListaPrelieviScreen");
                                                                }
                                                                  PostPickingArticolo(
                                                                    "CAMBIA_QUANTITA_PRELEVATA2");
                                                              }))))),
                                    ],
                                  ),
                                )),
                          ),
                        )),
                  ],
                )),
          ),
        ),
        floatingActionButton: Visibility(
                visible: (_documento.id_testata == 0),
                child: FloatingActionButton.extended(
                    tooltip: "Ricerca ODP associato all'utente",
                    backgroundColor: colore1,
                    onPressed: () async {
                      if(_parametri[0].text_message == "TUTTI") {
                        addLog("Premuto bottone RICARICA", "A", "ListaPrelieviScreen");
                      }

                      await stopTimerDocumento().then((v) {
                        showMessageDuration(
                            scaffoldKEY, "Aggiornamento...", 1000, colore1);
                        validateDocumento(0);
                      });
                    },
                    icon: Icon(Icons.settings_backup_restore,
                        color: Colors.white),
                    label: Text("RICARICA"))));
  }
}

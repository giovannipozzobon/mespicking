import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:MesPicking/common/platform/platformScaffold.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  CAPTIONS
String _lbl_title = "Mes Picking App";
String _lbl_copyright = "© Copyright Quadrivium 2020";

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final int splashDuration = 3;

  startTime() async {
    return Timer(Duration(seconds: splashDuration), () {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      Navigator.of(context).pushNamedAndRemoveUntil('/LoginScreen', (Route<dynamic> route) => false);
    });
  }

  @override
  initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
        drawer: Drawer(),
        body: Container(
            decoration: BoxDecoration(
                color: Color(4286588201)),
            child: Column(children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(4286588201)),
                  alignment: FractionalOffset(0.5, 0.3),
                  child: Text(
                    _lbl_title,
                    style: TextStyle(fontSize: 40.0, color: Colors.white),
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                  child: Text(_lbl_copyright,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.white,
                      )))
            ])));
  }
}

import 'package:MesPicking/common/functions/FileManagment.dart';
import 'package:MesPicking/common/functions/LogManagment.dart';
import 'package:MesPicking/model/json/CheckParametri.dart';
import 'package:MesPicking/model/json/LogJSON.dart';
import 'package:MesPicking/service/CheckParametriService.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:popup_menu/popup_menu.dart';
import 'package:shared_preferences/shared_preferences.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
List<CheckParametri> _parametri;
List<Log> _lista_logs;
String _directory_applicazione = "/storage/emulated/0/Android/data/it.quadrivium.MesPicking/files";

bool _mostra_campi = false;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(0xffb83d3d); //4288362813
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);
Color colore7 = Colors.pinkAccent[100];
Color colore8 = Colors.lightGreen[200];
Color _colore_aperto_1 = Colors.green;
Color _colore_aperto_2 = Colors.lightGreen;
Color _colore_chiuso_1 = Colors.red;
Color _colore_chiuso_2 = Colors.redAccent;

class ElencoLogsScreen extends StatefulWidget {
  @override
  _ElencoLogsScreen createState() =>
      new _ElencoLogsScreen();
}

class _ElencoLogsScreen extends State<ElencoLogsScreen> {
  final GlobalKey<ScaffoldState> scaffoldKEY = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKEY = new GlobalKey<FormState>();
  GlobalKey menuKEY = GlobalKey();
  BouncingScrollPhysics bouncingScroll = new BouncingScrollPhysics();

  bool Initialized = false;

  getDirectoryApplicazione() async {
    _directory_applicazione = (await getExternalStorageDirectory()).path;
    await Future.delayed(const Duration(milliseconds: 1200));
  }

  AzzeraParametri() {
    addLog("Azzeramento parametri", "E", "ListaPrelieviScreen");
    setState(() {
      _parametri = new List<CheckParametri>();

      setState(() {
        _parametri.add(new CheckParametri(
            0, "NOMEFILELOG", "", "log_mes_picking.txt", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(
            0, "NOMELOGSERVER", "", "http://13.93.33.246:8080/gmenode/ODP Picking/logs", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(
            0, "CATEGORIALOG", "", "TUTTI", "", 0, "", 0, "", ""));
      });
    });
  }

  AzzeraListaLogs() {
    setState(() {
      _lista_logs = new List<Log>();
    });
  }

  CheckListaLogs() async {
    String nome_file = getNomeFileLog(GlobalConfiguration().getValue("nome_file_log").toString());
    String cartella = GlobalConfiguration().getValue("cartella_terminale").toString() + "/logs";
    String percorso = cartella + "/" + nome_file;

    if(GlobalConfiguration().getValue("tipo_log") == "TUTTI") {
      addLog("Controllo inizializzazione lista log da mostrare", "E", "ElencoLogsScreen");
    }

    try{
    AzzeraListaLogs();
    WriteLogs();

    List<LogJSON> contenuto = new List<LogJSON>();
    await leggiFileJsonLog(percorso).then((result) {
     setState(() {
       contenuto = result;
     });
    });

    setState(() {
      _lista_logs = fromLogJsontoLog(contenuto);
    });

    } catch(e) {
      addLog("Errore  inizializzazione lista log da mostrare: ${e.toString()}", "X",
          "ElencoLogsScreen");
    }
  }

  Future getParametri() async {
    try {
      List<CheckParametri> check = await CheckParametriService.browse(
          filter: "");
      if (check.length > 0) {
        for (int i = 0; i < check.length; i++) {
          for (int j = 0; j < _parametri.length; j++) {
            if (check[i].codice_parametro == _parametri[j].codice_parametro) {
              setState(() {
                _parametri[j].id_parametro = check[i].id_parametro;
                _parametri[j].text_message = check[i].text_message;
                _parametri[j].num_message = check[i].num_message;
              });
            }
          }
        }
      }
    } catch(e) {
      addLog("Errore caricamento parametri: ${e.toString()}", "X", "ListaPrelieviScreen");
    }
  }

  @override
  Widget build(BuildContext context) {
    onClickMenu(MenuItemProvider item) {
      if (item.menuTitle == "Nascondi") {
        setState(() {
          _mostra_campi = false;
        });
      } else if (item.menuTitle == "Mostra") {
        setState(() {
          _mostra_campi = true;
        });
      }
      CheckListaLogs();
    }

    stateChanged(bool isShow) {}

    onCloseMenu() {}

    displayMenu() {
      PopupMenu menu = PopupMenu(
          backgroundColor: colore1,
          lineColor: Colors.white,
          maxColumn: 2,
          items: [
            MenuItem(
                title: (_mostra_campi) ? "Nascondi" : "Mostra",
                textStyle: TextStyle(color: Colors.white, fontSize: 12.5),
                image: Icon( Icons.remove_red_eye,
                  color: Colors.white,
                ))
          ],
          onClickMenu: onClickMenu,
          stateChanged: stateChanged,
          onDismiss: onCloseMenu);
      menu.show(widgetKey: menuKEY);
    }

    restoreMenuContext() {
      setState(() {
        PopupMenu.context = context;
      });
    }

    onPressedMenu() {
      restoreMenuContext();
      displayMenu();
      CheckListaLogs();
    }

    onPressedSalvaLog() {
      if(GlobalConfiguration().getValue("tipo_log") == "TUTTI") {
        addLog("Pressione tasto salva log", "A", "ElencoLogsScreen");
      }

      try {
        WriteLogs(true);
      } catch(e) {
        addLog("Errore funzione di WriteLog e Salvataggio log nel server: ${e.toString()}", "X", "ElencoLogsScreen");
      }
    }

    if (!Initialized) {
      Initialized = true;

      restoreMenuContext();
      getDirectoryApplicazione();
      AzzeraParametri();
      getParametri();

      CheckListaLogs();
    }

    return Scaffold(
      key: scaffoldKEY,
      appBar: AppBar(
        backgroundColor: colore1,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, null),
        ),
        title: Text(
          "Elenco Logs",
          style: TextStyle(
            fontSize: 16,
            //fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          Visibility(
              visible: true,
              child: IconButton(
                  key: menuKEY,
                  tooltip: "Menu'",
                  icon: Icon(Icons.filter_list),
                  onPressed: () {
                    addLog("Apertura menu' popup", "A", "ElencoLogsScreen");
                    onPressedMenu();
                  })),
          Chip(
            label: Text(
              (_lista_logs.length).toString(),
              style: TextStyle(fontWeight: FontWeight.bold, color: colore1),
            ),
            backgroundColor: Colors.white,
          ),
          Padding(
            padding: EdgeInsets.only(right: 16),
          )
        ],
      ),
      body: SafeArea(
        top: false,
        bottom: false,
        child: Form(
          key: formKEY,
          child: RawKeyboardListener(
            focusNode: FocusNode(),
            onKey: (RawKeyEvent event) {
              if (event.runtimeType == RawKeyDownEvent &&
                  event.logicalKey == LogicalKeyboardKey.enter) {
              }
            },
            child: ListView.separated(
              physics: bouncingScroll,
              itemCount: _lista_logs?.length ?? 0,
              separatorBuilder: (context, index) => Divider(),
              itemBuilder: (BuildContext context, int index) {
                return new Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      title: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 4.0, left: 0.0, right: 4.0, bottom: 0.0),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width /
                                      10 * 1.5,
                                  child: Align(
                                    alignment:
                                    Alignment.centerLeft,
                                    child: Card(
                                      color: colore1,
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(
                                            top: 4.0,
                                            bottom: 4.0,
                                            left: 4.0,
                                            right: 4.0),
                                        child: Text(
                                          "${(_lista_logs[index].progr + 1).toString()}",
                                          style: TextStyle(
                                              fontWeight:
                                              FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width /
                                      10 *
                                      7,
                                  child: Align(
                                    alignment:
                                    Alignment.centerLeft,
                                    child: Card(
                                      color: colore2,
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(
                                            top: 4.0,
                                            bottom: 4.0,
                                            left: 4.0,
                                            right: 4.0),
                                        child: Text(
                                          "${_lista_logs[index].data_ora}",
                                          style: TextStyle(
                                              fontWeight:
                                              FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 14),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ),
                          Visibility(
                              visible: true,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 4.0,
                                    left: 4.0,
                                    right: 0.0,
                                    bottom: 0.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                      "${_lista_logs[index].testo}",
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal,
                                      )),
                                ),
                              )),
                Visibility(
                visible: (_mostra_campi),
                child: Padding(
                            padding: const EdgeInsets.only(left: 0),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: TextField(
                                      readOnly: true,
                                      controller: TextEditingController(
                                        text: _lista_logs[index].categoria,
                                      ),
                                      decoration: InputDecoration(
                                          labelText: "Categoria",
                                          labelStyle:
                                          TextStyle(fontSize: 14)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: TextField(
                                      readOnly: true,
                                      controller: TextEditingController(
                                        text: _lista_logs[index].maschera,
                                      ),
                                      decoration: InputDecoration(
                                          labelText: "Maschera",
                                          labelStyle:
                                          TextStyle(fontSize: 14)),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width /
                                      10 *
                                      2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: TextField(
                                      readOnly: true,
                                      controller: TextEditingController(
                                        text: _lista_logs[index].utente,
                                      ),
                                      decoration: InputDecoration(
                                          labelText: "Utente",
                                          labelStyle:
                                          TextStyle(fontSize: 14)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),)
                        ],
                      ),
                      onTap: () {
                      },
                    ),
                  ],
                );
              },
            )
          ),
        ),
      ),
        floatingActionButton: Visibility(
            visible: true,
            child: FloatingActionButton.extended(
                tooltip: "Salva i log nel file",
                backgroundColor: colore1,
                onPressed: () {
                  onPressedSalvaLog();
                },
                icon: Icon(Icons.save,
                    color: Colors.white),
                label: Text("SALVA")))
    );
  }
}

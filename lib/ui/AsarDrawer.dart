import 'package:MesPicking/common/functions/LogScheduler.dart';
import 'dart:async';
import 'package:MesPicking/common/functions/LogManagment.dart';
import 'package:MesPicking/common/functions/ShowMessage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:MesPicking/common/functions/saveLogout.dart';
import 'package:MesPicking/model/json/CheckParametri.dart';
import 'package:MesPicking/model/json/CheckUtente.dart';
import 'package:MesPicking/service/CheckParametriService.dart';
import 'package:MesPicking/service/CheckUtenteService.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
CheckUtente _utente;
List<CheckParametri> _parametri;
String _nome_utente = "";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(4288362813);
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  PARAMETRI

class AppDrawer extends StatefulWidget {
  AppDrawer({
    Key key,
    this.callback,
  }) : super(key: key);
  final DrawerCallback callback;

  @override
  _AppDrawer createState() => new _AppDrawer();
}

class _AppDrawer extends State<AppDrawer> {
  Future getSharedPreferences() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      _nome_utente = await preferences.getString("username");
    }
    catch(e) {
    addLog("Errore caricamento shared preferences: ${e.toString()}", "X",
    "AsarDraweScreen");
    }
  }

  AzzeraUtente() {
    setState(() {
      _utente = new CheckUtente("", "", "", 0, "", 0, "", "", "", "", "", "", "");
    });
  }

  AzzeraParametri() {
    setState(() {
      _parametri = new List<CheckParametri>();

      setState(() {
        _parametri.add(new CheckParametri(0, "MOSTRALOGS", "", "N", "", 0, "", 0, "", ""));
      });
    });
  }

  Future getParametri() async {
    List<CheckParametri> check = await CheckParametriService.browse(filter: "");
    if (check.length > 0) {
      for (int i = 0; i < check.length; i++) {
        for (int j = 0; j < _parametri.length; j++) {
          if (check[i].codice_parametro ==  _parametri[j].codice_parametro) {
            setState(() {
              _parametri[j].id_parametro = check[i].id_parametro;
              _parametri[j].text_message = check[i].text_message;
              _parametri[j].num_message = check[i].num_message;
            });
          }
        }
      }
    }
  }

  Future validateUtente() async {
    if(GlobalConfiguration().getValue("tipo_log") == "TUTTI") {
      addLog("Get check utente (utente: $_nome_utente)", "E", "AsarDrawerScreen");
    }
    
    List<CheckUtente> check = await CheckUtenteService.browse(filter: "utente=$_nome_utente");
    if (check.length == 0) {
      addLog("Nessun utente restituito dal validate (utente: $_nome_utente)", "X", "AsarDrawerScreen");
      
      AzzeraUtente();
    } else {
      addLog("Utente restituito e valido (utente: $_nome_utente)", "X", "AsarDrawerScreen");
      
      try {
      setState(() {
        _utente.id_utente = check[0].id_utente;
        _utente.cognome = check[0].cognome;
        _utente.nome = check[0].nome;
        _utente.utente = check[0].utente;
        _utente.password = check[0].password;
        _utente.id_societa = check[0].id_societa;
        _utente.rag_sociale = check[0].rag_sociale;
        _utente.codice_societa = check[0].codice_societa;
        _utente.provincia = check[0].provincia;
        _utente.citta = check[0].citta;
        _utente.telefono = check[0].telefono;
        _utente.partita_iva = check[0].partita_iva;
        _utente.indirizzo = check[0].indirizzo;
      });
    }
    catch(e) {
    addLog("Errore caricamento shared preferences: ${e.toString()}", "X",
    "AsarDraweScreen");
    }
    }
  }

  displaySocieta(BuildContext context) async {
    if(GlobalConfiguration().getValue("tipo_log") == "TUTTI") {
      addLog("Apertura dialog societa", "E", "AsarDrawerScreen");
    }
    
    String message = "Societa': ${_utente.codice_societa} \n\n${_utente.indirizzo} \n${_utente.citta} (${_utente.provincia}) \nTelefono: ${_utente.telefono}";

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colore1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Center(
              child: Text(
                "${_utente.rag_sociale}",
                style: TextStyle(fontSize: 18.0, color: Colors.white),
              ),
            ),
            content: Text(message,
                style: TextStyle(fontSize: 16.0, color: Colors.white)),
            actions: <Widget>[
            ],
          );
        });
  }

  onPressedSocieta() {
    displaySocieta(context);
  }

  Future onPressedDisconnetti() async {
    addLog("Pressione tasto di logout", "A", "AsarDrawerScreen");

    WriteLogs();
    stopTimerLogTerminale();

    try {
      await saveLogout();
      Navigator.of(context).pushNamedAndRemoveUntil('/LoginScreen', (Route<dynamic> route) => false);
    } catch (e) {
      print(e.toString());
      addLog("Errore Logout: ${e.toString()}", "X", "AsarDrawerScreen");
    }
  }

  onSubmittedLogMessage() {
    String message = messageLogs("M");

    showMessageDuration(widget.key, message, 10000, colore1);
  }

  displayLogs(BuildContext context) async {
    if(GlobalConfiguration().getValue("tipo_log") == "TUTTI") {
      addLog("Apertura dialog log a video", "E", "AsarDrawerScreen");
    }
    
    String message = messageLogs("V");

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colore1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Center(
              child: Text(
                "Elenco Log",
                style: TextStyle(fontSize: 18.0, color: Colors.white),
              ),
            ),
            content: Text( message,
                style: TextStyle(fontSize: 10.0, color: Colors.white)),
            actions: <Widget>[
            ],
          );
        });
  }

  onPressedMostraLogs() {
    addLog("Pressione tasto di mostra log", "A", "AsarDrawerScreen");

    //M = messaggio a video
    //S = apertura screen/maschera dedicata
    //P = print su terminale
    //F = apertura file log
    //N = nessuno
    //C = scegli manualmente
    //V = display a video

    if(_parametri[0].text_message == "M") {
      //displayLogs(context);
      Navigator.of(context).pushNamed('/ElencoLogsScreen');
    } else if(_parametri[0].text_message == "V") {
      onSubmittedLogMessage();
    } else if(_parametri[0].text_message == "S") {
      Navigator.of(context).pushNamed('/ElencoLogsScreen');
    } else if(_parametri[0].text_message == "P") {
      print(messageLogs("V").toString());
    }
  }

  onPressedAggiornamento() {
    addLog("Pressione tasto di aggiornamento", "A", "AsarDrawerScreen");

    Navigator.of(context).pushNamed('/AggiornamentoScreen');
  }

  @override
  initState() {
    if (widget.callback != null) {
      widget.callback(true);
    }

    AzzeraUtente();
    AzzeraParametri();
    getParametri();
    validateUtente();

    super.initState();
  }

  @override
  dispose() {
    if (widget.callback != null) {
      widget.callback(false);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getSharedPreferences();

    return Drawer(
      key: widget.key,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountEmail: Text(""),
              accountName: Text("${_utente.nome} ${_utente.cognome}"),
              otherAccountsPictures: <Widget>[
                Visibility(
                  visible: true,
                  child: FloatingActionButton(
                    tooltip: "Info Societa'",
                    heroTag: "soc",
                    backgroundColor: colore2,
                    onPressed: () async {
                      onPressedSocieta();
                    },
                    child: Icon(Icons.info, color: Colors.white),
                  ),
                )
              ],
              currentAccountPicture: CircleAvatar(
                child: Text(
                  "${_utente.utente}",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                backgroundColor: colore2,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 0.25, bottom: 0.25),
              child: FlatButton(
                onPressed: () {
                  onPressedAggiornamento();
                },
                color: Color(0xffD6D2D1),
                padding: EdgeInsets.only(left: 20.0, top: 8.0, bottom: 8.0),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.system_update),
                    Text("  Aggiornamento")
                  ]
                )
              )
            ),
            Visibility(
              visible: (_parametri[0].text_message != "N"),
              child:  Padding(
                  padding: const EdgeInsets.only(top: 0.25, bottom: 0.25),
                  child: FlatButton(
                      onPressed: () {
                        onPressedMostraLogs();
                      },
                      color: Color(0xffD6D2D1),
                      padding: EdgeInsets.only(left: 20.0, top: 8.0, bottom: 8.0),
                      child: Row(
                          children: <Widget>[
                            Icon(Icons.event_note),
                            Text("  Mostra Logs")
                          ]
                      )
                  )
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 0.25, bottom: 0.25),
              child: FlatButton(
                onPressed: () {
                  onPressedDisconnetti();
                },
                color: Color(0xffD6D2D1),
                padding: EdgeInsets.only(left: 20.0, top: 8.0, bottom: 8.0),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.do_not_disturb),
                    Text("  Logout")
                  ]
                )
              )
            )
          ]
        )
      )
    );
  }
}

class CanaleFlutterAndroid {
  static MethodChannel platformMethodChannel = const MethodChannel("CANALE");
}

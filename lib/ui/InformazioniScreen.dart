import 'package:MesPicking/common/functions/LogManagment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
String _versione_attuale = "";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(4288362813);
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  PARAMETRI

class InformazioniScreen extends StatefulWidget {
  @override
  _InformazioniScreen createState() => new _InformazioniScreen();
}

class _InformazioniScreen extends State<InformazioniScreen> {
  final GlobalKey<ScaffoldState> scaffoldKEY = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKEY = new GlobalKey<FormState>();

  bool Initialized = false;

  Future getCurrentVersion() async {
    if(GlobalConfiguration().getValue("tipo_log") == "TUTTI") {
      addLog("Ottenimento versione attuale app", "E", "InformazioniScreen");
    }

    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      setState(() {
        _versione_attuale = packageInfo.version;
      });
    } catch(e) {
      addLog("Errore ottenimento versione attuale app: ${e.toString()}", "X", "InformazioniScreen");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!Initialized) {
      Initialized = true;

      getCurrentVersion();
    }

    return Scaffold(
      key: scaffoldKEY,
      appBar: AppBar(
        backgroundColor: colore1,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, null),
        ),
        title: Text("Informazioni App"),
        actions: <Widget>[],
      ),
      body: SafeArea(
        bottom: false,
        top: false,
        child: Form(
          key: formKEY,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 4,
                  decoration: BoxDecoration(
                      color: colore1),
                  alignment: FractionalOffset(0.5, 0.3),
                  child: Text(
                    "MES Picking App",
                    style: TextStyle(fontSize: 40.0, color: Colors.white),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Center(
                          child: Align(
                            alignment: Alignment.center,
                            child: Card(
                              color: colore2,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  "Versione app: $_versione_attuale",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Row(
                    children: <Widget>[
                      Visibility(
                        visible: (true),
                        child: Expanded(
                          child: new Center(
                            child: FlatButton.icon(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16.0),
                                    side: BorderSide(color: colore1)),
                                color: colore1,
                                onPressed: () {
                                  if(GlobalConfiguration().getValue("tipo_log") == "TUTTI") {
                                    addLog("Lancio apertura sito web quadrivium (https://quadrivium.it/)", "A", "InformazioniScreen");
                                  }

                                  launch('https://quadrivium.it/');
                                },
                                label: Text("Sito Web Quadrivium Srl",
                                    style: TextStyle(color: Colors.white)),
                                icon: Icon(Icons.wifi, color: Colors.white)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 100.0, left: 8, right: 8, bottom: 4),
                  child: Center(
                    child: Card(
                        color: colore5,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 0, left: 8.0, right: 8.0),
                          child: Row(
                            children: <Widget>[
                              Container(
                                width:
                                MediaQuery.of(context).size.width /
                                    10 *
                                    7,
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Padding(
                                        padding:
                                        const EdgeInsets.all(8.0),
                                        child: TextField(
                                          readOnly: true,
                                          autofocus: false,
                                          controller: TextEditingController(text: GlobalConfiguration().getString("asar_server")),
                                          decoration: InputDecoration(
                                            labelText: "Indirizzo Server Node",
                                            errorText: null,
                                          ),
                                          maxLines: null,
                                        ))),
                              ),
                            ],
                          ),
                        )),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart';
import 'package:MesPicking/common/functions/ShowMessage.dart';
import 'package:MesPicking/common/functions/FileManagment.dart';
import 'package:MesPicking/model/json/CheckParametri.dart';
import 'package:MesPicking/service/CheckParametriService.dart';
import 'package:MesPicking/service/PermissionService.dart';
import 'package:path_provider/path_provider.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  CAPTIONS
Map<String, String> values = new Map<String, String>();
String _lbl_download = "   DOWNLOAD";
String _lbl_apri_file = "   APRI FILE";
String _lbl_apri_app = "   APRI APPLICAZIONE";
String _nome_maschera = "Downloads Utili";

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
String _directory_applicazione = "";
String _directory_downloads = "";
bool _loading = false;
String _percentuale_loading = "0%";
String _valore_percentuale_loading = "0";
String _applicazione_loading = "";
StreamSubscription _streamSubscription = null;

List<ApkDownloads> _applicazioni = new List<ApkDownloads>();
List<CheckParametri> _parametri;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(4288362813);
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  PARAMETRI

class ElencoDownloadsScreen extends StatefulWidget {
  @override
  _ElencoDownloadsScreen createState() => new _ElencoDownloadsScreen();
}

class _ElencoDownloadsScreen extends State<ElencoDownloadsScreen> {
  BouncingScrollPhysics bouncingScroll = new BouncingScrollPhysics();
  final GlobalKey<ScaffoldState> scaffoldKEY = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKEY = new GlobalKey<FormState>();

  bool Initialized = false;
  static var httpClient = new HttpClient();

  AzzeraParametri() {
    setState(() {
      _parametri = new List<CheckParametri>();

      setState(() {
        _parametri.add(new CheckParametri(0, "NOMEAPKAPP", "", "odp_picking.apk", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEAPKANYDESK", "", "anydesk.apk", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEAPKPLUGIN", "", "anydesk-plugin.apk", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEFILEVERSIONE", "", "version.txt", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEMANUALE", "", "manuale.pdf", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "PERCORSONODEAPP", "", "http://13.93.33.246:8080/gmenode/ODP Picking/", "", 0, "", 0, "", ""));
        _parametri.add(new CheckParametri(0, "NOMEAPKJSONEDITOR", "", "json_editor.apk", "", 0, "", 0, "", ""));
      });
    });
  }

  Future getParametri() async {
    List<CheckParametri> check = await CheckParametriService.browse(filter: "");
    if (check.length > 0) {
      for (int i = 0; i < check.length; i++) {
        for (int j = 0; j < _parametri.length; j++) {
          if (check[i].codice_parametro == _parametri[j].codice_parametro) {
            setState(() {
              _parametri[j].id_parametro = check[i].id_parametro;
              _parametri[j].text_message = check[i].text_message;
              _parametri[j].num_message = check[i].num_message;
            });
          }
        }
      }
    }
  }

  Future AzzeraApplicazioni() async {
    setState(() {
      _applicazioni.add(new ApkDownloads(
          "ANYDESK",
          "L'applicazione Anydesk permette di eseguire il controllo remoto del terminale da un'altro dispositivo o da un computer",
          _parametri[1].text_message,
          "com.anydesk.anydeskandroid",
          false,false));
      _applicazioni.add(new ApkDownloads(
          "PLUGIN ANYDESK AD1",
          "Plugin di Anydesk per il controllo remoto da PC",
          _parametri[2].text_message,
          "",
          false,false));
      _applicazioni.add(new ApkDownloads(
          "MES Picking",
          "File di installazione dell'ultima versione dell'applicazione per il picking MES ",
          _parametri[0].text_message,
          "it.quadrivium.MesPicking",
          false,false));
      _applicazioni.add(new ApkDownloads(
          "MANUALE",
          "Manuale dell'applicazione ",
          _parametri[4].text_message,
          "",
          false,false));
      _applicazioni.add(new ApkDownloads(
          "JSON Editor",
          "File di installazione dell'app per la modifica file di configurazione",
          _parametri[6].text_message,
          "",
          false,false));
    });

    await getDirectoryApp().then((v) async {
      await getDirectoryDownloads();
    });
  }

  Future DownloadFileServer(int i) async {
    setState(() {
      _applicazione_loading = _applicazioni[i].nome;

      _loading = true;
      _valore_percentuale_loading = "0";
      _percentuale_loading = "0%";
    });

    await DownloadFile("${_parametri[5].text_message}${_applicazioni[i].nome_apk}","$_directory_downloads/${_applicazioni[i].nome_apk}");
  }

  Initialization() {
    setState(() {
      _loading = false;
      _applicazioni = new List<ApkDownloads>();
      _valore_percentuale_loading = "0";
      _percentuale_loading = "0%";
      _applicazione_loading = "";
    });
    AzzeraParametri();
    AzzeraApplicazioni();
  }

  Future onPressedDownload(int i) async {
    await PermissionsService().hasStoragePermission().then((val) async {
      if (val != null) {
        if (val) {
          setState(() {
            _loading = true;
          });
          await DownloadFileServer(i).then((v2) async {
            setState(() {
              _loading = false;
            });
            await CheckExistFileTerminale(i).then((v1) {
              if (_applicazioni[i].exist_terminale) {
                showMessage(scaffoldKEY, "File scaricato nella cartella: ${_directory_downloads}", colore1);
                displayAperturaApk(context, i);
              } else {
                showMessage(scaffoldKEY, "Il file non e' stato scaricato correttamente");
              }
            });
          });
        } else {
          await PermissionsService().requestStoragePermission(
              onPermissionDenied: () {
            print('Permission has been denied');
          }).then((val2) async {
            if (val2 != null) {
              if (val2) {
                setState(() {
                  _loading = true;
                });
                await DownloadFileServer(i).then((v2) async {
                  setState(() {
                    _loading = false;
                  });
                  await CheckExistFileTerminale(i).then((v1) {
                    if (_applicazioni[i].exist_terminale) {
                      showMessage(
                          scaffoldKEY,
                          "File scaricato nella cartella: $_directory_downloads",
                          colore1);
                      displayAperturaApk(context, i);
                    } else {
                      showMessage(scaffoldKEY,
                          "Il file non e' stato scaricato correttamente");
                    }
                  });
                });
              }
            }
          });
        }
      } else {
        await PermissionsService().requestStoragePermission(
            onPermissionDenied: () {
          print('Permission has been denied');
        }).then((val2) async {
          if (val2 != null) {
            if (val2) {
              setState(() {
                _loading = true;
              });
              await DownloadFileServer(i).then((v2) async {
                setState(() {
                  _loading = false;
                });
                await CheckExistFileTerminale(i).then((v1) {
                  if (_applicazioni[i].exist_terminale) {
                    showMessage(
                        scaffoldKEY,
                        "File scaricato nella cartella: $_directory_downloads",
                        colore1);
                    displayAperturaApk(context, i);
                  } else {
                    showMessage(scaffoldKEY,
                        "Il file non e' stato scaricato correttamente");
                  }
                });
              });
            }
          }
        });
      }
    });
  }

  Future DownloadFile(String url, String percorso) async {
    try {
      final request = Request('GET', Uri.parse(url));
      final StreamedResponse response = await Client().send(request);

      List<List<int>> chunks = new List();
      int downloaded = 0;

      _streamSubscription = null;

      _streamSubscription = await response.stream
          .listen((List<int> chunk) {
            setState(() {
              _valore_percentuale_loading =
                  (downloaded / response.contentLength * 100)
                      .toStringAsFixed(0);
              _percentuale_loading = (downloaded / response.contentLength * 100)
                      .toStringAsFixed(0) +
                  "%";
            });
            chunks.add(chunk);
            downloaded += chunk.length;
          }, onDone: () async {
          }, onError: (err) {
            print(err);
            showMessage(scaffoldKEY, err.toString());
            setState(() {
              _streamSubscription = null;
            });
          }, cancelOnError: true)
          .asFuture()
          .then((val) async {
            setState(() {
              _valore_percentuale_loading =
                  (downloaded / response.contentLength * 100)
                      .toStringAsFixed(0);
              _percentuale_loading = (downloaded / response.contentLength * 100)
                      .toStringAsFixed(0) +
                  "%";
            });
            final Uint8List bytes = Uint8List(response.contentLength);
            int offset = 0;
            for (List<int> chunk in chunks) {
              bytes.setRange(offset, offset + chunk.length, chunk);
              offset += chunk.length;
            }
            File file = await new File(percorso);
            await file.writeAsBytes(bytes).then((val) async {
              setState(() {
                _streamSubscription = null;
              });
            });
          });
    } catch (e) {
      print(e);
      showMessage(scaffoldKEY, e.toString());
      setState(() {
        _streamSubscription = null;
      });
    }
  }

  Future CheckExistFileTerminale(int i) async {
    if (await File("${_directory_downloads}/${_applicazioni[i].nome_apk}").exists()) {
      _applicazioni[i].exist_terminale = true;
    } else {
      _applicazioni[i].exist_terminale = false;
    }

    setState(() {});
  }

  Future getDirectoryApp() async {
    _directory_applicazione = (await getExternalStorageDirectory()).path;
  }

  Future getDirectoryDownloads() async {
    _directory_downloads = "/storage/emulated/0/Download";
    // _directory_downloads = _directory_applicazione;
  }

  Future CheckExistAppTerminale(int i) async {
    await checkExistApplication(_applicazioni[i].nome_pacchetto, _applicazioni[i].nome).then((val) {
      if(val != null) {
      setState(() {
        _applicazioni[i].exist_app = val;
      });
      } else {
        setState(() {
          _applicazioni[i].exist_app = false;
        });
      }
    });
  }

  Future onPressedApriApplicazione(int i) async {
   await CheckExistAppTerminale(i).then((v) async {
      if( _applicazioni[i].exist_app) {
        await openApplication(
            _applicazioni[i].nome_pacchetto, _applicazioni[i].nome);
      } else {
        showMessage(scaffoldKEY, "L'applicazione " +  _applicazioni[i].nome + " non e' installata nel dispositivo");
      }
    });
  }

  displayAperturaApk(BuildContext context, int i) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colore1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Text(
                "Vuoi aprire il file di installazione dell'app ${_applicazioni[i].nome}?",
                style: TextStyle(color: Colors.white)),
            actions: <Widget>[
              FlatButton(
                child: Text("NO",
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                onPressed: () {
                  Navigator.pop(context, false);
                },
              ),
              FlatButton(
                child: Text("SI",
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                onPressed: () {
                  Navigator.pop(context, true);
                },
              ),
            ],
          );
        }).then((val) async {
      if (val != null) {
        if (val) {
          openFile("$_directory_downloads/${_applicazioni[i].nome_apk}").then((v) async {
            await CheckExistFileTerminale(i);
          });
        } else {
          await CheckExistFileTerminale(i);
        }
      } else {
        await CheckExistFileTerminale(i);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!Initialized) {
      Initialized = true;

      Initialization();
      getParametri();
    }

    return Scaffold(
      key: scaffoldKEY,
      appBar: AppBar(
        backgroundColor: colore1,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, null),
        ),
        title: Text(_nome_maschera),
        actions: <Widget>[],
      ),
      body: SafeArea(
          bottom: false,
          top: false,
          child: RawKeyboardListener(
            focusNode: FocusNode(),
            onKey: (RawKeyEvent event) {
              if (event.runtimeType == RawKeyDownEvent &&
                  event.logicalKey == LogicalKeyboardKey.enter) {}
            },
            child: Form(
              key: formKEY,
              child: (_applicazioni == null || _applicazioni.length == 0)
                  ? Center(
                      child: Padding(
                        padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Center(
                              child: SizedBox(
                                child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            colore1)),
                                width: 45,
                                height: 45,
                              ),
                            ),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.only(top: 16),
                                child: Center(
                                  child: Text('Attendere...'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : ListView(
                      children: <Widget>[
                        Visibility(
                            visible: (_loading),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: Center(
                                  child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Download $_applicazione_loading...",
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  CircularPercentIndicator(
                                      radius: 125.0,
                                      lineWidth: 10.0,
                                      percent: (double.parse(
                                              _valore_percentuale_loading) /
                                          100),
                                      center: Text(
                                        _percentuale_loading,
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            color: colore1,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      backgroundColor: Colors.grey,
                                      progressColor: colore1)
                                ],
                              )),
                            )),
                        Visibility(
                          visible: (true),
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: ExpansionTile(
                                title: Text(
                                  _applicazioni[0].nome,
                                  style: TextStyle(
                                      color: colore2,
                                      fontWeight: FontWeight.bold),
                                ),
                                leading: Icon(
                                  Icons.remove_red_eye,
                                  color: colore2,
                                ),
                                children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.all(16),
                                      child: Text(
                                        _applicazioni[0].descr,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.normal),
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 0.25, bottom: 0.25),
                                    child: FlatButton(
                                      onPressed: () async {
                                        await onPressedDownload(0);
                                      },
                                      color: Color(0xffD6D2D1),
                                      padding: EdgeInsets.only(
                                          left: 20.0, top: 8.0, bottom: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.file_download),
                                          Text(_lbl_download)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: (_applicazioni[0].exist_terminale),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.25, bottom: 0.25),
                                      child: (_applicazioni[0].exist_app) ? FlatButton(
                                        onPressed: () {
                                          onPressedApriApplicazione(0);
                                        },
                                        color: Color(0xffD6D2D1),
                                        padding: EdgeInsets.only(
                                            left: 20.0, top: 8.0, bottom: 8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.open_with),
                                            Text(_lbl_apri_app)
                                          ],
                                        ),
                                      ) :
                                      FlatButton(
                                        onPressed: () {
                                          displayAperturaApk(context, 0);
                                        },
                                        color: Color(0xffD6D2D1),
                                        padding: EdgeInsets.only(
                                            left: 20.0, top: 8.0, bottom: 8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.open_in_new),
                                            Text(_lbl_apri_file)
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                                onExpansionChanged: (val) async {
                                  if (val) {
                                    await CheckExistFileTerminale(0).then((v) async {
                                      await CheckExistAppTerminale(0);
                                    });
                                  }
                                }),
                          ),
                        ),
                        Visibility(
                          visible: (true),
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: ExpansionTile(
                                title: Text(
                                  _applicazioni[1].nome,
                                  style: TextStyle(
                                      color: colore2,
                                      fontWeight: FontWeight.bold),
                                ),
                                leading: Icon(
                                  Icons.desktop_mac,
                                  color: colore2,
                                ),
                                children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.all(16),
                                      child: Text(
                                        _applicazioni[1].descr,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.normal),
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 0.25, bottom: 0.25),
                                    child: FlatButton(
                                      onPressed: () async {
                                        await onPressedDownload(1);
                                      },
                                      color: Color(0xffD6D2D1),
                                      padding: EdgeInsets.only(
                                          left: 20.0, top: 8.0, bottom: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.file_download),
                                          Text(_lbl_download)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: (_applicazioni[1].exist_terminale),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.25, bottom: 0.25),
                                      child: FlatButton(
                                        onPressed: () {
                                          displayAperturaApk(context, 1);
                                        },
                                        color: Color(0xffD6D2D1),
                                        padding: EdgeInsets.only(
                                            left: 20.0, top: 8.0, bottom: 8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.open_in_new),
                                            Text(_lbl_apri_file)
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                                onExpansionChanged: (val) async {
                                  if (val) {
                                    await CheckExistFileTerminale(1);
                                  }
                                }),
                          ),
                        ),
                        Visibility(
                          visible: (true),
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: ExpansionTile(
                                title: Text(
                                  _applicazioni[2].nome,
                                  style: TextStyle(
                                      color: colore2,
                                      fontWeight: FontWeight.bold),
                                ),
                                leading: Icon(
                                  Icons.bug_report,
                                  color: colore2,
                                ),
                                children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.all(16),
                                      child: Text(
                                        _applicazioni[2].descr,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.normal),
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 0.25, bottom: 0.25),
                                    child: FlatButton(
                                      onPressed: () async {
                                        await onPressedDownload(2);
                                      },
                                      color: Color(0xffD6D2D1),
                                      padding: EdgeInsets.only(
                                          left: 20.0, top: 8.0, bottom: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.file_download),
                                          Text(_lbl_download)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: (_applicazioni[2].exist_terminale),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.25, bottom: 0.25),
                                      child: FlatButton(
                                        onPressed: () {
                                          displayAperturaApk(context, 2);
                                        },
                                        color: Color(0xffD6D2D1),
                                        padding: EdgeInsets.only(
                                            left: 20.0, top: 8.0, bottom: 8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.open_in_new),
                                            Text(_lbl_apri_file)
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                                onExpansionChanged: (val) async {
                                  if (val) {
                                    await CheckExistFileTerminale(2);
                                  }
                                }),
                          ),
                        ),
                        Visibility(
                          visible: (true),
                          child: Padding(
                            padding:
                            const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: ExpansionTile(
                                title: Text(
                                  _applicazioni[4].nome,
                                  style: TextStyle(
                                      color: colore2,
                                      fontWeight: FontWeight.bold),
                                ),
                                leading: Icon(
                                  Icons.edit,
                                  color: colore2,
                                ),
                                children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.all(16),
                                      child: Text(
                                        _applicazioni[4].descr,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.normal),
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 0.25, bottom: 0.25),
                                    child: FlatButton(
                                      onPressed: () async {
                                        await onPressedDownload(4);
                                      },
                                      color: Color(0xffD6D2D1),
                                      padding: EdgeInsets.only(
                                          left: 20.0, top: 8.0, bottom: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.file_download),
                                          Text(_lbl_download)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: (_applicazioni[4].exist_terminale),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.25, bottom: 0.25),
                                      child: FlatButton(
                                        onPressed: () {
                                          displayAperturaApk(context, 4);
                                        },
                                        color: Color(0xffD6D2D1),
                                        padding: EdgeInsets.only(
                                            left: 20.0, top: 8.0, bottom: 8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.open_in_new),
                                            Text(_lbl_apri_file)
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                                onExpansionChanged: (val) async {
                                  if (val) {
                                    await CheckExistFileTerminale(4);
                                  }
                                }),
                          ),
                        ),
                        Visibility(
                          visible: true,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: ExpansionTile(
                                title: Text(
                                  _applicazioni[3].nome,
                                  style: TextStyle(
                                      color: colore2,
                                      fontWeight: FontWeight.bold),
                                ),
                                leading: Icon(
                                  Icons.library_books,
                                  color: colore2,
                                ),
                                children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.all(16),
                                      child: Text(
                                        _applicazioni[3].descr,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.normal),
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 0.25, bottom: 0.25),
                                    child: FlatButton(
                                      onPressed: () async {
                                        await onPressedDownload(3);
                                      },
                                      color: Color(0xffD6D2D1),
                                      padding: EdgeInsets.only(
                                          left: 20.0, top: 8.0, bottom: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.file_download),
                                          Text(_lbl_download)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: (_applicazioni[3].exist_terminale),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.25, bottom: 0.25),
                                      child: FlatButton(
                                        onPressed: () {
                                          displayAperturaApk(context,3);
                                        },
                                        color: Color(0xffD6D2D1),
                                        padding: EdgeInsets.only(
                                            left: 20.0, top: 8.0, bottom: 8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.open_in_new),
                                            Text(_lbl_apri_file)
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                                onExpansionChanged: (val) async {
                                  if (val) {
                                    await CheckExistFileTerminale(3);
                                  }
                                }),
                          ),
                        ),
                      ],
                    ),
            ),
          )),
    );
  }
}

class ApkDownloads {
  String nome;
  String descr;
  String nome_apk;
  String nome_pacchetto;
  bool exist_terminale;
  bool exist_app;

  ApkDownloads(this.nome, this.descr, this.nome_apk,this.nome_pacchetto, this.exist_terminale,this.exist_app);
}

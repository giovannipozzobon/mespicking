class CheckParametri {
  int id_parametro;
  String codice_parametro;
  String descr_parametro;
  String text_message;
  String nota;
  int num_message;
  String fl_attivo;
  int id_testata_parametro;
  String codice_testata_parametro;
  String descr_testata_parametro;

  CheckParametri(this.id_parametro, this.codice_parametro, this.descr_parametro, this.text_message, this.nota, this.num_message, this.fl_attivo, this.id_testata_parametro, this.codice_testata_parametro, this.descr_testata_parametro);

  CheckParametri.fromJson(Map<String, dynamic> json)
      :
        codice_parametro = json['codice_parametro'],
        descr_parametro = json['descr_parametro'],
        id_parametro = json['id_parametro'],
        fl_attivo = json['fl_attivo'],
        nota = json['nota'],
        codice_testata_parametro = json['codice_testata_parametro'],
        descr_testata_parametro = json['descr_testata_parametro'],
        id_testata_parametro = json['id_testata_parametro'],
        text_message = json['text_message'],
        num_message = json['num_message'];
}
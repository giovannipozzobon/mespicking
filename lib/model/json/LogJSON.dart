class LogJSON {
  int progr = 0;
  String testo = "";
  String categoria = "";
  String maschera = "";
  String data_ora = "";
  String utente = "";

  LogJSON (this.progr, this.testo, this.categoria, this.maschera, this.data_ora, this.utente);

  LogJSON.fromJson(Map<String, dynamic> json)
      : progr = json['progr'],
        testo = json['testo'],
        categoria = json['categoria'],
        maschera = json['maschera'],
        data_ora = json['data_ora'],
        utente = json['utente'];

  Map<String, dynamic> toJson() => {
    'progr': progr,
    'testo': testo,
    'categoria': categoria,
    'maschera': maschera,
    'data_ora': data_ora,
    'utente': utente,
  };

  static List encondeToJson(List<LogJSON>list){
    List jsonList = List();
    list.map((item)=>
        jsonList.add(item.toJson())
    ).toList();
    return jsonList;
  }
}

class Log {
  int progr = 0;
  String testo = "";
  String categoria = "";
  String maschera = "";
  String data_ora = "";
  String utente = "";
  bool written_terminale = false;
  bool written_server = false;

  Log (this.progr, this.testo, this.categoria, this.maschera, this.data_ora, this.utente, this.written_terminale, this.written_server);
}
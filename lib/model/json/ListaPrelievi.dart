class ListaPrelievi {
  final int id_testata;
  final String codice_testata;
  final String data_testata;
  final String stato_testata;
  final int id_dettaglio;
  final String stato_dettaglio;
  final String data_prelievo;
  final double quantita;
  final double quantita_prelevata;
  final String codice_articolo;
  final int id_articolo;
  final String descr_articolo;
  final String codice_cella;
  final int id_cella;
  final String nome_utente;
  final int id_utente;
  final int num_dettagli_aperti;
  final int num_dettagli_chiusi;
  final String fl_trasferito;

  ListaPrelievi(this.id_testata, this.codice_testata, this.data_testata, this.stato_testata, this.id_dettaglio, this.stato_dettaglio, this.data_prelievo, this.quantita, this.quantita_prelevata, this.codice_articolo, this.id_articolo, this.descr_articolo, this.codice_cella, this.id_cella, this.nome_utente, this.id_utente, this.num_dettagli_aperti, this.num_dettagli_chiusi, this.fl_trasferito);

  ListaPrelievi.fromJson(Map<String, dynamic> json)
      : id_testata = json['id_testata'],
        codice_testata = json['codice_testata'],
        data_testata = json['data_testata'],
        stato_testata = json['stato_testata'],
        id_dettaglio = json['id_dettaglio'],
        stato_dettaglio = json['stato_dettaglio'],
        data_prelievo = json['data_prelievo'],
        codice_articolo = json['codice_articolo'],
        descr_articolo = json['descr_articolo'],
        codice_cella = json['codice_cella'],
        nome_utente = json['nome_utente'],
        id_utente = json['id_utente'],
        id_cella = json['id_cella'],
        id_articolo = json['id_articolo'],
        quantita = json['quantita'].toDouble(),
        quantita_prelevata = json['quantita_prelevata'].toDouble(),
        num_dettagli_aperti = json['num_dettagli_aperti'],
        num_dettagli_chiusi = json['num_dettagli_chiusi'],
        fl_trasferito  = json['fl_trasferito'];
}

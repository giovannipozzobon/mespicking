class LoginModel {
  final String userName;
  final String token;
  final String email;
  final int userId;
  final String first_name;
  final String second_name;
  final String login_code;

  LoginModel(this.userName, this.token, this.email, this.userId, this.first_name, this.second_name, this.login_code);

  LoginModel.fromJson(Map<String, dynamic> json)
      : userName = json['username'],
        token = json['token'],
        email = json['email'],
        userId = json['pk'],
        first_name = json['first_name'],
        second_name = json['second_name'],
        login_code = json['login_code'];

  Map<String, dynamic> toJson() =>
      {
        'name': userName,
        'token': token,
        'email': email,
        'pk': userId,
      };
}
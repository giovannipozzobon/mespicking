class PickingArticolo {
  final int id_articolo;
  final int id_documento;
  final double quantita_prelevata;
  final String stato;
  final String tipo;

  PickingArticolo(this.id_articolo, this.id_documento,this.quantita_prelevata, this.stato, this.tipo);

  Map<String, dynamic> toJson() => {
    'id_articolo': id_articolo,
    'id_documento': id_documento,
    'quantita_prelevata': quantita_prelevata,
    'stato': stato,
    'tipo': tipo,
  };

  static List encondeToJson(List<PickingArticolo>list){
    List jsonList = List();
    list.map((item)=>
        jsonList.add(item.toJson())
    ).toList();
    return jsonList;
  }
}

class UserProperties {
  final String target_server;

  UserProperties(this.target_server);

  UserProperties.fromJson(Map<String, dynamic> json)
      : target_server = json['target_server'];
}
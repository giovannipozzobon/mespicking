class CheckDocumento {
  int id_testata;
  String codice_testata;
  String data_testata;
  String stato_testata;
  String tipo_documento;
  String nome_utente;
  int id_utente;
  int num_dettagli_aperti;
  int num_dettagli_chiusi;

  CheckDocumento(this.id_testata, this.codice_testata, this.data_testata, this.stato_testata, this.tipo_documento, this.nome_utente, this.id_utente, this.num_dettagli_aperti, this.num_dettagli_chiusi);

  CheckDocumento.fromJson(Map<String, dynamic> json)
      : id_testata = json['id_testata'],
        codice_testata = json['codice_testata'],
        data_testata = json['data_testata'],
        stato_testata = json['stato_testata'],
        tipo_documento = json['tipo_documento'],
        nome_utente = json['nome_utente'],
        id_utente = json['id_utente'],
        num_dettagli_aperti = json['num_dettagli_aperti'],
        num_dettagli_chiusi = json['num_dettagli_chiusi'];
}

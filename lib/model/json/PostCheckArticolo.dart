class PostCheckArticolo {
  final String codice_articolo;
  final int id_documento;

  PostCheckArticolo(this.codice_articolo, this.id_documento);

  PostCheckArticolo.fromJson(Map<String, dynamic> json)
      : codice_articolo = json['codice_articolo'],
        id_documento = json['id_documento'];

  Map<String, dynamic> toJson() => {
    'codice_articolo': codice_articolo,
    'id_documento': id_documento,
  };

  static List encondeToJson(List<PostCheckArticolo>list){
    List jsonList = List();
    list.map((item)=>
        jsonList.add(item.toJson())
    ).toList();
    return jsonList;
  }
}

class CheckUtente {
  int id_utente;
  String cognome;
  String nome;
  String utente;
  String password;
  int id_societa;
  String rag_sociale;
  String codice_societa;
  String provincia;
  String citta;
  String telefono;
  String partita_iva;
  String indirizzo;

  CheckUtente(this.utente, this.cognome, this.nome, this.id_utente, this.password, this.id_societa, this.rag_sociale, this.codice_societa, this.provincia, this.citta, this.telefono, this.partita_iva, this.indirizzo);

  CheckUtente.fromJson(Map<String, dynamic> json)
      : id_utente = json['id_utente'],
        cognome = json['cognome'],
        nome = json['nome'],
        utente = json['utente'],
        password = json['password'],
        id_societa = json['id_societa'],
        rag_sociale = json['rag_sociale'],
        codice_societa = json['codice_societa'],
        provincia = json['provincia'],
        citta = json['citta'],
        telefono = json['telefono'],
        partita_iva = json['partita_iva'],
        indirizzo = json['indirizzo']
  ;

}

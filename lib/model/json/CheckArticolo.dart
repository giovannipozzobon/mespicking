class CheckArticolo {
  String codice_articolo;
  String descr_articolo;
  int id_articolo;
  int id_dettaglio;
  String stato_dettaglio;
  double quantita;
  double quantita_prelevata;
  int error;
  String messagge;
  bool result;

  CheckArticolo(this.codice_articolo, this.descr_articolo, this.id_articolo, this.id_dettaglio, this.stato_dettaglio, this.quantita, this.quantita_prelevata, this.error, this.messagge, this.result);
}

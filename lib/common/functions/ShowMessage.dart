import 'package:flutter/material.dart';
import 'package:flutter_beep/flutter_beep.dart';

int _suono_errore = 37;

void showMessage(GlobalKey<ScaffoldState> _scaffoldKey, String message, [Color color = Colors.red]) {
  if (color == Colors.red) {
    FlutterBeep.playSysSound(_suono_errore);
  }
  _scaffoldKey.currentState.showSnackBar(
      new SnackBar(backgroundColor: color, content: new Text(message)));
}

void showMessageDuration(GlobalKey<ScaffoldState> _scaffoldKey, String message, int millisecond, [Color color = Colors.red]) {
  if (color == Colors.red) {
    FlutterBeep.playSysSound(_suono_errore);
  }
  _scaffoldKey.currentState.showSnackBar(
      new SnackBar(backgroundColor: color,
          content: new Text(message),
          duration: Duration(milliseconds: millisecond)));
}

void showMessageAction(GlobalKey<ScaffoldState> _scaffoldKey, String message, int millisecond, SnackBarAction actions, [Color color = Colors.red]) {
  if (color == Colors.red) {
    FlutterBeep.playSysSound(_suono_errore);
  }
  _scaffoldKey.currentState.showSnackBar(
      new SnackBar(backgroundColor: color,
          content: new Text(message),
          duration: Duration(milliseconds: millisecond),
          action: actions));
}
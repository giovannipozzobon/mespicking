import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:MesPicking/common/functions/ShowMessage.dart';
import 'package:MesPicking/common/functions/getToken.dart';
import 'package:MesPicking/model/json/CheckArticolo.dart';

Future genericPostCheckArticolo(
    String address,
    String body,
    BuildContext context,
    GlobalKey<ScaffoldState> _scaffoldKey,
    bool showMessages) async {
  String _url = GlobalConfiguration().getString("asar_server") + address;
  var token;

  await getToken().then((result) {
    token = result;
  });

  final response = await http.post(
    _url,
    body: body,
    headers: {
      HttpHeaders.authorizationHeader: "$token",
      "Content-Type": "application/json"
    },
  ).catchError((test) {
    showMessage(_scaffoldKey, "Errore nella chiamata post!");
  });

  if (response.statusCode == 200) {
    final decoded = jsonDecode(response.body) as Map;
    final String codice_articolo = decoded['codice_articolo'];
    final String descr_articolo = decoded['descr_articolo'];
    final int id_articolo = decoded['id_articolo'];
    final int id_dettaglio = decoded['id_dettaglio'];
    final String stato_dettaglio = decoded['stato_dettaglio'];
    final double quantita = decoded['quantita'].toDouble();
    final double quantita_prelevata = decoded['quantita_prelevata'].toDouble();
    final int error = decoded['error'];
    final String messagge = decoded['messagge'];

    if (error == 0) {
      return new CheckArticolo(codice_articolo, descr_articolo, id_articolo,id_dettaglio,stato_dettaglio,quantita,quantita_prelevata,error,messagge, true);
    } else {
      showMessage(_scaffoldKey, messagge);
      return new CheckArticolo(codice_articolo, descr_articolo, id_articolo,id_dettaglio,stato_dettaglio,quantita,quantita_prelevata,error,messagge, true);
    }
  } else {
    showMessage(_scaffoldKey, 'Server error!');
    return new CheckArticolo("","",0,0,"",0,0,response.statusCode, response.reasonPhrase, false);
  }
}
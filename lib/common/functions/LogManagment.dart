import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:MesPicking/common/functions/FileManagment.dart';
import 'package:MesPicking/model/json/LogJSON.dart';
import 'package:dio/dio.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
List<Log> _logs = new List<Log>();
var _upload = Dio();
String _content = "";

resetLog(int num_indici, [bool only_written = true]) {
  if (num_indici > 0) {
    for (int i = 0; i < num_indici; i++) {
      if ((_logs[0].written_terminale == true && only_written) || only_written == false) {
        _logs.remove(_logs[0]);
      }
    }
  } else {
    _logs = new List<Log>();
  }
}

bool isNuoviLogs() {
    for (int i = 0; i < _logs.length; i++) {
      if (_logs[i].written_terminale == false) {
        return true;
      }
    }
    return false;
}

uploadLogFile() async {
  String nome_file = "TOWRITE_" + getNomeFileLog(GlobalConfiguration().getValue("nome_file_log").toString());
  String cartella = GlobalConfiguration().getValue("cartella_terminale").toString() + "/logs";
  String percorso = cartella + "/" + nome_file;
  String nome_file_server = getNomeFileLog(GlobalConfiguration().getValue("nome_file_log").toString());
  String cartella_server = GlobalConfiguration().getValue("cartella_log_server").toString();
  String percorso_server = cartella_server + "/" + nome_file_server;

  List<LogJSON> contenuto_nuovo = new List<LogJSON>();
  await leggiFileJsonLog(percorso).then((result) {
    contenuto_nuovo = result;
  });

  if(contenuto_nuovo.length > 0) {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = await preferences.getString('LastToken');
    Map<String, String> headers = {"authorization": token};

    String data_ora = DateFormat("yyyyMMdd_hhmm").format(new DateTime.now());
    String url = GlobalConfiguration().getString("asar_server") +
        "android/upload_logs";

    //PASSO I dati al server
    /*for (int i = 0; i < contenuto.length; i++) {
    try {
      FormData formData = new FormData.fromMap({
        "progr" : contenuto[i].progr.toString(),
        "testo" : contenuto[i].testo,
        "categoria" : contenuto[i].categoria,
        "maschera" : contenuto[i].maschera,
        "data_ora" : contenuto[i].data_ora,
        "utente" : contenuto[i].utente,
        "nome_file" : nome_file_server
        /*"wms_file": await MultipartFile.fromFile(
            percorso, filename: nome_file)*/
      });
      var response = await _upload.post(
          url,
          data: formData,
          options: Options(headers: headers));
      print(response.statusCode.toString());
      if (response.statusCode == 200) {
        //
      } else {
        //
      }
    } catch (e) {
      print(e.toString());
      //
    }
  }*/

    //una volta finito azzero il file
    await esisteFile(cartella, nome_file).then((esiste) async {
      if (!esiste) {
        createFile("[]", percorso);
      }
    });

    esisteFile(cartella, nome_file).then((esiste) async {
      if (esiste) {
        File file = await new File(percorso);
        file.writeAsStringSync("[]");
      } else {
        /////
      }
    });
  }
}

String testoLog(String mode) {
  if (mode == "TUTTI") {
    String logs = "";
    for (int i = 0; (i < _logs.length); i++) {
      if (_logs[i].written_terminale == false) {
        logs = logs + "${_logs[i].data_ora} -->  ${_logs[i].testo}  [${_logs[i].maschera}] \n       ${_logs[i].utente}  [${_logs[i].progr.toString()}]";
        logs = logs + "\n";
        _logs[i].written_terminale = true;
      }
    }

    return logs;
  } else if (mode == "ERRORI") {
    String logs = "";
    for (int i = 0; (i < _logs.length); i++) {
      if (_logs[i].written_terminale == false) {
        if(_logs[i].categoria == "ERRORE") {
            logs = logs + "${_logs[i].data_ora} -->  ${_logs[i].testo}  [${_logs[i].maschera}] \n       ${_logs[i].utente}  [${_logs[i].progr.toString()}]";
          logs = logs + "\n";
        }
        _logs[i].written_terminale = true;
      }
    }

    return logs;
  } else if (mode == "EVENTI") {
    String logs = "";
    for (int i = 0; (i < _logs.length); i++) {
      if (_logs[i].written_terminale == false) {
        if(_logs[i].categoria == "ERRORE" || _logs[i].categoria == "EVENTI") {
          logs = logs + "${_logs[i].data_ora} -->  ${_logs[i].testo}  [${_logs[i].maschera}] \n       ${_logs[i].utente}  [${_logs[i].progr.toString()}]";
          logs = logs + "\n";
        }
        _logs[i].written_terminale = true;
      }
    }

    return logs;
  }
}

List<Log> testoLogJson(String mode, [bool aggiorna_stato_terminale = true]) {
  List<Log> logs = new List<Log>();

  if (mode == "TUTTI") {
    for (int i = 0; (i < _logs.length); i++) {
      if (_logs[i].written_terminale == false) {
        logs.add(_logs[i]);
        if(aggiorna_stato_terminale) {
          _logs[i].written_terminale = true;
        }
      }
    }

    return logs;
  } else if (mode == "ERRORI") {
    for (int i = 0; (i < _logs.length); i++) {
      if (_logs[i].written_terminale == false) {
        if(_logs[i].categoria == "ERRORE") {
          logs.add(_logs[i]);
        }
        if(aggiorna_stato_terminale) {
          _logs[i].written_terminale = true;
        }
      }
    }

    return logs;
  } else if (mode == "EVENTI") {
    for (int i = 0; (i < _logs.length); i++) {
      if (_logs[i].written_terminale == false) {
        if(_logs[i].categoria == "ERRORE" || _logs[i].categoria == "EVENTI") {
          logs.add(_logs[i]);
        }
        if(aggiorna_stato_terminale) {
          _logs[i].written_terminale = true;
        }
      }
    }

    return logs;
  }
}

/*writeToFile(String key, String value) {
  Map<String, dynamic> content = {key: value};
  if (await File(percorso).exists()) {
    Map<String, dynamic> jsonFileContent =
    json.decode(fileJson.readAsStringSync());
    jsonFileContent.addAll(content);
    fileJson.writeAsStringSync(json.encode(jsonFileContent));
  } else {
    createFile(content, _directory, "config.json");
  }
}*/

Future<List<LogJSON>> leggiFileJsonLog(String percorso) async {
  List<LogJSON> l = new List<LogJSON>();

  if (await File(percorso).exists()) {
    try {
      final file = await File(percorso);

      String content = file.readAsStringSync();

      List collection = json.decode(content);

      Iterable<LogJSON> _resources = collection.map((_) => LogJSON.fromJson(_));

      l = _resources.toList();

      return l;
      /*Map<String, dynamic> jsonAllLog = json.decode(file.readAsStringSync());
      jsonAllLog.forEach((final String key, final value) {
        Map<String, String> map = new Map<String, String>();
        List<dynamic> jsonOneLog = jsonAllLog[key];



        /*for (var i = 0; i <= jsonOneLog.length - 1; i++) {
          Map<String, dynamic> jsonObj = jsonOneLog[i];
          jsonObj.forEach((final String key2, final value2) {
            List<dynamic> jsonAllLang = jsonObj[key2];
            for (var j = 0; j <= jsonAllLang.length - 1; j++) {
              Map<String, dynamic> jsonLang = jsonAllLang[j];

              int progr = 0;
              String testo = "";
              String categoria = "";
              String maschera = "";
              String data_ora = "";
              String utente = "";

              jsonLang.forEach((final String key3, final value3) {
                print(key3 + " " + value3);

                if (key3 == "progr") {
                  progr = value3;
                } else if (key3 == "testo") {
                  testo = value3;
                }  else if (key3 == "categoria") {
                  categoria = value3;
                } else if (key3 == "maschera") {
                  maschera = value3;
                } else if (key3 == "data_ora") {
                  data_ora = value3;
                } else if (key3 == "utente") {
                  utente = value3;
                }
              });
              l.add(new LogJSON(progr, testo, categoria, maschera, data_ora, utente, false, false));
            }
          });
          //CaptionsLingua.captions_lingua[key] = mapValori;
        }*/
      });

      /*CaptionsLingua.captions_lingua.forEach((final String key, final value) {
        Map<String, String> provaScorrim = CaptionsLingua.captions_lingua[key];
        provaScorrim.forEach((final String key2, value2) {
          //   print("Maschera: " + " Oggetto: " +  key2 +  " Traduzione: " +       value2);
        });
      });*/




*/

      /*content.forEach((final String key, final value) async {
        if (key == "") {
          //IndirizzoIPServer = value;
        }
      });*/

        } catch (e) {
      print("Eccezione WRITE LOG: " + e.toString());
      return l;
    }
  } else {
    print("Il file contenente la nuova versione dell'app non esiste!");
    return l;
  }
}

Future writeLogFileTemporaneo(List<LogJSON> contenuto_nuovo) async {
  String cartella = GlobalConfiguration().getValue("cartella_terminale").toString() + "/logs";
  String nome_file = "TOWRITE_" + getNomeFileLog(GlobalConfiguration().getValue("nome_file_log").toString());
  String percorso = cartella + "/" + nome_file;

  await esisteFile(cartella, nome_file).then((esiste) async {
    if (!esiste) {
      createFile("[]", percorso);
    }
  });

  if(percorso.contains(".txt")) {
  } else if (percorso.contains(".json")) {
    List<LogJSON> contenuto_iniziale = new List<LogJSON>();
    await leggiFileJsonLog(percorso).then((result) {
      contenuto_iniziale = result;
    });

    List<LogJSON> contenuto_finale = contenuto_iniziale + contenuto_nuovo;

    esisteFile(cartella, nome_file).then((esiste) async {
      if (esiste) {
        File file = await new File(percorso);
        file.writeAsStringSync(json.encode(contenuto_finale));
      } else {
        /////
      }
    });

  }
}

WriteLogs([bool upload_server = false]) async {
  if (_logs.length >= 100) {
    resetLog(50);
  }

  await deteleOldLogFile(GlobalConfiguration().getInt("giorni_retenzione_file_log")).then((v) async {
   if(isNuoviLogs()) {
     await writeLogFile().then((v) async {
       if (upload_server) {
         await uploadLogFile();
       }
     });
   }
  });
}

Future writeLogFile() async {
  String cartella = GlobalConfiguration().getValue("cartella_terminale").toString() + "/logs";
  String nome_file = getNomeFileLog(GlobalConfiguration().getValue("nome_file_log").toString());
  String tipo_log = GlobalConfiguration().getValue("tipo_log").toString();
  String percorso = cartella + "/" + nome_file;

  await esisteFile(cartella, nome_file).then((esiste) async {
    if (!esiste) {
      createFile("[]", percorso);
    }
  });

  if(percorso.contains(".txt")) {
    String contenuto_iniziale = "";
    await leggiFileTxt(percorso).then((result) {
      contenuto_iniziale = result;
    });

    String contenuto_nuovo = testoLog(tipo_log);

    String contenuto_finale = contenuto_iniziale + "\n" + contenuto_nuovo;
    esisteFile(cartella, nome_file).then((esiste) async {
      if (esiste) {
        File file = await new File(percorso);
        file.writeAsStringSync(contenuto_finale);
      } else {
        /////
      }
    });

  } else if (percorso.contains(".json")) {
    List<LogJSON> contenuto_iniziale = new List<LogJSON>();
    await leggiFileJsonLog(percorso).then((result) {
      contenuto_iniziale = result;
    });

    List<LogJSON> contenuto_nuovo = fromLogtoLogJson(testoLogJson(tipo_log));
    List<LogJSON> contenuto_finale = contenuto_iniziale + contenuto_nuovo;

    esisteFile(cartella, nome_file).then((esiste) async {
      if (esiste) {
        File file = await new File(percorso);
        file.writeAsStringSync(json.encode(contenuto_finale));
      } else {
        /////
      }
    });

    writeLogFileTemporaneo(contenuto_nuovo);
  }
}

List<LogJSON> fromLogtoLogJson(List<Log> log) {
  List<LogJSON> log_json = new List<LogJSON>();
  for(int i =0; i<log.length; i++) {
    log_json.add(new LogJSON(log[i].progr, log[i].testo, log[i].categoria, log[i].maschera, log[i].data_ora, log[i].utente));
  }

  return log_json;
}

List<Log> fromLogJsontoLog(List<LogJSON> log_json) {
  List<Log> log = new List<Log>();
  for(int i = 0; i < log_json.length; i++) {
    log.add(new Log(log_json[i].progr, log_json[i].testo, log_json[i].categoria, log_json[i].maschera, log_json[i].data_ora, log_json[i].utente, true, true));
  }

  return log;
}

Future copyLogFile(String cartella_server, String cartella_terminale, String nome_file) {
  String percorso_server = cartella_server + "/" + nome_file;
  String percorso_terminale = cartella_terminale + "/" + nome_file;

  String contenuto = returnFileContent(cartella_terminale, nome_file).toString();

  esisteFile(cartella_server, nome_file).then((esiste) async {
    if (esiste) {
      File file = await new File(percorso_server);
      file.writeAsStringSync(contenuto);
    } else {
      /////
    }
  });
}

String messageLogs(String mode) {
  //M = messaggio a video
  //S = apertura screen/maschera dedicata
  //P = print su terminale
  //F = apertura file log
  //V = display a video

  if(mode == "") {
    String logs = "";
    for (int i = 0; i < _logs.length; i++) {
      logs = logs + "${_logs[i].data_ora} -->  ${_logs[i].testo}  [${_logs[i].maschera}] \n       ${_logs[i].utente} > ${_logs[i].maschera}  [${_logs[i].progr.toString()}]";
      logs = logs + "\n";
    }

    return logs;
  } else if (mode == "V") {
    String logs = "";
    for (int j = 0; (j < _logs.length && j < 10); j++) {
      int i = _logs.length - 1 - j;
      logs = logs + "${_logs[i].data_ora} -->  ${_logs[i].testo}  [${_logs[i].maschera}] \n       ${_logs[i].utente}  [${_logs[i].progr.toString()}]";
      logs = logs + "\n";
    }

    return logs;
  } else if (mode == "M") {
    String logs = "";
    for (int j = 0; (j < _logs.length && j < 10); j++) {
      int i = _logs.length - 1 - j;
      logs = logs + "${_logs[i].data_ora} \n${_logs[i].testo} ";
      logs = logs + "\n\n";
    }

    return logs;
  } else {
    return "";
  }
}

Log returnLog(int indice) {
  return _logs[indice];
}

int returnLogLenght() {
  return _logs.length;
}

addLog(String testo, String tipo, String maschera) async {
  int progr = 0;
  String categoria = "";
  String data_ora = DateTime.now().toString();
  String nome_utente = GlobalConfiguration().getValue("utente");

    if (tipo == "X") {
      categoria = "ERRORE";
    } else if (tipo == "A") {
      categoria = "AZIONE";
    } else if (tipo == "E") {
      categoria = "EVENTO";
    }

    if (_logs == null || _logs.length == 0) {
      _logs = new List<Log>();

      _logs.add(new Log(
          0,
          "*** avvio applicazione ***",
          "EVENTO",
          "LoginScreen",
          data_ora,
          nome_utente,
          false,
          false));
      progr = 1;
    } else {
      progr = _logs.length;
    }

    _logs.add(new Log(
        progr,
        testo,
        categoria,
        maschera,
        data_ora,
        nome_utente,
        false,
        false));
}

deteleOldLogFile(int giorni_retenzione, [bool elimina_temp = true]) async {
  String cartella = GlobalConfiguration().getValue("cartella_terminale").toString() + "/logs";
  String nome_file_temp_attuale = "TOWRITE_" + getNomeFileLog(GlobalConfiguration().getValue("nome_file_log").toString());

  final directory = Directory(cartella);
  List<File> files = await filesCartella(cartella);

  for(int i = 0; i< files.length; i++) {
    String nome_file = files[i].path.toString();
    nome_file = nome_file.replaceAll(cartella  + "/", "");

    if(nome_file.contains("TOWRITE_") && nome_file != nome_file_temp_attuale && elimina_temp) {
      files[i].deleteSync();
      print("File Eliminato: " + files[i].path);
    } else {
      if(!nome_file.contains("TOWRITE_")) {
        DateTime data_file = files[i].lastModifiedSync();
        DateTime data_oggi = DateTime.now();
        final difference = data_oggi.difference(data_file).inDays;
        if (difference <= giorni_retenzione) {
          files[i].deleteSync();
          print("File Eliminato: " + files[i].path);
        }
      }
    }
  }





  //dir.deleteSync(recursive: true);

  /*List files = directory.listSync();
  for (var fileCartella in files) {
    if (fileCartella is File) {
      print(fileCartella.path);
    } else if (fileCartella is Directory) {
      print(fileCartella.path);
    }
  }*/
}

/*Future<List<String>> filesCartella(Directory cartella) {
  var filenames = <String>[];
  var completer = new Completer();
  var lister = cartella.list();
  lister.onFile = (filename) => filenames.add(filename);
  // should also register onError
  lister.onDone
  = (_) => completer.complete(filenames);
  return completer.future;
}*/

/*Future<List<FileSystemEntity>> filesCartella() {
  if (Directory(cartella).existsSync() == false) {
    Directory(cartella).createSync(recursive: true);
  }
  Directory directory = Directory(cartella);

  var files = <FileSystemEntity>[];
  var completer = new Completer();
  var lister = directory.list(recursive: false);
  lister.listen ((file) => files.add(file),
      onDone:   () => completer.complete(files)
  );
  return completer.future;
}*/



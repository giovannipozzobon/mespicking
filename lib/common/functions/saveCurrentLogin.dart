import 'package:MesPicking/model/json/loginModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

saveCurrentLogin(Map responseJson) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();

  var user;
  if ((responseJson != null && !responseJson.isEmpty)) {
    user = LoginModel.fromJson(responseJson).userName;
  } else {
    user = "";
  }
  var token = (responseJson != null && !responseJson.isEmpty) ? LoginModel.fromJson(responseJson).token : "";
  var email = (responseJson != null && !responseJson.isEmpty) ? LoginModel.fromJson(responseJson).email : "";
  var username = (responseJson != null && !responseJson.isEmpty) ? LoginModel.fromJson(responseJson).userName : "";
  var first_name = (responseJson != null && !responseJson.isEmpty) ? LoginModel.fromJson(responseJson).first_name : "";
  var second_name = (responseJson != null && !responseJson.isEmpty) ? LoginModel.fromJson(responseJson).second_name : "";
  var login_code = (responseJson != null && !responseJson.isEmpty) ? LoginModel.fromJson(responseJson).login_code : "";

  await preferences.setString('LastUser', (user != null && user.length > 0) ? user : "");
  await preferences.setString('LastToken', (token != null && token.length > 0) ? token : "");
  await preferences.setString('LastEmail', (email != null && email.length > 0) ? email : "");
  await preferences.setString('username', (username != null && username.length > 0) ? username : "");
  await preferences.setString('first_name', (first_name != null && first_name.length > 0) ? first_name : "");
  await preferences.setString('second_name', (second_name != null && second_name.length > 0) ? second_name : "");
  await preferences.setString('login_code', (login_code != null && login_code.length > 0) ? login_code : "");
  /*
  Map<String, String> map = {
    "token" : token
  };

  GlobalConfiguration().add(map);

  */
}
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:MesPicking/model/json/LogJSON.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:MesPicking/ui/AsarDrawer.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////  VARIABILI
var httpClient2 = new HttpClient();

Future openFile(String filename) async {
  try {
    final String filePath = filename;
    print(filePath);
    final String result = await CanaleFlutterAndroid.platformMethodChannel
        .invokeMethod('OpenFileDownloaded', {'filePath': '$filePath'});
    print(result);
  } on PlatformException catch (e) {
    print(e.message);
  }
}

Future createFile(String contenuto, String percorso) {
  File file = new File(percorso);
  file.createSync();
  file.writeAsStringSync(contenuto);
}

String getNomeFileLog(String nome_file) {
  String data_ora = DateFormat("yyyyMMdd").format(new DateTime.now());
  String nome_utente = GlobalConfiguration().getValue("utente").toString();

  nome_file = nome_file.replaceAll("%UTENTE%", nome_utente);
  nome_file = nome_file.replaceAll("%DATA%", data_ora);

  return nome_file;
}

Future<List<File>> filesCartella(String cartella) async {
  if (Directory(cartella).existsSync() == false) {
    Directory(cartella).createSync(recursive: true);
  }
  Directory directory = Directory(cartella);


  List<File> files = <File>[];
  await for (FileSystemEntity entity in directory.list(
      recursive: false, followLinks: false)) {
    FileSystemEntityType type = await FileSystemEntity.type(entity.path);
    if (type == FileSystemEntityType.FILE) {
      files.add(entity);
      //print(entity.path);
    }
  }
  return files;
}

Future<bool> esisteFile(String cartella, String nome_file) async {
  String percorso = cartella + "/" + nome_file;

  if (Directory(cartella).existsSync() == false) {
    Directory(cartella).createSync(recursive: true);
  }

  if (await File(percorso).exists()) {
   return true;
  } else {
    createFile("[]", percorso);
    return true;
  }
}

Future<String> leggiFileTxt(String percorso) async {
  if (await File(percorso).exists()) {
    try {
      final file = await File(percorso);
      String content = await file.readAsString();
      return content;
    } catch (e) {
      print("Eccezione: " + e.toString());
      return "**";
    }
  } else {
    print("Il file non esiste!");
    return "";
  }
}



Future<String> returnFileContent(String cartella, String nome_file) async {
  String percorso = cartella + "/" + nome_file;

  esisteFile(cartella, nome_file).then((esiste) async {
    if (esiste) {
      try {
        final nome_file = await File(percorso);
        String testo = await nome_file.readAsString();
        return testo;
      } catch (e) {
        print("Eccezione: " + e.toString());
        return "";
      }
    } else {
      return "";
    }
  });
}

Future<bool> checkExistApplication(String packageName, String nomeApp) async {
  try {
    final bool result = await CanaleFlutterAndroid.platformMethodChannel
        .invokeMethod(
            'CheckExistApp', {'parametri': '$packageName;' + '$nomeApp'});
    print(result);
    if (result != null) {
      return result;
    } else {
      return false;
    }
  } on PlatformException catch (e) {
    print(e.message);
    return false;
  }
}

Future openApplication(String packageName, String nomeApp) async {
  try {
    final String result = await CanaleFlutterAndroid.platformMethodChannel
        .invokeMethod(
            'OpenAppGeneral', {'parametri': '$packageName;' + '$nomeApp'});
    print(result);
  } on PlatformException catch (e) {
    print(e.message);
  }
}

Future downloadFile(String url, String percorso) async {
  var request = await httpClient2.getUrl(Uri.parse(url));
  var response = await request.close();
  var bytes = await consolidateHttpClientResponseBytes(response);
  File file = await new File(percorso);
  await file.writeAsBytes(bytes);
}

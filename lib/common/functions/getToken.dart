import 'package:shared_preferences/shared_preferences.dart';

getToken() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();

  String getToken = await preferences.getString("LastToken");
  return getToken;
}

String syncGetToken() {
  SharedPreferences.getInstance().then((preferences){
    String token = preferences.getString("LastToken");
    return token;
  });
}

String ReplaceTextController(String value) {
  if (value.contains('\n')) {
    String temp = value.replaceAll("\n", "");
    temp = temp
        .split(new RegExp(r'(?:\r?\n|\r)'))
        .where((s) => s.trim().length != 0).join('\n');
    value = temp;
  }
  return value;
}
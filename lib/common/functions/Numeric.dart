bool NumericField(String value)//controllo se campo contiene solo numeri
{
  try
  {
    int.parse(value);
    return true;
  }
  catch(e)
  {
    try
    {
      double.parse(value);
      return true;
    }
    catch(e)
    {
      return false;
    }
  }
}

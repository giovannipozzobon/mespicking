import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:MesPicking/common/functions/ShowMessage.dart';
import 'package:MesPicking/common/functions/getToken.dart';

Future genericPost(String address, String body, BuildContext context, GlobalKey<ScaffoldState> _scaffoldKey, bool showMessages) async
{
  String _url = GlobalConfiguration().getString("asar_server") + address;
  var token;

  await getToken().then((result)
  {
    token = result;
  });

  final response = await http.post(
    _url,
    body: body,
    headers: {HttpHeaders.authorizationHeader: "$token", "Content-Type": "application/json"},
  ).catchError((test){
    showMessage(_scaffoldKey, "Errore nella chiamata post!");
  });

  if (response.statusCode == 200) {
    final decoded = jsonDecode(response.body) as Map;
    final int err = decoded['err'];
    final String error_msg = decoded['error_msg'];

    if (err == 0)
    {
      return new PostReturn(err, error_msg, true);
    } else if (err == 1000)
    {
      return new PostReturn(err, error_msg, true);
    } else
    {
      if (showMessages == true) showMessage(_scaffoldKey, error_msg);
      return new PostReturn(err, error_msg, false);
    }
  }
  else {
    if (showMessages == true) showMessage(_scaffoldKey, 'Server error!');
    return new PostReturn(response.statusCode, response.reasonPhrase, false);
  }

}

class PostReturn {
  final int err;
  final String error_msg;
  final bool result;

  PostReturn(this.err, this.error_msg, this.result);
}

import 'dart:async';
import 'package:MesPicking/common/functions/LogManagment.dart';
import 'package:global_configuration/global_configuration.dart';

class LogScheduler {
  static Timer timer_upload_terminale = null;
  static bool attivo_timer_terminale = false;
  static Timer timer_upload_server = null;
  static bool attivo_timer_server = false;
}

@override
startTimerLogTerminale() {
  try {
    if (LogScheduler.timer_upload_terminale == null /*&& LogScheduler.attivo == true*/) {
      LogScheduler.timer_upload_terminale = Timer.periodic(Duration(seconds: 15), (timer) async {
        WriteLogs();
        print('**** Scrittura Log Terminale (TIMER ATTIVO)');
      });
    }
  } catch (e) {
    print(e.toString());
  }
}

@override
stopTimerLogTerminale() {
  try {
    if (LogScheduler.timer_upload_terminale != null) {
      LogScheduler.timer_upload_terminale.cancel();
      LogScheduler.timer_upload_terminale = null;
      print('#### Scrittura Log Terminale (TIMER STOP)');
    }
  } catch (e) {
    print(e.toString());
  }
}

@override
startTimerLogServer() {
  int secondi = GlobalConfiguration().getInt("time_upload_log_server");
  try {
    if (LogScheduler.timer_upload_server == null /*&& LogScheduler.attivo == true*/) {
      LogScheduler.timer_upload_server = Timer.periodic(Duration(seconds: secondi), (timer) async {
        uploadLogFile();
        print('**** Scrittura Log Server (TIMER ATTIVO)');
      });
    }
  } catch (e) {
    print(e.toString());
  }
}

@override
stopTimerLogServer() {
  try {
    if (LogScheduler.timer_upload_terminale != null) {
      LogScheduler.timer_upload_terminale.cancel();
      LogScheduler.timer_upload_terminale = null;
      print('#### Scrittura Log Server (TIMER STOP)');
    }
  } catch (e) {
    print(e.toString());
  }
}
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:global_configuration/global_configuration.dart';
import 'package:MesPicking/common/functions/saveCurrentLogin.dart';
import 'package:MesPicking/model/json/CheckUtente.dart';
import 'package:MesPicking/model/json/loginModel.dart';
import 'package:MesPicking/service/CheckUtenteService.dart';
import 'package:MesPicking/ui/loginScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

void setUserProperties () async {
  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    List<CheckUtente> check = await CheckUtenteService.browse(filter: "utente=");

    if (check.length > 0) {
      await preferences.setString(
          'UserTitle', (check[0].utente != null) ? check[0].utente : "O");
      await preferences.setString(
          'UserCode', (check[0].rag_sociale != null) ? check[0].rag_sociale : "");
    }
  }
  catch (e)
  {
    print(e.toString());
  }
}

Future<LoginModel> requestLoginAPI(BuildContext context, String username, String password) async {
  try {
    final url = GlobalConfiguration().getString("asar_server") + GlobalConfiguration().getString("url_login");

    Map<String, String> body = {
      'username': username,
      'password': password,
    };

    final response = await http.post(
      url,
      body: body,
    );

    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body);

      await saveCurrentLogin(responseJson);


      GlobalConfiguration().setValue("utente", username);
      Navigator.of(context).pushNamedAndRemoveUntil('/ListaPrelieviScreen', (Route<dynamic> route) => false);

      return LoginModel.fromJson(responseJson);
    } else {
      final responseJson = json.decode(response.body);

      saveCurrentLogin(responseJson);

      return null;
    }
  }
  catch (e)
  {
    print(e.toString());
  }
}


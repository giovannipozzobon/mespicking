import 'dart:async';
import 'package:flutter/material.dart';
import 'package:MesPicking/common/functions/saveLogout.dart';
import 'package:MesPicking/model/json/loginModel.dart';


Future<LoginModel> requestLogoutAPI(BuildContext context) async {
  saveLogout();
  return null;
}
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////  CONFIGURAZIONE
Color colore1 = Color(4286588201);
Color colore2 = Color(4288362813);
Color colore3 = Color(4293229064);
Color colore4 = Color(0xfff5f5f5);
Color colore5 = Color(0xffD6D2D1);

class BasicDrawer extends StatefulWidget {
  BasicDrawer({
    Key key,
    this.callback,
  }) : super(key: key);

  final DrawerCallback callback;

  @override
  _BasicDrawerState createState() => _BasicDrawerState();
}

class _BasicDrawerState extends State<BasicDrawer> {
  bool Initialized = false;

  @override
  initState() {
    if (widget.callback != null) {
      widget.callback(true);
    }
    super.initState();
  }

  onPressedInformazioni() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');

    Navigator.of(context).pushNamed('/InformazioniScreen');
  }

  @override
  dispose() {
    if (widget.callback != null) {
      widget.callback(false);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      key: widget.key,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: FlatButton(
                onPressed: () {
                  onPressedInformazioni();
                },
                color: colore5,
                padding: EdgeInsets.all(20.0),
                child: Row(
                  children: <Widget>[Icon(Icons.info), Text("  Informazioni App")],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

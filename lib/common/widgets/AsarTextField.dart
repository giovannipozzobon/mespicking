import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


// Classe per definire un TextEdit inglobato in RawKeyboardListener
//

class AsarTextField extends StatefulWidget {

  final key;
  final Function onAsarSubmitted;
  final asarTextEditingController;
  final asarFocusNode;
  final asarReadOnly;
  final asarDecoration;
  final asarAutoFocus;
  final asarKeyboardType;
  final asarHintText;
  final asarLabelText;
  final asarErrorText;

  const  AsarTextField(
      {
        this.onAsarSubmitted,
        this.asarTextEditingController,
        this.asarFocusNode,
        this.asarReadOnly,
        this.asarDecoration,
        this.asarAutoFocus,
        this.asarKeyboardType,
        this.asarErrorText,
        this.asarLabelText,
        this.asarHintText,
        this.key})
      : super(key: key);

	@override
	_AsarTextField createState() => new _AsarTextField();
}


class _AsarTextField extends State<AsarTextField> {

  /*
  final ValueChanged<String> onAsarSubmitted;
  final TextEditingController asarTextEditingController;
  final FocusNode asarFocusNode;
  final bool asarReadOnly;
  final InputDecoration asarDecoration;
  final bool asarAutoFocus;
  final TextInputType asarKeyboardType;
  final String asarHintText;
  final String asarLabelText;
  final String asarErrorText;

   */

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (widget.asarReadOnly) {

      print ('Label created');
      return TextField(
        key: widget.key,
        controller: widget.asarTextEditingController,
        focusNode: widget.asarFocusNode,
        // decoration: widget.asarDecoration,
        decoration: (widget.asarDecoration != null) ? widget.asarDecoration : new InputDecoration(
            hintText: widget.asarHintText,
            labelText: widget.asarLabelText,
            errorText: widget.asarErrorText),
        readOnly: true,
      );
    }
    else {
      print ('TextField created');
      return TextField(
        controller: widget.asarTextEditingController,
        focusNode: widget.asarFocusNode,
        // decoration: widget.asarDecoration,
        decoration: (widget.asarDecoration != null) ? widget.asarDecoration : new InputDecoration(
            hintText: widget.asarHintText,
            labelText: widget.asarLabelText,
            errorText: widget.asarErrorText),
        onSubmitted: widget.onAsarSubmitted,
        readOnly: widget.asarReadOnly,
        autofocus: widget.asarAutoFocus,
        keyboardType: (widget.asarKeyboardType != null) ? widget.asarKeyboardType : TextInputType.text,
        onChanged: (value) {
          // test if last key is Enter
          print ('value is ' + value);
        },
      );
    }
  }
}


class AsarTextFieldKeyboard extends StatelessWidget {
  /*
  final ValueChanged<String> onAsarSubmitted;
  final TextEditingController asarTextEditingController;
  final FocusNode asarFocusNode;
  final bool asarReadOnly;
  final InputDecoration asarDecoration;
  final bool asarAutoFocus;
  final TextInputType asarKeyboardType;
  final String asarHintText;
  final String asarLabelText;
  final String asarErrorText;

   */
  final key;
  final Function onAsarSubmitted;
  final asarTextEditingController;
  final asarFocusNode;
  final asarReadOnly;
  final asarDecoration;
  final asarAutoFocus;
  final asarKeyboardType;
  final asarHintText;
  final asarLabelText;
  final asarErrorText;

  const AsarTextFieldKeyboard(
      {
      this.onAsarSubmitted,
      this.asarTextEditingController,
      this.asarFocusNode,
      this.asarReadOnly,
      this.asarDecoration,
      this.asarAutoFocus,
      this.asarKeyboardType,
      this.asarErrorText,
      this.asarLabelText,
      this.asarHintText,
        this.key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (asarReadOnly) {

      print ('Label created');
      return TextField(
        key: key,
        controller: asarTextEditingController,
        focusNode: asarFocusNode,
        // decoration: widget.asarDecoration,
        decoration: (asarDecoration != null) ? asarDecoration : new InputDecoration(
            hintText: asarHintText,
            labelText: asarLabelText,
            errorText: asarErrorText),
        readOnly: true,
      );
    }
    else {
      print ('TextField created');
      return RawKeyboardListener(
        focusNode: new FocusNode(),
        onKey: (RawKeyEvent event) {
          if (event.runtimeType == RawKeyDownEvent &&
              event.logicalKey == LogicalKeyboardKey.enter) {
            if (onAsarSubmitted != null) {
              print ('Exec onSubmitted on ' + asarTextEditingController.text);
              onAsarSubmitted(asarTextEditingController.text);
            } else {
              print ('no Sumbit function found');
            }

          } else {
            print ('Other event occurred: ' + event.toStringShort());
          }
        },
        child: TextField(
          controller: asarTextEditingController,
          focusNode: asarFocusNode,
          // decoration: widget.asarDecoration,
          decoration: (asarDecoration != null) ? asarDecoration : new InputDecoration(
              hintText: asarHintText,
              labelText: asarLabelText,
              errorText: asarErrorText),
          onSubmitted: onAsarSubmitted,
          readOnly: asarReadOnly,
          autofocus: asarAutoFocus,
          keyboardType: (asarKeyboardType != null) ? asarKeyboardType : TextInputType.text,
          onChanged: (value) {
            // test if last key is Enter
            print ('value is ' + value);
          },
        ),
      );
    }
  }
}

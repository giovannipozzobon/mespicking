import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

class CircleIconButton extends StatelessWidget {
  final double size;
  final Function onPressed;
  final IconData icon;
  final FocusNode focus;

  CircleIconButton(
      {this.size = 30.0, this.icon = Icons.add, this.onPressed, this.focus});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: this.onPressed,
        child: SizedBox(
            width: size,
            height: size,
            child: Stack(
              alignment: Alignment(0.0, 0.0),
              children: <Widget>[
                Container(
                  width: size,
                  height: size,
                  decoration: (focus.hasFocus)
                      ? BoxDecoration(
                      border: Border(
                        top: BorderSide(width: 1.0, color: Color(4286588201)),
                        left: BorderSide(width: 1.0, color: Color(4286588201)),
                        right: BorderSide(width: 1.0, color: Color(4286588201)),
                        bottom: BorderSide(width: 1.0, color: Color(4286588201)),
                      ),
                      shape: BoxShape.circle,
                      color: Colors.white)
                      : BoxDecoration(
                      border: Border(
                        top: BorderSide(
                            width: 1.0, color: Color(0xffD6D2D1)),
                        left: BorderSide(
                            width: 1.0, color: Color(0xffD6D2D1)),
                        right: BorderSide(
                            width: 1.0, color: Color(0xffD6D2D1)),
                        bottom: BorderSide(
                            width: 1.0, color: Color(0xffD6D2D1)),
                      ),
                      shape: BoxShape.circle,
                      color: Colors.white),
                  child: Icon(
                    icon,
                    size: size * 0.6,
                  ),
                ),
              ],
            )));
  }
}